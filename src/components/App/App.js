import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import { ThemeProvider } from "styled-components";
import LayoutApp from "./../Layout";
//import LoginPage from "./../../components/LoginPage";
import { darkerColor, ChangeOpactityColor } from "./../../utils/style/color";
//import { IsAuthenticated, IsNotAuthenticated } from "./../../lib/auth/index";

const defaultTheme = {
	colors: {
		primary: "#ED8B00",
		secondary: "#CC5D0A",
		default: "#8A8A8A",
		success: "#4CAF50",
		info: "#00BCD4",
		danger: "#E53935",
		warning: "#FBC02D",
		light: "#FFF",
		dark: "#263238",
	},
	font: {
		fontFamily: "Roboto, sans-serif",
		fontSize: "14px",
	},
	spacing: {
		unit: 2,
	},
	functions: {
		luminanceColor: darkerColor,
		opacityColor: ChangeOpactityColor,
	},
};
const myTheme = {
	...defaultTheme,
	spacing: {
		...defaultTheme.spacing,
		small: 5 * defaultTheme.spacing.unit,
		medium: 8 * defaultTheme.spacing.unit,
		large: 12 * defaultTheme.spacing.unit,
	},
};

class App extends Component {
	render() {
		return (
			<ThemeProvider theme={myTheme}>
				<Switch>
					<Route path="/" component={LayoutApp} />
				</Switch>
			</ThemeProvider>
		);
	}
}

export default App;
