export const I18n_FR = {
  fr: {
    UserNotFoundException: "Utilisateur introuvable",
    "User does not exist.": "L'utilisateur n'existe pas",
    "Username cannot be empty": "Le nom d'utilisateur ne peut pas être vide",
    NotAuthorizedException: "Identifiant ou mot de passe incorrect.",
    "Username/client id combination not found.":
      "La combinaison nom d'utilisateur / identifiant client n'a pas été trouvée.",
    CodeMismatchException: "Erreur de discordance de code",
    "Invalid verification code provided, please try again.":
      "Le code de validation n'est pas valide. Veuillez réessayer.",
    LimitExceededException: "Limite dépassée ",
    "Attempt limit exceeded, please try after some time.":
      "La limite de tentative a dépassé, veuillez essayer après un certain temps.",
  },
};
