import React, { Fragment } from "react";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route } from "react-router-dom";
import App from "./../App";

const Root = ({ store }) => {
	//let history = syncHistoryWithStore(Router, store);
	//console.log(history);
	return (
		<Provider store={store}>
			<Router>
				<Fragment>
					<Route path="/" component={App} />
				</Fragment>
			</Router>
		</Provider>
	);
};

export default Root;
