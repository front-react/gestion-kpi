import React, { Component } from "react";
import LoginFormAws from "./LoginFormAws";
import { Icon } from "antd";
import { Auth, I18n, JS } from "aws-amplify";
import { AmplifyTheme } from "aws-amplify-react";
import { Alert, Row, Col } from "antd";
import {
  PaperLogin,
  MarginLogin,
  BorderLogo,
  FormLogin,
  ButtonLogin,
  AlertBox,
  ButtonMdp,
} from "./../loginStyle/loginStyle";
import { I18n_FR } from "./../../I18nFr/I18nFr";
import "./../../../lib/amplifyAws/configAmplify";
I18n.setLanguage("fr");
I18n.putVocabularies(I18n_FR);

const SectionHeaderTheme = {
  ...AmplifyTheme.sectionHeader,
  backgroundColor: "none",
  color: "black",
};
const FormSectionTheme = { ...AmplifyTheme.formSection, marginTop: "150px" };

const theme = Object.assign({}, AmplifyTheme, {
  sectionHeader: SectionHeaderTheme,
  formSection: FormSectionTheme,
});

class SignIn extends Component {
  componentWillReceiveProps(nextProps) {
    if (nextProps.user.challengeName === "NEW_PASSWORD_REQUIRED") {
      this.props.onStateChange("requireNewPassword", nextProps.user);
    }
  }

  checkContact = user => {
    Auth.verifiedContact(user).then(data => {
      if (data.verified) {
        this.props.onStateChange("signedIn", user);
      }
    });
  };

  onSubmit = values => {
    //this.props.setIdentityUser(values);
    Auth.signIn(values.username, values.password)
      .then(user => {
        if (user.challengeName === "NEW_PASSWORD_REQUIRED") {
          this.props.onStateChange("requireNewPassword", user);
        } else {
          this.props.onStateChange("signedIn", user);
          this.props.setIdentityUser(user);
        }
        //this.props.setIdentityUser(user);
      })
      .catch(err => {
        //console.log(err);
        this.props.ErrorStatusLogin(err);
      });
  };
  handleForgot = () => {
    this.props.onStateChange("forgotPassword");
    this.props.ErrorStatusLogin(null);
  };
  onClose = () => {
    this.props.ErrorStatusLogin(null);
  };
  render() {
    const { errorUser } = this.props;

    if (this.props.authState !== "signIn") {
      return null;
    }

    return (
      <PaperLogin>
        <MarginLogin>
          <BorderLogo />
          <FormLogin>
            <LoginFormAws onSubmit={this.onSubmit} />
          </FormLogin>
          <ButtonMdp onClick={this.handleForgot}>
            Mot de passe oublié ?
          </ButtonMdp>
          {errorUser &&
            <AlertBox>
              <Alert
                message={I18n.get(errorUser.code)}
                description={
                  errorUser.message
                    ? I18n.get(errorUser.message)
                    : I18n.get(errorUser)
                }
                type="error"
                closable
                onClose={this.onClose}
              />
            </AlertBox>}
        </MarginLogin>
      </PaperLogin>
    );
  }
}
export default SignIn;
