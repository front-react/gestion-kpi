import { connect } from "react-redux";
import LogoutAws from "./LogoutAws";
import { ErrorStatusLogin } from "./../../../redux/actions/user";

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    ErrorStatusLogin: data => {
      dispatch(ErrorStatusLogin(data));
    },
  };
};

export default connect(null, mapDispatchToProps)(LogoutAws);
