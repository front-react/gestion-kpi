import React, { Component } from "react";
import { Icon } from "react-icons-kit";
import { powerOff } from "react-icons-kit/fa/powerOff";
import { Auth } from "aws-amplify";
import { withAuthenticator } from "aws-amplify-react";
import { browserHistory } from "react-router";
import { withRouter } from "react-router-dom";
import { Link } from "react-router-dom";

import "./../../../lib/amplifyAws/configAmplify";

class LogoutAws extends Component {
  handleAuthStateChange = () => {
    const { match, location, history } = this.props;
    console.log(this.props);
    Auth.signOut()
      .then(data => {
        this.props.onStateChange("signOut");
      })
      .catch(err => this.props.ErrorStatusLogin(null));
  };

  render() {
    return (
      <div>
        <div onClick={this.handleAuthStateChange}>
          <Link to={"#"}>
            <Icon
              icon={powerOff}
              size={28}
              style={{ color: "black", width: 24, height: 24, margin: 14 }}
            />
          </Link>
        </div>
      </div>
    );
  }
}

export default withRouter(LogoutAws);
