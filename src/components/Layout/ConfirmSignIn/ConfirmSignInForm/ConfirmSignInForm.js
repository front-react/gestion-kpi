import React, { Component } from "react";
import { Form as FinalForm } from "react-final-form";
import Form from "./Form";

class ConfirmSignInForm extends Component {
  componentDidMount() {}

  onSubmit = values => {
    this.props.onSubmit(values);
  };

  render() {
    const { initialValues } = this.props;
    return (
      <FinalForm
        onSubmit={this.onSubmit}
        initialValues={initialValues}
        render={props => <Form {...props} />}
      />
    );
  }
}

export default ConfirmSignInForm;
