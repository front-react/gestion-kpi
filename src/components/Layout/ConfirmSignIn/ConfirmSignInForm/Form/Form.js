import React from "react";
import { Field } from "react-final-form";
import { Icon } from "antd";
import {
  FormInput,
  InputAnt,
  SubmitButtonIconLabelAnt,
} from "./../../../loginStyle/loginStyle";

const renderActionButtons = (
  submitting,
  reset,
  pristine,
  onClickSubmit,
  onClickCancel,
) =>
  <div>
    <SubmitButtonIconLabelAnt
      disabled={submitting}
      onClick={onClickSubmit}
      color="primary"
    />
  </div>;

const Form = ({ handleSubmit, reset, submitting, pristine, values }) => {
  return (
    <div>
      <form onSubmit={handleSubmit}>
        <Field name="code">
          {({ input }) =>
            <FormInput>
              <InputAnt
                placeholder="Votre code"
                autocomplete="code"
                prefix={
                  <Icon type="eye-o" style={{ color: "rgba(0,0,0,.25)" }} />
                }
              />
            </FormInput>}
        </Field>
        {renderActionButtons()}
      </form>
    </div>
  );
};

export default Form;
