import React, { Component } from "react";
import ConfirmSignInForm from "./ConfirmSignInForm";
import { Icon } from "antd";
import { Auth, I18n, JS } from "aws-amplify";
import { Alert } from "antd";
import {
  PaperLogin,
  MarginLogin,
  BorderLogo,
  FormLogin,
  ButtonLogin,
  AlertBox,
} from "./../loginStyle/loginStyle";
import { I18n_FR } from "./../../I18nFr/I18nFr";
import "./../../../lib/amplifyAws/configAmplify";
I18n.setLanguage("fr");
I18n.putVocabularies(I18n_FR);

class ConfirmSignIn extends Component {
  state = {
    mfaType: "SMS",
  };

  onSubmit = values => {
    const user = this.props.authData;
    const mfaType =
      user.challengeName === "SOFTWARE_TOKEN_MFA" ? "SOFTWARE_TOKEN_MFA" : null;
    Auth.confirmSignIn(user, values.code, mfaType)
      .then(() => this.props.onStateChange("signedIn"))
      .catch(err => this.props.ErrorStatusLogin(err));
  };
  handleSignIn = () => {
    this.props.onStateChange("signIn");
    this.props.ErrorStatusLogin(null);
  };

  componentDidUpdate() {
    const user = this.props.authData;
    const mfaType =
      user && user.challengeName === "SOFTWARE_TOKEN_MFA" ? "TOTP" : "SMS";
    if (this.state.mfaType !== mfaType) this.setState({ mfaType });
  }

  render() {
    const { errorUser } = this.props;
    if (this.props.authState !== "confirmSignIn") {
      return null;
    }
    return (
      <PaperLogin>
        <MarginLogin>
          <BorderLogo />
          <FormLogin>
            <ConfirmSignInForm onSubmit={this.onSubmit} />
          </FormLogin>
          <ButtonLogin onClick={this.handleSignIn}>
            Retour à la page de connexion
          </ButtonLogin>
          {errorUser &&
            <AlertBox>
              <Alert
                message={I18n.get(errorUser.code)}
                description={
                  errorUser.message
                    ? I18n.get(errorUser.message)
                    : I18n.get(errorUser)
                }
                type="error"
                closable
                onClose={this.onClose}
              />
            </AlertBox>}
        </MarginLogin>
      </PaperLogin>
    );
  }
}
export default ConfirmSignIn;
