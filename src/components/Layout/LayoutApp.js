import React, { Component } from "react";
import { Route, Link } from "react-router-dom";
import { withStyles } from "material-ui/styles";
import { orange } from "material-ui/colors";
import {
	ConfirmSignUp,
	SignUp,
	VerifyContact,
	withAuthenticator,
} from "aws-amplify-react";
import SignIn from "./SignIn";
import LogoutAws from "./LogoutAws";
import ForgotPasswordAws from "./ForgotPasswordAws";
import RequireNewPassword from "./RequireNewPassword";
import ConfirmSignIn from "./ConfirmSignIn";
import Survey from "./../../modules/survey/components/Survey";
import Client from "./../../modules/client/components/Client";
//import { Icon } from "react-icons-kit";
//import { newspaperO } from "react-icons-kit/fa/newspaperO";
import { withRouter } from "react-router-dom";
import { Menu, Icon } from "antd";
import "@trendmicro/react-sidenav/dist/react-sidenav.css";
import "./../../lib/amplifyAws/configAmplify";

const styles = theme => ({
	content: {
		backgroundColor: theme.palette.background.default,
		padding: theme.spacing.unit * 2,
	},
	logo: {
		height: "32px",
		margin: "16px",
	},
	sider: {
		backgroundColor: orange[500],
	},
	menu: {
		backgroundColor: orange[500],
	},
});

class LayoutApp extends Component {
	state = {
		current: "mail",
	};

	handleClick = e => {
		this.setState({
			current: e.key,
		});
	};
	render() {
		const {} = this.props;

		return (
			<div>
				<Menu
					onClick={this.handleClick}
					selectedKeys={[this.state.current]}
					mode="horizontal"
				>
					<Menu.Item key="survey">
						<Link to="/survey">
							<Icon type="layout" style={{ height: 36 }} />
							<span style={{ color: "black", fontSize: "12px" }}>Survey</span>
						</Link>
					</Menu.Item>
					<Menu.Item key="client">
						<Link to="/client">
							<Icon type="file-excel" style={{ height: 36 }} />
							<span style={{ color: "black", fontSize: "12px" }}>Client</span>
						</Link>
					</Menu.Item>
					<Menu.Item key="logout">
						<LogoutAws onStateChange={this.props.onStateChange} />
					</Menu.Item>
				</Menu>
				<Route path="/survey" component={props => <Survey {...props} />} />
				<Route path="/client" component={props => <Client {...props} />} />
			</div>
		);
	}
}

export default withAuthenticator(withStyles(styles)(withRouter(LayoutApp)), false, [
	<SignIn />,
	<ConfirmSignIn />,
	<RequireNewPassword />,
	<VerifyContact />,
	<SignUp />,
	<ConfirmSignUp />,
	<ForgotPasswordAws />,
]);
