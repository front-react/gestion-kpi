import React, { Component } from "react";
import RequireNewPasswordForm from "./RequireNewPasswordForm";
import { Icon, Popover, Alert } from "antd";
import { Auth, I18n } from "aws-amplify";
import {
  PaperLogin,
  MarginLogin,
  BorderLogo,
  FormLogin,
  ButtonLogin,
  AlertBox,
  AlertAnt,
} from "./../loginStyle/loginStyle";
import { I18n_FR } from "./../../I18nFr/I18nFr";
import "./../../../lib/amplifyAws/configAmplify";
I18n.setLanguage("fr");
I18n.putVocabularies(I18n_FR);

// const messagePassword = (
//   <AlertAnt
//     message={
//       <span>
//         Votre mot de passe doit se composer au minimum de{" "}
//         <strong>8 caractères</strong> avec quatre types de caractères différents
//         : <strong>majuscules</strong>, <strong>minuscules</strong>,{" "}
//         <strong>chiffres</strong>, et <strong>signes de ponctuation</strong> ou{" "}
//         <strong>caractères spéciaux</strong>
//       </span>
//     }
//     type="warning"
//     showIcon
//   />
// );

class RequireNewPassword extends Component {
  onSubmit = values => {
    const user = this.props.authData;
    const { requiredAttributes } = user.challengeParam;
    Auth.completeNewPassword(user, values.password, requiredAttributes)
      .then(user => {
        if (user.challengeName === "SMS_MFA") {
          this.props.onStateChange("confirmSignIn", user);
        } else {
          this.props.onStateChange("signedIn", user);
        }
      })
      .catch(err => this.props.ErrorStatusLogin(err));
  };
  handleSignIn = () => {
    this.props.onStateChange("signIn");
    this.props.ErrorStatusLogin(null);
  };
  onClose = () => {
    this.props.ErrorStatusLogin(null);
  };
  render() {
    const { errorUser } = this.props;
    if (this.props.authState !== "requireNewPassword") {
      return null;
    }
    return (
      <PaperLogin>
        <MarginLogin>
          <BorderLogo />

          {/* <Popover placement="leftTop" content={messagePassword}> */}
          <FormLogin>
            <RequireNewPasswordForm onSubmit={this.onSubmit} />
          </FormLogin>
          {/* </Popover> */}

          <ButtonLogin onClick={this.handleSignIn}>
            Retour à la page de connexion
          </ButtonLogin>
          {errorUser &&
            <AlertBox>
              <Alert
                message={I18n.get(errorUser.code)}
                description={
                  errorUser.message
                    ? I18n.get(errorUser.message)
                    : I18n.get(errorUser)
                }
                type="error"
                closable
                onClose={this.onClose}
              />
            </AlertBox>}
        </MarginLogin>
      </PaperLogin>
    );
  }
}
export default RequireNewPassword;
