import React from "react";
import { Field } from "react-final-form";
import { Input, Icon } from "antd";
import { SubmitButtonIconLabel, CancelButtonIconLabel } from "up-componup";
import {
  FormInput,
  InputAnt,
  SubmitButtonIconLabelAnt,
} from "./../../../loginStyle/loginStyle";

const renderActionButtons = (
  submitting,
  reset,
  pristine,
  onClickSubmit,
  onClickCancel,
) =>
  <div>
    <SubmitButtonIconLabelAnt
      disabled={submitting}
      onClick={onClickSubmit}
      color="primary"
    />
  </div>;

const Form = ({ handleSubmit, reset, submitting, pristine, values }) => {
  return (
    <div>
      <form onSubmit={handleSubmit}>
        <Field name="username">
          {({ input }) =>
            <FormInput>
              <InputAnt
                {...input}
                placeholder="Nom d'utilisateur"
                autoComplete="username"
                prefix={
                  <Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />
                }
              />
            </FormInput>}
        </Field>

        {renderActionButtons()}
      </form>
    </div>
  );
};

export default Form;
