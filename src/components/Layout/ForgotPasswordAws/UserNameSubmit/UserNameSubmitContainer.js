import { connect } from "react-redux";
import UserNameSubmit from "./UserNameSubmit";
import { ErrorStatusLogin } from "./../../../../redux/actions/user";
import { NotificationStatusLogin } from "./../../../../redux/actions/user";

const mapStateToProps = (state, ownProps) => {
  return {
    errorUser: state.user.errorUser,
    notifUser: state.user.notifUser,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    ErrorStatusLogin: data => {
      dispatch(ErrorStatusLogin(data));
    },
    NotificationStatusLogin: data => {
      dispatch(NotificationStatusLogin(data));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserNameSubmit);
