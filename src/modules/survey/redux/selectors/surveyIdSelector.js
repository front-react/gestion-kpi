import { createSelector } from "reselect";

const getSurveyList = (state, ownProps) =>
	state.survey.surveyManager.surveyList;

const getSurveyId = (state, ownProps) => ownProps.surveyId;

const surveyIdSelector = createSelector(
	[getSurveyList, getSurveyId],
	(surveyList, surveyId) => {
		console.log(surveyList);
		console.log(surveyId);
		// if (surveyList === []) {
		// 	return surveyList;
		// }
		let surveySelected = surveyList.filter(survey => {
			if (survey._id === surveyId) {
				return survey;
			}
		});

		return surveySelected;
	}
);

export default surveyIdSelector;
