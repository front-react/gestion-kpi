import { createSelector } from "reselect";
import _ from "lodash";
import {
	filter,
	keyBy,
	isPlainObject,
	forEach,
	zipObject,
	assign,
} from "lodash/fp";

const getSurveyManger = (state, ownProps) =>
	state.survey.surveyManager.surveyProgress;

const getLabelKpi = (state, ownProps) => ownProps.labelKpi;
const getLabelSelector = (state, ownProps) => ownProps.labelSelector;

const kpiLocationStatementSelector = createSelector(
	[getSurveyManger, getLabelKpi, getLabelSelector],
	(SurveyManger, LabelKpi, LabelSelector) => {
		let selectKpiByType = [];
		let indexStatement = 0;
		let indexKpi = 0;

		_.forEach(SurveyManger.locations, (location, index) => {
			if (location.label === LabelSelector) {
				indexStatement = index;
				_.forEach(
					SurveyManger.locations[indexStatement].statements,
					(statement, index) => {
						indexKpi = index;
						if (statement.label === LabelKpi) {
							selectKpiByType.push(
								SurveyManger.locations[indexStatement].statements[indexKpi].kpis
							);
						}
					}
				);
			}
		});

		return selectKpiByType;
	}
);

export default kpiLocationStatementSelector;
