import { createSelector } from "reselect";
import _ from "lodash";
import {
	filter,
	keyBy,
	isPlainObject,
	forEach,
	zipObject,
	assign,
} from "lodash/fp";

const getSurveyList = (state, ownProps) =>
	state.survey.surveyManager.surveyList;

const getStatusId = (state, ownProps) => ownProps.status;

const surveySelector = createSelector(
	[getSurveyList, getStatusId],
	(surveyList, statusId) => {
		if (statusId === 0) {
			return surveyList;
		}

		let surveySelected = surveyList.filter(survey => { 
			if(parseInt(survey.periodicity.status) === statusId) {
				return survey;
			}
		});

		return surveySelected;
	}
);

export default surveySelector;
