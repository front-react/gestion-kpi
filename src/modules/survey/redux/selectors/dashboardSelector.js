import { createSelector } from "reselect";
import _ from "lodash";
import { filter, keyBy, isPlainObject } from "lodash/fp";

const getDashboardList = (state, ownProps) =>
  state.survey.surveyManager.dashboardList;

const getDashboardId = (state, ownProps) => ownProps.match.params.id;

const dashboardSelector = createSelector(
  [getDashboardList, getDashboardId],
  (dashboardList, dashboardId) => {
    // if (dashboardList === []) {
    //   return [];
    // }

    // //let pdvSelected = pdvsList.find(item => item.pdvId === pdvId);
    let dashboardSelected = _.filter(dashboardList, [
      "id",
      parseInt(dashboardId),
    ]);
    //return dashboardSelected;
    let dashboard = dashboardSelected[0];
    return dashboard;

    // return pdv;
  },
);

export default dashboardSelector;
