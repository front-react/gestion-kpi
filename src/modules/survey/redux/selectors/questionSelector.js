import { createSelector } from "reselect";
import _ from "lodash";
import { filter, keyBy, isPlainObject } from "lodash/fp";

const getAllKpiQuestionsList = (state, ownProps) =>
  state.survey.surveyManager.questionsByKpi;

const getKpiId = (state, ownProps) => ownProps.idKpi;

const questionSelector = createSelector(
  [getAllKpiQuestionsList, getKpiId],
  (allKpiQuestionsList, kpiId) => {
    if (allKpiQuestionsList === []) {
      return [];
    }
    // //let pdvSelected = pdvsList.find(item => item.pdvId === pdvId);
    let questionsSelected = _.filter(allKpiQuestionsList, [
      "kpi",
      parseInt(kpiId),
    ]);
    //return dashboardSelected;
    //let dashboard = dashboardSelected[0];
    return questionsSelected;

    // return pdv;
  },
);

export default questionSelector;
