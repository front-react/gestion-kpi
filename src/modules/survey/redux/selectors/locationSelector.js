import { createSelector } from "reselect";
import _ from "lodash";
import { filter, keyBy, isPlainObject } from "lodash/fp";

const getSurveyComplet = (state, ownProps) =>
	state.survey.surveyManager.surveyProgress;

const getLabelSelector = (state, ownProps) => ownProps.labelSelector;

const locationSelector = createSelector(
	[getSurveyComplet, getLabelSelector],
	(surveyComplet, labelSelector) => {

		let locationSelect = _.filter(surveyComplet.locations, [
			"label",
			labelSelector,
		]);
		let location = locationSelect[0];
		return location;
	}
);

export default locationSelector;
