import { ADD_SURVEY_ELEMENT } from "./../../../types";
import { surveyApiUpsellProvider } from "./../../../../../../lib/api/providers";

const addElement = data => {

	return {
		prefixType: ADD_SURVEY_ELEMENT,
		callAPI: () => surveyApiUpsellProvider.post(`/viewer`, data),
	};
};

export default addElement;
