import { SURVEY_PROGRESS_STEPS } from "./../../../types";

const addStepSurvey = survey => {
	console.log(survey);
	return {
		prefixType: SURVEY_PROGRESS_STEPS,
		callAPI: survey,
	};
};

export default addStepSurvey;
