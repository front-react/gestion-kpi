import { ADD_SURVEY_ELEMENT } from "./../../../types";
import { surveyApiUpsellProvider } from "./../../../../../../lib/api/providers";

const createElement = data => {

	return {
		prefixType: ADD_SURVEY_ELEMENT,
		callAPI: () => surveyApiUpsellProvider.post(`/create`, data),
	};
};

export default createElement;
