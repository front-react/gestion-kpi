import { GET_RETAILER_BY_IDRETAILER_IDCLIENT } from "../../../types";
import { assortmentApiUpsellProvider } from "../../../../../../lib/api/providers";

const getByIdClient = (idClient) => {
	return {
		prefixType: GET_RETAILER_BY_IDRETAILER_IDCLIENT,
		callAPI: () =>
		assortmentApiUpsellProvider.get(
				`/findByIdClient/${idClient}`
			),
	};
};

export default getByIdClient;
