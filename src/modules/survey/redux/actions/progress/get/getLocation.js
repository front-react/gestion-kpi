import { GET_ALL_LOCATION } from "../../../types";
import { locationApiUpsellProvider } from "../../../../../../lib/api/providers";

const getLocation = (idClient, idRetailer) => {

	return {
		prefixType: GET_ALL_LOCATION,
		callAPI: () =>
		locationApiUpsellProvider.get(
				`/findByIdClientIdRetailer/${idClient}/${idRetailer}`
			),
	};
};

export default getLocation;
