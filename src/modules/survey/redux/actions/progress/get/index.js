import getmodelId from "./getmodelId";
import getLocation from "./getLocation";
import getByIdClient from "./getByIdClient";
import getSector from "./getSector";
export { getmodelId, getLocation, getByIdClient, getSector };
