import { GET_ALL_MODEL_BY_ID } from "./../../../types";
import { assortmentApiUpsellProvider } from "./../../../../../../lib/api/providers";

const getmodelId = (idClient) => {
	console.log(idClient);

	return {
		prefixType: GET_ALL_MODEL_BY_ID,
		callAPI: () =>
			assortmentApiUpsellProvider.get(
				`/findByIdClient/${idClient}`
			),
	};
};

export default getmodelId;
