import { UPDATE_SURVEY } from "./../../../types";
import { surveyApiUpsellProvider } from "./../../../../../../lib/api/providers";

const updateSurvey = data => {
	return {
		prefixType: UPDATE_SURVEY,
		callAPI: () => surveyApiUpsellProvider.put(`/updateSurveyStatus`, data),
	};
};

export default updateSurvey;
