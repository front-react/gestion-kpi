import { GET_SECTORS_LIST } from "./../../../types";
import { kpiApiUpsellProvider } from "./../../../../../../lib/api/providers";

const getSectorsList = () => {
  return {
    prefixType: GET_SECTORS_LIST,
    callAPI: () => kpiApiUpsellProvider.get(`/getSectorsList`),
  };
};

export default getSectorsList;
