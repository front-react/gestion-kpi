import { GET_SURVEY_LIST } from "./../../../types";
import { surveyApiUpsellProvider } from "./../../../../../../lib/api/providers";

const getSurveyList = () => {
  return {
    prefixType: GET_SURVEY_LIST,
    callAPI: () => surveyApiUpsellProvider.get(`/findAll`),
  };
};

export default getSurveyList;
