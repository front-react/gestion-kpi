import getKpiList from "./getKpiList";
import getQuestionsByKpi from "./getQuestionsByKpi";
import getAllKpiQuestions from "./getAllKpiQuestions";
import getSurveyList from "./getSurveyList";
import getSectorsList from "./getSectorsList";
import getSurveyEnseignesList from "./getSurveyEnseignesList";
import getSurveySectorsList from "./getSurveySectorsList";
import getSurveyModelesList from "./getSurveyModelesList";

export {
  getKpiList,
  getQuestionsByKpi,
  getAllKpiQuestions,
  getSurveyList,
  getSectorsList,
  getSurveyEnseignesList,
  getSurveySectorsList,
  getSurveyModelesList
};
