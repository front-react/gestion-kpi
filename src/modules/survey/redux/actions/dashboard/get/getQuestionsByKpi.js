import { GET_QUESTIONS_BY_KPI } from "./../../../types";
import { kpiApiUpsellProvider } from "./../../../../../../lib/api/providers";

const getQuestionsByKpi = idKpi => {
  return {
    prefixType: GET_QUESTIONS_BY_KPI,
    callAPI: () => kpiApiUpsellProvider.get(`/getQuestionsByKpiId/${idKpi}`),
    payload: { idKpi },
  };
};

export default getQuestionsByKpi;
