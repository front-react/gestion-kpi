import { GET_SURVEY_MODELES_LIST } from "../../../types";

const getSurveyModelesList = (modelesList) => {
  return {
    prefixType: GET_SURVEY_MODELES_LIST,
    callAPI: modelesList,
  };
};

export default getSurveyModelesList;
