import { GET_SURVEY_SECTORS_LIST } from "./../../../types";

const getSurveySectorsList = (sectorList) => {
  return {
    prefixType: GET_SURVEY_SECTORS_LIST,
    callAPI: sectorList,
  };
};

export default getSurveySectorsList;
