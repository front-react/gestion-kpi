import { GET_KPIS_LIST } from "./../../../types";
import { kpiApiUpsellProvider } from "./../../../../../../lib/api/providers";

const getKpiList = () => {
  return {
    prefixType: GET_KPIS_LIST,
    callAPI: () => kpiApiUpsellProvider.get(`/findAll`),
  };
};

export default getKpiList;
