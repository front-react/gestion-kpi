import { GET_SURVEY_ENSEIGNES_LIST } from "../../../types";

const getSurveyEnseignesList = enseigneList => {
	return {
		prefixType: GET_SURVEY_ENSEIGNES_LIST,
		callAPI: enseigneList,
	};
};

export default getSurveyEnseignesList;
