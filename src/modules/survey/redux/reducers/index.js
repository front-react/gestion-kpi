import { combineReducers } from "redux";
import surveyReducers from "./surveyReducers";

const surveyManagerdReducers = combineReducers({
  surveyManager: surveyReducers,
});

export default surveyManagerdReducers;
