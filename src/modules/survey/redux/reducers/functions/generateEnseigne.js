import _ from "lodash";
import { assign, forEach, zipObject } from "lodash/fp";

export default function generateEnseigne(survey) {
	let surveyList = survey;
	let constuctEnseigneList = [];
	_.forEach(surveyList, (survey, surveyIndex) => {
		constuctEnseigneList.push(survey.sectors);
	});

	_.forEach(constuctEnseigneList, (sector, sectorIndex) => {
		_.forEach(sector, (selectSector, selectSectorIndex) => {
			_.assign(
				constuctEnseigneList[sectorIndex][selectSectorIndex],
				{ periodicity: surveyList[sectorIndex].periodicity },
				{
					periodicityAssortment: surveyList[sectorIndex].periodicityAssortment,
				},
				{
					modele:
						typeof surveyList[sectorIndex].retailer === Object
							? surveyList[sectorIndex].retailer
							: surveyList[sectorIndex].retailer,
				},
				{
					enseigne:
						typeof surveyList[sectorIndex].retailer === Object
							? surveyList[sectorIndex].retailer
							: surveyList[sectorIndex].retailer,
				},
				{ kpi: [] }
			);
		});
	});

	const assignKpiForSurvey = (kpiList, indexSurvey) => {
		_.forEach(constuctEnseigneList[indexSurvey], (secteur, indexSecteur) => {
			_.forEach(kpiList, (kpi, index) => {
				constuctEnseigneList[indexSurvey][indexSecteur].kpi.push(kpi);
			});
		});
	};

	_.forEach(surveyList, (survey, surveyIndex) => {
		_.forEach(survey.metier.locationTypes, (location, index) => {
			_.forEach(location.statements, (statement, index) => {
				assignKpiForSurvey(statement.kpis, surveyIndex);
			});
		});
	});

	let secteurProgress = [];
	_.forEach(constuctEnseigneList, (secteur, index) => {
		_.forEach(secteur, (selectSecteur, index) => {
			secteurProgress.push(selectSecteur);
		});
	});

	return secteurProgress;
}
