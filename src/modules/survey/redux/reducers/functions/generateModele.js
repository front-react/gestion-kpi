import _ from "lodash";
import { assign, forEach, zipObject } from "lodash/fp";

export default function generateModele(survey, sector) {
	let sectorList = sector;
	let surveyList = survey;
	let constuctModeleList = [];
	_.forEach(surveyList, (survey, surveyIndex) => {
		constuctModeleList.push(survey.retailer);
	});

	_.forEach(constuctModeleList, (enseigne, modeleIndex) => {
		_.assign(
			constuctModeleList[modeleIndex],
			{
				periodicityAssortment: surveyList[modeleIndex].periodicityAssortment,
			},
			{
				modele:
					typeof surveyList[modeleIndex].retailer === Object
						? surveyList[modeleIndex].retailer
						: surveyList[modeleIndex].retailer,
			},
			{
				enseigne:
					typeof surveyList[modeleIndex].retailer === Object
						? surveyList[modeleIndex].retailer
						: surveyList[modeleIndex].retailer,
			},
			{ kpi: [] },
			{ sector: [] },
			{ survey: surveyList[modeleIndex].periodicity.label }
		);
	});

	const assignKpiForSurvey = (kpiList, indexSurvey) => {
		_.forEach(constuctModeleList[indexSurvey], (secteur, indexSecteur) => {
			_.forEach(kpiList, (kpi, index) => {
				constuctModeleList[indexSurvey].kpi.push(kpi);
			});
		});
	};

	_.forEach(surveyList, (survey, surveyIndex) => {
		_.forEach(survey.metier.locationTypes, (location, index) => {
			_.forEach(location.statements, (statement, index) => {
				assignKpiForSurvey(statement.kpis, surveyIndex);
			});
		});
	});

	_.forEach(surveyList, (sector, index) => {
		let randomSector =
			sectorList[Math.floor(Math.random() * Math.floor(sectorList.length))];
		_.assign(constuctModeleList[index], {
			sector: [randomSector],
		});
	});

	console.log(constuctModeleList);

	let secteurProgress = [];
	_.forEach(constuctModeleList, (secteur, index) => {
		_.forEach(secteur, (selectSecteur, index) => {
			secteurProgress.push(selectSecteur);
		});
	});

	return secteurProgress;
}
