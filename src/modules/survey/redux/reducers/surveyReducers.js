import {
	GET_KPIS_LIST,
	GET_QUESTIONS_BY_KPI,
	GET_ALL_KPI_QUESTIONS_LIST,
	GET_SURVEY_LIST,
	SURVEY_PROGRESS_STEPS,
	ADD_SURVEY_ELEMENT,
	GET_ALL_MODEL_BY_ID,
	GET_RETAILER_BY_IDRETAILER_IDCLIENT,
	GET_ALL_LOCATION,
	GET_ALL_SECTOR,
	GET_SURVEY_SECTORS_LIST,
	GET_SURVEY_ENSEIGNES_LIST,
	GET_SURVEY_MODELES_LIST,
	UPDATE_SURVEY
} from "../types";
import { secteurList, modeleList, enseigneList, sector } from "./fakeDate";

import store from "store";
import {
	createReducer,
	createApiHandlers,
	//createAddItemsInArrayWithFilter,
	//createAddItemInArray,
	//createDeleteItemInArray,
} from "./../../../../redux/reducers/functions";

import { generateSector, generateEnseigne, generateModele } from "./functions";

import _ from "lodash";
import { assign, forEach, zipObject } from "lodash/fp";

const initialState = {
	surveyProgress: {},
	surveySectorsList: [],
	surveyList: [],
	allKpiQuestions: [],
	questionsByKpi: [],
	modeleList: [],
	currentSurvey: {},
	locatonList: [],
	surveyModelesList: [],
	surveyEnseigneList: [],
	assortmentClient: [],
	sectorList: [],
	secteurList: [],
	surveySectorsList: [],
};

const getKpisHandlers = createApiHandlers(GET_KPIS_LIST, (state, action) => {
	return { ...state, kpisList: action.payload.data };
});
const getquestionsByKpiHandlers = createApiHandlers(
	GET_QUESTIONS_BY_KPI,
	(state, action) => {
		return { ...state, questionsByKpi: action.payload.data };
	}
);
const getAllKpiquestionsHandlers = createApiHandlers(
	GET_ALL_KPI_QUESTIONS_LIST,
	(state, action) => {
		return { ...state, allKpiQuestions: action.payload.data };
	}
);

const updateSurveyHandlers = createApiHandlers(
	UPDATE_SURVEY,
	(state, action) => {
		return { ...state, surveyList: action.payload.data };
	}
);

const getAllSurveyHandlers = createApiHandlers(
	GET_SURVEY_LIST,
	(state, action) => {
		if (action.payload.data !== null) {
			let secteurList = sector;
			let surveyList = action.payload.data;
			//store.remove("surveyList");

			// _.forEach(surveyList, (sector, index) => {
			// 	let randomSector =
			// 		secteurList[
			// 			Math.floor(Math.random() * Math.floor(secteurList.length))
			// 		];
			// 	_.assign(surveyList[index], {
			// 		sectors: [randomSector],
			// 	});
			// });

			//store.set("surveyList", surveyList);
			return {
				...state,
				//surveyList: store.get("surveyList"),
				surveyList: surveyList,
				secteurList: generateSector(surveyList),
				//enseigneList: generateEnseigne(surveyList),
				modeleList: generateModele(surveyList, sector),
			};
		}
	}
);

const getModelsSurveyHandlers = createApiHandlers(
	GET_ALL_MODEL_BY_ID,
	(state, action) => {
		return { ...state, modeleList: action.payload.data };
	}
);
const updateProgressSurveyHandlers = createApiHandlers(
	SURVEY_PROGRESS_STEPS,
	(state, action) => {
		return { ...state, surveyProgress: action.payload.data };
	}
);

const simulationSurveyHandlers = createApiHandlers(
	ADD_SURVEY_ELEMENT,
	(state, action) => {
		if (action.payload.data !== null) {
			return { ...state, currentSurvey: action.payload.data };
		}
	}
);
const locationsSurveyHandlers = createApiHandlers(
	GET_ALL_LOCATION,
	(state, action) => {
		return { ...state, locatonList: action.payload.data };
	}
);

const assortmentClientSurveyHandlers = createApiHandlers(
	GET_RETAILER_BY_IDRETAILER_IDCLIENT,
	(state, action) => {
		return { ...state, assortmentClient: action.payload.data };
	}
);
const sectorSurveyHandlers = createApiHandlers(
	GET_ALL_SECTOR,
	(state, action) => {
		return { ...state, sectorList: action.payload.data };
	}
);

const surveySectorListHandlers = createApiHandlers(
	GET_SURVEY_SECTORS_LIST,
	(state, action) => {
		return { ...state, surveySectorsList: action.payload.data };
	}
);

const surveyEnseignesListHandlers = createApiHandlers(
	GET_SURVEY_ENSEIGNES_LIST,
	(state, action) => {
		return { ...state, surveyEnseigneList: action.payload.data };
	}
);

const surveyModeleListHandlers = createApiHandlers(
	GET_SURVEY_MODELES_LIST,
	(state, action) => {
		return { ...state, surveyModelesList: action.payload.data };
	}
);

const surveyReducers = createReducer(initialState, {
	...getKpisHandlers,
	...getquestionsByKpiHandlers,
	...getAllKpiquestionsHandlers,
	...getAllSurveyHandlers,
	...updateProgressSurveyHandlers,
	...simulationSurveyHandlers,
	...getModelsSurveyHandlers,
	...assortmentClientSurveyHandlers,
	...locationsSurveyHandlers,
	...sectorSurveyHandlers,
	...surveySectorListHandlers,
	...surveyEnseignesListHandlers,
	...surveyModeleListHandlers,
	...updateSurveyHandlers
});
export default surveyReducers;
