export const secteurList = [
	{
		id: "15",
		secteurName: "Secteur 1",
		surveyName: "GSK 2016",
		periodicity: { status: 1 },
		beginCreation: "04/12/2018",
		endCreation: "04/12/2018",
		begin: "01/04/2016",
		end: "01/06/2016",
		modele: [
			{
				id: "1",
				label: "CARREFFOUR 2017",
			},
			{
				id: "4",
				label: "INTERMACHE 2019",
			},
		],
		kpi: [
			{
				id: "4",
				label: "FACING",
			},
			{
				id: "4",
				label: "DN",
			},
		],
		enseigne: [
			{
				label: "CARREFOUR",
			},
			{
				label: "INTERMARCHE",
			},
			{
				label: "FRANPRIX",
			},
		],
	},
];

export const modeleList = [
	{
		id: "23",
		modeleName: "modele 1",
		beginCreation: "04/12/2018",
		endCreation: "04/12/2018",
		enseigne: [
			{
				label: "enseigne 8",
			},
			{
				label: "enseigne 10",
			},
			{
				label: "enseigne 14",
			},
		],
		secteur: [
			{
				label: "secteur 2",
			},
			{
				label: "secteur 45",
			},
		],
		survey: [
			{
				label: "Survey 12",
			},
			{
				label: "Survey 49",
			},
		],
		kpi: [
			{
				label: "kpi 45",
			},
			{
				label: "kpi 25",
			},
		],
	},
	{
		id: "33",
		modeleName: "modele 2",
		beginCreation: "04/12/2018",
		endCreation: "04/12/2018",
		enseigne: [
			{
				label: "enseigne 8",
			},
			{
				label: "enseigne 10",
			},
			{
				label: "enseigne 14",
			},
		],
		secteur: [
			{
				label: "secteur 2",
			},
			{
				label: "secteur 45",
			},
		],
		survey: [
			{
				label: "Survey 12",
			},
			{
				label: "Survey 49",
			},
		],
		kpi: [
			{
				label: "kpi 45",
			},
			{
				label: "kpi 25",
			},
		],
	},
];

export const enseigneList = [
	{
		id: "69",
		enseigneName: "Enseigne 1",
		secteur: [
			{
				label: "secteur 2",
			},
			{
				label: "secteur 45",
			},
		],
		survey: [
			{
				label: "Survey 12",
			},
			{
				label: "Survey 49",
			},
		],
		modele: [
			{
				label: "modele 1",
			},
		],
		kpi: [
			{
				label: "kpi 45",
			},
			{
				label: "kpi 25",
			},
		],
	},
	{
		id: "69",
		enseigneName: "Enseigne 1",
		secteur: [
			{
				id: "15",
				label: "secteur 2",
			},
			{
				id: "7",
				label: "secteur 45",
			},
		],
		survey: [
			{
				label: "Survey 12",
			},
			{
				label: "Survey 49",
			},
		],
		modele: [
			{
				label: "modele 1",
			},
		],
		kpi: [
			{
				label: "kpi 45",
			},
			{
				label: "kpi 25",
			},
		],
	},
];

export const sector = [
	{
		idSector: 736,
		sectorName: "BORDEAUX",
		workers: 1,
		missionNumber: 1,
		cdncNumber: 159,
	},
	{
		idSector: 918,
		sectorName: "BORDEAUX NON COUVERT",
		workers: 0,
		missionNumber: 0,
		cdncNumber: 99,
	},
	{
		idSector: 737,
		sectorName: "CAEN",
		workers: 1,
		missionNumber: 1,
		cdncNumber: 161,
	},
	{
		idSector: 919,
		sectorName: "CAEN NON COUVERT",
		workers: 0,
		missionNumber: 0,
		cdncNumber: 123,
	},
	{
		idSector: 738,
		sectorName: "LILLE",
		workers: 1,
		missionNumber: 1,
		cdncNumber: 162,
	},
	{
		idSector: 720,
		sectorName: "LILLE NON COUVERT",
		workers: 0,
		missionNumber: 0,
		cdncNumber: 158,
	},
	{
		idSector: 739,
		sectorName: "LYON",
		workers: 0,
		missionNumber: 0,
		cdncNumber: 160,
	},
	{
		idSector: 721,
		sectorName: "LYON NON COUVERT",
		workers: 1,
		missionNumber: 1,
		cdncNumber: 78,
	},
	{
		idSector: 740,
		sectorName: "MARSEILLE",
		workers: 1,
		missionNumber: 1,
		cdncNumber: 161,
	},
	{
		idSector: 722,
		sectorName: "MARSEILLE NON COUVERT",
		workers: 0,
		missionNumber: 0,
		cdncNumber: 56,
	},
];
