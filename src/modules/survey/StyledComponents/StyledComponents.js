import styled from "styled-components";
import { Card } from "antd";

export const Title = styled.p`
	font-size: 40px;
	color: black;
	padding-left: 20px;
	margin-top: 40px;
`;

export const TableKpi = styled.table`
	overflow: hidden;
	&& {
		td,
		th {
			padding: 10px;
			position: relative;
			outline: 0;
		}

		tbody tr:hover {
			background-color: rgba(170, 57, 57, 0.6);
		}

		td:hover::after,
		thead th:not(:empty):hover::after,
		td:focus::after,
		thead th:not(:empty):focus::after {
			content: "";
			height: 10000px;
			left: 0;
			position: absolute;
			top: -5000px;
			width: 100%;
			z-index: 0;
		}

		td:hover::after,
		th:hover::after {
			/*background-color: ;*/
			border: 5px solid rgb(170, 57, 57);
		}

		td:focus::after,
		th:focus::after {
			background-color: lightblue;
		}

		/* Focus stuff for mobile */
		td:focus::before,
		tbody th:focus::before {
			background-color: lightblue;
			content: "";
			height: 100%;
			top: 0;
			left: -5000px;
			position: absolute;
			width: 10000px;
			z-index: -1;
		}
	}
`;

export const TitleStep = styled.p`
	font-size: 20px;
	color: ${props => props.theme.colors.default};
	margin-bottom: ${props => (props.marginBottom ? props.marginBottom : "20px")};
	padding-top: 10px;
	padding-bottom: ${props =>
		props.paddingBottom ? props.paddingBottom : "10px"};
	text-transform: uppercase;
	text-align: center;
	border-bottom: ${props =>
		props.borderBottom ? props.borderBottom : "1px solid rgb(200, 200, 200);"};
`;
export const SubTitleStep = styled.p`
	font-size: 10px;
	padding-bottom: 5px;
	text-align: center;
	font-style: italic;
	border-bottom: 1px solid rgb(200, 200, 200);
`;

export const SubTitle = styled.p`
	font-size: ${props => (props.size ? props.size : "16px")};
	color: ${props => (props.color ? props.color : "black")};
	padding-left: ${props => (props.paddingLeft ? props.paddingLeft : "20px")};
	margin-top: ${props => (props.marginTop ? props.marginTop : "40px")};
	margin-bottom: ${props => (props.marginBottom ? props.marginBottom : "20px")};
`;

export const Content = styled.div`
	padding: 5px;
	margin-top: 40px;
`;

export const SpanLocation = styled.span`
	text-decoration: ${props => (props.decoration ? props.decoration : "none")};
	font-style: ${props => (props.fontStyle ? props.fontStyle : "normal")};
	font-size: 15px;
`;

export const ContentCard = styled.div`
	margin-top: ${props => (props.marginTop ? props.marginTop : "5px")};
	padding-left: ${props => (props.paddinSumm ? props.paddinSumm : "20px")};
`;

export const StepCard = styled(Card)`
	&& {
		margin: ${props => (props.margin ? props.margin : "40px auto 0 auto")};
		width: ${props => (props.width ? props.width : "80%")};
		max-width: ${props => (props.maxWidth ? props.maxWidth : "400px")};
		border-top: 4px solid ${props => props.theme.colors.primary} !important;
		height: ${props => (props.maxHeight ? props.maxHeight : "300px")};
		text-align: ${props => (props.align ? props.align : "left")};
		box-shadow: 3px 3px 5px 0 rgba(170, 170, 170, 0.5);
	}
`;

export const StepDiv = styled.div`
	margin: ${props =>
		props.margin ? props.margin : "40px auto 0 auto !important"};
	width: 80%;
	max-width: ${props => (props.maxWidth ? props.maxWidth : "600px")};
	min-height: 300px;
	text-align: ${props => (props.align ? props.align : "left")};
`;
export const StepCardAction = styled.div`
	background-color: transparent !important;
	max-width: 400px;
	margin: auto;
	border-left: none;
	border-right: none;
	border-bottom: none;
	padding: 0px 2% 0px 2%;
`;

export const InfosMetier = styled.div`
	background-color: rgb(245, 245, 245);
	padding: 5px;
	margin-top: 10px;
`;

export const Label = styled.span`
	font-size: 12px;
	font-weight: ${props => props.weight};
`;

export const ContentDiv = styled.div`
	margin-top: 20px;
	margin-bottom: 40px;
`;

export const ContentMap = styled.div`
	margin-top: 10px;
`;

export const Divider = styled.div`
	display: block;
	height: 1px;
	width: 100%;
	margin: 10px 0;
	background-color: rgb(200, 200, 200);
`;

export const PaperApp = styled.div`
	margin: 0px 45px 20px 45px;
`;

export const InputField = styled.input`
	width: 100%;
	padding: 8px 20px;
	margin: 8px 0;
	display: inline-block;
	border: 1px solid #ccc;
	border-radius: 4px;
	box-sizing: border-box;
`;

export const InputSubmitOutline = styled.input`
	width: 100%;
	color: ${props => props.theme.colors.primary};
	background-color: white;
	padding: 14px 10px;
	margin: 20px 0 8px 0;
	border: 1px solid ${props => props.theme.colors.primary};
	border-radius: 4px;
	cursor: pointer;
	&:hover {
		background-color: ${props => props.theme.colors.primary};
		color: white;
	}
`;
export const InputSubmit = styled.input`
	background-color: ${props => props.theme.colors.primary};
	color: white;
	padding: 10px 25px;
	border: 1px solid ${props => props.theme.colors.primary};
	border-radius: 4px;
	margin: ${props => (props.margin ? props.margin : "auto 0 0 auto")};
	cursor: pointer;
	&:hover {
		background-color: #fba123;
		color: white;
	}
	z-index: 1;
`;
export const InputFile = styled.input`
	opacity: 0;
	position: absolute;
	height: 40px;
	left: 0;
	&:hover {
		cursor: pointer;
	}
`;
export const InputFileLabel = styled.label`
	position: relative;
	width: 100%;
	color: ${props => props.theme.colors.primary};
	background-color: white;
	padding: 7px 15px;
	border: 1px solid ${props => props.theme.colors.primary};
	border-radius: 4px;
	&:hover {
		cursor: pointer;
		background-color: ${props => props.theme.colors.primary};
		color: white;
	}
`;

export const DraggableItem = styled.li`
	padding: 3px 6px;
	border-radius: 5px;
	border: 1px solid #e6e6e6;
	text-align: center;
	margin: 8px 0;
	&:hover {
		cursor: pointer;
	}
`;
