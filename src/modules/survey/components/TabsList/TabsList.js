import React from "react";
import { Tabs, Icon } from "antd";

const ContentTab = ({ tabsList, position, typeTab, ActiveKey, ...rest }) => {
	return (
		<div>
			<Tabs
				activeKey={ActiveKey}
				tabPosition={position}
				type={typeTab}
				{...rest}
			>
				{tabsList.map((tabs, index) => (
					<Tabs.TabPane
						tab={
							<span>
								{tabs.icon && tabs.icon}
								{tabs.label && tabs.label}
							</span>
						}
						key={index}
						style={{ padding: "5px" }}
					>
						{tabs.component}
					</Tabs.TabPane>
				))}
			</Tabs>
		</div>
	);
};
ContentTab.defaultProps = {
	typeTab: "card",
	position: "top",
	ActiveKey: "0",
	tabsList: [
		{
			label: "Label Tab",
			component: "<ComponentName />",
		},
	],
};

class TabsList extends React.Component {
	componentDidMount() {}

	render() {
		const {
			tabsList,
			position,
			typeTab,
			ActiveKey,
			type,
			...rest
		} = this.props;
		return this.props.render({
			ContentTab: (
				<ContentTab
					tabsList={tabsList}
					position={position}
					typeTab={typeTab}
					ActiveKey={ActiveKey}
					type={type}
					{...rest}
				/>
			),
		});
	}
}

export default TabsList;
