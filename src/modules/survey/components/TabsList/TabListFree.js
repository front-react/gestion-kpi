import React from "react";
import { Tabs, Icon } from "antd";

import { StepCard } from "./../../StyledComponents/StyledComponents";

const ContentTab = ({
	tabsList,
	position,
	typeTab,
	ActiveKey,
	tabBarStyle,
	...rest
}) => {
	return (
		<div>
			<Tabs
				//activeKey={ActiveKey}
				tabPosition={position}
				type={typeTab}
				tabBarStyle={tabBarStyle}
				{...rest}
			>
				{tabsList.map((tabs, index) => (
					<Tabs.TabPane
						tab={
							<span>
								{tabs.icon && tabs.icon}
								{tabs.label && tabs.label}
							</span>
						}
						key={index}
					>
						<StepCard margin="0!important" maxWidth="none" width="100%">
							{tabs.component}
						</StepCard>
					</Tabs.TabPane>
				))}
			</Tabs>
		</div>
	);
};
ContentTab.defaultProps = {
	typeTab: "card",
	position: "top",
	ActiveKey: "0",
	tabsList: [
		{
			label: "Label Tab",
			component: "<ComponentName />",
		},
	],
};

class TabListFree extends React.Component {
	componentDidMount() {}

	render() {
		const {
			tabsList,
			position,
			typeTab,
			ActiveKey,
			tabBarStyle,
			...rest
		} = this.props;
		return this.props.render({
			ContentTab: (
				<ContentTab
					tabsList={tabsList}
					position={position}
					typeTab={typeTab}
					ActiveKey={ActiveKey}
					tabBarStyle={tabBarStyle}
					{...rest}
				/>
			),
		});
	}
}

export default TabListFree;
