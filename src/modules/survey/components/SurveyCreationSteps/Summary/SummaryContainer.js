import { connect } from "react-redux";
import Summary from "./Summary";
//import { surveySelector } from "./../../redux/selectors";
import { addStepSurvey } from "./../../../redux/actions/progress/post";

const mapStateToProps = (state, ownProps) => {
	return {
		surveyProgress: state.survey.surveyManager.surveyProgress,
		secteurs: state.survey.surveyManager.surveyProgress.secteurs,
		locations: state.survey.surveyManager.surveyProgress.locations,
		model: state.survey.surveyManager.surveyProgress.model,
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		addStepSurvey: survey => {
			dispatch(addStepSurvey(survey));
		},
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Summary);
