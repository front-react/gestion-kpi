import React, { Component, Fragment } from "react";
import { Card, Divider } from "antd";
import {
	SubTitle,
	ContentCard,
	SpanLocation,
} from "./../../../StyledComponents/StyledComponents";
import LocationAction from "./LocationAction";
import _ from "lodash";
import { filter, keyBy, isPlainObject, isEmpty } from "lodash/fp";
import { defaultProps } from "recompose";
import moment from "moment";

class Summary extends Component {
	render() {
		const { surveyProgress, secteurs, locations, model } = this.props;

		return (
			<Card
				style={{
					borderTop: "4px solid #CC5D0A",
					height: "650px",
					overflowY: "auto",
					minHeight: "90vh",
					height: "103vh",
				}}
				bordered={false}
			>
				<div
					style={{
						textAlign: "center",
						color: "#CC5D0A",
						fontSize: "20px",
						paddingTop: "15px",
						paddingBottom: "15px",
						borderBottom: "1px solid rgb(200,200,200)",
					}}
				>
					RECAPITULATIF
				</div>
				<SubTitle color={"#CC5D0A"}>Modèle :</SubTitle>
				{_.isEmpty(model) ? (
					<ContentCard>Ancun</ContentCard>
				) : (
					<ContentCard>
						{model.map((model, index) => {
							return (
								<Fragment>{`${model.retailerName}${
									surveyProgress.model.length - 1 !== index ? "," : ""
								} `}</Fragment>
							);
						})}
					</ContentCard>
				)}

				<Divider type="horizontal" />
				<SubTitle color={"#CC5D0A"}>Période :</SubTitle>
				{_.isEmpty(surveyProgress.periodicity) ? (
					<ContentCard>Ancun</ContentCard>
				) : (
					<Fragment>
						<ContentCard>
							<span>
								{moment(
									`${surveyProgress.periodicity.startDate}`,
									"YYYY-MM-DDTHH:mm:ss.SSSSZ"
								).format("DD/MM/YYYY")}
							</span>
							{` - `}
							<span>
								{moment(
									`${surveyProgress.periodicity.endDate}`,
									"YYYY-MM-DDTHH:mm:ss.SSSSZ"
								).format("DD/MM/YYYY")}
							</span>
						</ContentCard>
					</Fragment>
				)}

				<Divider type="horizontal" />
				<SubTitle color={"#CC5D0A"}>Emplacement :</SubTitle>
				{_.isEmpty(locations) ? (
					<ContentCard>Ancun</ContentCard>
				) : (
					<ContentCard>
						{locations.map((location, index) => {
							return (
								<Fragment>{`${
									location.label === "Lineaire" ? "Linéaire" : location.label
								}${locations.length - 1 !== index ? "," : ""} `}</Fragment>
							);
						})}
					</ContentCard>
				)}

				<Divider type="horizontal" />
				<SubTitle color={"#CC5D0A"}>Actions :</SubTitle>
				{_.isEmpty(locations) ? (
					<ContentCard>Ancun</ContentCard>
				) : (
					<ContentCard>
						{locations.map((location, index) => {
							return (
								<Fragment>
									<span key={location.label}>
										<strong>{`${location.label}`}</strong>
										<ContentCard>
											{location.statements.map((statement, index) => {
												return (
													<Fragment>
														<div key={statement.label}>
															<div>
																<SpanLocation decoration={"underline"}>{`${
																	statement.label === "releve"
																		? "Relevé"
																		: statement.label === "plv"
																			? "PLV"
																			: statement.label === "implantation"
																				? "Implantation"
																				: statement.label === "mea"
																					? "MEA"
																					: statement.label
																} `}</SpanLocation>
																<span>: </span>
																{statement.kpis.map((kpi, index) => (
																	<Fragment>
																		<span>{`${kpi.label}${
																			statement.kpis.length - 1 !== index
																				? ","
																				: ""
																		} `}</span>
																	</Fragment>
																))}
															</div>
															<div>
																<SpanLocation
																	fontStyle={"italic"}
																>{`Médias : `}</SpanLocation>
																{statement.medias.map((media, index) => (
																	<Fragment>
																		<span>{`${media.quantity} ${
																			media.type === "Photo"
																				? "Photo(s)"
																				: media.type === "Video"
																					? "Vidéo(s)"
																					: media.type === "Audio"
																						? "Audio(s)"
																						: media.type
																		}${
																			statement.medias.length - 1 !== index
																				? ","
																				: ""
																		} `}</span>
																	</Fragment>
																))}
															</div>
														</div>
													</Fragment>
												);
											})}
										</ContentCard>
									</span>
									<Divider type="horizontal" />
								</Fragment>
							);
						})}
					</ContentCard>
				)}
				<Divider type="horizontal" />
				<SubTitle color={"#CC5D0A"}>Secteurs :</SubTitle>
				{_.isEmpty(locations) ? (
					<ContentCard>Ancun</ContentCard>
				) : (
					<ContentCard>
						{secteurs.map((secteur, index) => {
							return (
								<span key={secteur.label}>{`${secteur.label}${
									secteurs.length - 1 !== index ? "," : ""
								} `}</span>
							);
						})}
					</ContentCard>
				)}
			</Card>
		);
	}
}

// Summary.defaultProps = {
// 	surveyProgress: {
// 		statusProgress: false,
// 		stepCreation: 0,
// 		assortments: ["5b913277f0c13e5bbc77a7cf"],
// 		periodicity: {
// 			label: "period GSK",
// 			startDate: "15/09/2018",
// 			endDate: "15/10/2018",
// 		},
// 		model: [
// 			{
// 				label: "Auchan",
// 				startDate: "22/05/2015",
// 				endDate: "21/08/2018",
// 			},
// 			{
// 				label: "Super U",
// 				startDate: "22/05/2015",
// 				endDate: "21/08/2018",
// 			},
// 			{
// 				label: "Carrefour",
// 				startDate: "22/05/2015",
// 				endDate: "21/08/2018",
// 			},
// 		],
// 		locations: [
// 			{
// 				label: "Lineaire",
// 				locationType: "L",
// 				statements: [
// 					{
// 						id: 0,
// 						label: "releve",
// 						kpis: [
// 							{
// 								label: "GAIN DE REFERENCES",
// 								_id: "5b98c1c74fecf600140c9ebe",
// 								question: "Quelle est le gain de références ?",
// 							},
// 							{
// 								label: "RESPECT DU NB DE VISITES",
// 								_id: "5b98c1c74fecf600140c9ecb",
// 								question: "Respect du nombre de visites",
// 							},
// 						],
// 						medias: [
// 							{
// 								type: "Photo",
// 								quantity: "1",
// 							},
// 							{
// 								type: "Video",
// 								quantity: "5",
// 							},
// 							{
// 								type: "Audio",
// 								quantity: "1",
// 							},
// 						],
// 					},
// 					{
// 						id: 4,
// 						label: "plv",
// 						kpis: [
// 							{
// 								label: "FACING",
// 								_id: "5b98c1c74fecf600140c9ec1",
// 								question: "Quel est le nombe de facings ?",
// 							},
// 							{
// 								label: "NB DE VISITES",
// 								_id: "5b98c1c74fecf600140c9ec9",
// 								question: "Quel est le nombre de visites",
// 							},
// 							{
// 								label: "PSR",
// 								_id: "5b98c1c74fecf600140c9ed3",
// 								question: "PSR",
// 							},
// 						],
// 						medias: [
// 							{
// 								type: "Photo",
// 								quantity: "1",
// 							},
// 							{
// 								type: "Video",
// 								quantity: "2",
// 							},
// 							{
// 								type: "Audio",
// 								quantity: "8",
// 							},
// 						],
// 					},
// 				],
// 			},
// 			{
// 				label: "Ambiant",
// 				locationType: "A",
// 				statements: [
// 					{
// 						id: 0,
// 						label: "releve",
// 						kpis: [
// 							{
// 								label: "GAIN DE REFERENCES",
// 								_id: "5b98c1c74fecf600140c9ebe",
// 								question: "Quelle est le gain de références ?",
// 							},
// 							{
// 								label: "RESPECT DU NB DE VISITES",
// 								_id: "5b98c1c74fecf600140c9ecb",
// 								question: "Respect du nombre de visites",
// 							},
// 						],
// 						medias: [
// 							{
// 								type: "Photo",
// 								quantity: "1",
// 							},
// 							{
// 								type: "Video",
// 								quantity: "5",
// 							},
// 							{
// 								type: "Audio",
// 								quantity: "1",
// 							},
// 						],
// 					},
// 					{
// 						id: 4,
// 						label: "plv",
// 						kpis: [
// 							{
// 								label: "FACING",
// 								_id: "5b98c1c74fecf600140c9ec1",
// 								question: "Quel est le nombe de facings ?",
// 							},
// 							{
// 								label: "NB DE VISITES",
// 								_id: "5b98c1c74fecf600140c9ec9",
// 								question: "Quel est le nombre de visites",
// 							},
// 							{
// 								label: "PSR",
// 								_id: "5b98c1c74fecf600140c9ed3",
// 								question: "PSR",
// 							},
// 						],
// 						medias: [
// 							{
// 								type: "Photo",
// 								quantity: "1",
// 							},
// 							{
// 								type: "Video",
// 								quantity: "2",
// 							},
// 							{
// 								type: "Audio",
// 								quantity: "8",
// 							},
// 						],
// 					},
// 				],
// 			},
// 		],
// 	},
// };

export default Summary;
