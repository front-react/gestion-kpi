import { connect } from "react-redux";
import SurveyCreationSteps from "./SurveyCreationSteps";
//import { surveySelector } from "./../../redux/selectors";
import { getByIdClient } from "./../../redux/actions/progress/get";
import { addStepSurvey } from "./../../redux/actions/progress/post";

const mapStateToProps = (state, ownProps) => {
	return {
		//currentSurvey: surveySelector(state, ownProps),
		//surveyProgress: state.survey.surveyManager.surveyProgress,
		locations: state.survey.surveyManager.locations,
		assortmentClient: state.survey.surveyManager.assortmentClient,
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		getByIdClient: idClient => {
			dispatch(getByIdClient(idClient));
		},
		addStepSurvey: survey => {
			dispatch(addStepSurvey(survey));
		},
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SurveyCreationSteps);
