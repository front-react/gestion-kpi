import React, { Component, Fragment } from "react";
import { Row, Col } from "antd";
import SurveyCreation from "./SurveyCreation";
import Summary from "./Summary";
import _ from "lodash";
import { isEmpty, assign, forEach } from "lodash/fp";
import { defaultProps } from 'recompose';

class SurveyCreationSteps extends Component {
	componentDidMount() {
		this.props.getByIdClient(321);
	}

	state = {
		surveyProgress: {
			statusProgress: false,
			surveyId: null,
			label: "",
			stepCreation: 0,
			assortments: "",
			periodicity: {},
			model: [],
			locations: [],
			secteurs: [],
		},
		modelClient: [],
	};

	componentWillReceiveProps(nextProps) {
		this.handleSetAssortement(nextProps.assortmentClient);
	}

	handleSetAssortement = assortments => {
		let retailersModel = [];
		let surveyProgress = this.state.surveyProgress;
		let assortementList = assortments;
		_.forEach(assortementList, (retailer, index) => {
			let retailerModel = retailer.retailer;
			let assignPeriod = _.assign(retailerModel, {
				periodicity: retailer.periodicity,
			});
			retailersModel.push(assignPeriod);
		});
		this.setState({ modelClient: retailersModel });
		this.props.addStepSurvey(surveyProgress);
	};

	render() {
		const { locations, handleClick, isModify } = this.props;
		const { modelClient } = this.state;

		return (
			<Fragment>
				<SurveyCreation
					locations={locations}
					handleClick={handleClick}
					modelClient={modelClient}
					isModify={isModify}
				/>
			</Fragment>
		);
	}
}

SurveyCreationSteps.defaultProps = {
	isModify: false
}

export default SurveyCreationSteps;
