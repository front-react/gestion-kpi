import React, { Component, Fragment } from "react";
import { Checkbox, Card } from "antd";
import {
	TitleStep,
	StepCard,
} from "./../../../../StyledComponents/StyledComponents";
import ButtonStep from "./../../../ButtonStep";
import _ from "lodash";
import { assign, zipObject, forEach, pick } from "lodash/fp";
const CheckboxGroup = Checkbox.Group;

class SurveyStepModeles extends Component {
	// componentWillReceiveProps(nextProps) {
	// 	console.log("componentWillReceiveProps");

	// }

	state = {
		surveyModel: [],
	};
	componentDidMount() {
		this.props.getmodelId(321);
		if (this.props.surveyProgress.model.length !== 0) {
			let surevyModel = this.props.surveyProgress.model;
			let labelModel = [];
			_.forEach(surevyModel, (model, index) => {
				let modelCurrent = _.pick(model, ["retailerName"]);
				labelModel.push(modelCurrent.retailerName);
			});
			this.setState({ surveyModel: labelModel });
			this.props.handleDisabled(false);
		}
	}
	onChange = checkedValues => {
		this.setState({ surveyModel: checkedValues });
		if (checkedValues.length < 1) {
			this.props.handleDisabled(true);
		}
		if (checkedValues.length > 0) {
			this.props.handleDisabled(false);
		}
	};

	next = current => {
		let surveyProgress = this.props.surveyProgress;
		let modeleList = this.props.modeleList;
		let assortement = this.props.assortmentClient;
		//let modeleList = _.assign([], [this.props.modeleList]);
		let selectModel = [];

		modeleList.filter(modele => {
			this.state.surveyModel.filter(actionModele => {
				if (actionModele === modele.retailerName) {
					selectModel.push(modele);
				}
				_.forEach(assortement, (assortement, index) => {
					if (assortement.retailer.retailerName === actionModele) {
						_.assign(surveyProgress, { assortments: assortement._id });
						_.assign(surveyProgress, { surveyTitle: "Survey GSK" });
					}
				});
			});
		});

		let zipModele = _.zipObject(["model"], [selectModel]);
		_.assign(surveyProgress, zipModele);
		this.props.handleDisabled(true);
		this.props.getLocation(321, this.props.surveyProgress.model[0].idRetailer);
		this.props.addStepSurvey(surveyProgress);
		this.props.next(current);
	};
	render() {
		const { modeleList, current, steps, sucess, disabled } = this.props;
		const { surveyModel } = this.state;
		//let modeleArrayList = _.assign([], [modeleList]);

		return (
			<Fragment>
				<StepCard>
					<TitleStep>Modèle</TitleStep>
					<CheckboxGroup
						style={{ width: "60%" }}
						onChange={this.onChange}
						value={surveyModel}
					>
						{/* {modeleList.map((modele, index) => (
							<div key={index} style={{ margin: "6px 0" }}>
								<Checkbox value={modele.label}>{modele.label}</Checkbox>
							</div>
						))} */}
						{modeleList.map((modele, index) => (
							<div key={index} style={{ margin: "6px 0" }}>
								<Checkbox value={modele.retailerName}>
									{modele.retailerName}
								</Checkbox>
							</div>
						))}
					</CheckboxGroup>
				</StepCard>
				<ButtonStep
					current={current}
					steps={steps}
					next={() => this.next(current)}
					sucess={() => sucess(current)}
					disabled={disabled}
				/>
			</Fragment>
		);
	}
}
// SurveyStepModeles.defaultProps = {
// 	modeleList: [
// 		{
// 			label: "Intermarché",
// 			startDate: "22/05/2015",
// 			endDate: "21/08/2018",
// 		},
// 		{
// 			label: "Auchan",
// 			startDate: "22/05/2015",
// 			endDate: "21/08/2018",
// 		},
// 		{
// 			label: "Super U",
// 			startDate: "22/05/2015",
// 			endDate: "21/08/2018",
// 		},
// 		{
// 			label: "Carrefour",
// 			startDate: "22/05/2015",
// 			endDate: "21/08/2018",
// 		},
// 		{
// 			label: "Casino",
// 			startDate: "22/05/2015",
// 			endDate: "21/08/2018",
// 		},
// 	],
// };
export default SurveyStepModeles;
