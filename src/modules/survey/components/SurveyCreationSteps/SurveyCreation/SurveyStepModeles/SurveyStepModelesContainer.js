import { connect } from "react-redux";
import SurveyStepModeles from "./SurveyStepModeles";
//import { surveySelector } from "./../../redux/selectors";
import { addStepSurvey } from "./../../../../redux/actions/progress/post";
import { getmodelId } from "./../../../../redux/actions/progress/get";
import { getLocation } from "./../../../../redux/actions/progress/get";

const mapStateToProps = (state, ownProps) => {
	return {
		//currentSurvey: surveySelector(state, ownProps),
		surveyProgress: state.survey.surveyManager.surveyProgress,
		//modeleList: state.survey.surveyManager.modeleList,
		assortmentClient: state.survey.surveyManager.assortmentClient,
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		addStepSurvey: survey => {
			dispatch(addStepSurvey(survey));
		},
		getmodelId: (idClient) => {
			dispatch(getmodelId(idClient));
		},
		getLocation: (idClient, idRetailer) => {
			dispatch(getLocation(idClient, idRetailer));
		},
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SurveyStepModeles);
