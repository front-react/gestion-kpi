import { connect } from "react-redux";
import SurveyCreation from "./SurveyCreation";
//import { surveySelector } from "./../../redux/selectors";
//import { getSurveyList } from "./../../../redux/actions/dashboard/get";
import { getSector } from "./../../../redux/actions/progress/get";
import { createElement } from "./../../../redux/actions/progress/post";

const mapStateToProps = (state, ownProps) => {
	return {
		surveyProgress: state.survey.surveyManager.surveyProgress,
		currentSurvey: state.survey.surveyManager.currentSurvey,
		locatonList: state.survey.surveyManager.locatonList,
		currentSurvey: state.survey.surveyManager.currentSurvey,
		sectorList: state.survey.surveyManager.sectorList,
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		// getSurveyList: surveyList => {
		// 	dispatch(getSurveyList(surveyList));
		// },
		createElement: data => {
			dispatch(createElement(data));
		},
		getSector: data => {
			dispatch(getSector(data));
		},
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SurveyCreation);
