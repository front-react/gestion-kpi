import { connect } from "react-redux";
import SurveyStepSimulation from "./SurveyStepSimulation";
//import { surveySelector } from "./../../redux/selectors";
import fakeSurvey from "./fakeSurvey.json";
import viewerCorrection from "./viewerCorrection.json";
import _ from "lodash";
import { assign } from "lodash/fp";

const mapStateToProps = (state, ownProps) => {
	return {
		currentSurvey: state.survey.surveyManager.currentSurvey,
		//currentSurvey: viewerCorrection,
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SurveyStepSimulation);
