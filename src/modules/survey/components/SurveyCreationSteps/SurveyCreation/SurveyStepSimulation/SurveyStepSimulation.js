import React, { Component, Fragment } from "react";
import { PhoneContainer, StepContainer, StepCard } from "./StyledComponents";
import ButtonStep from "./../../../ButtonStep";
import { EmplacementQuestion, SimulatedSurvey } from "./surveyStep";
import _ from "lodash";
import { isEmpty } from "lodash/fp";

class SurveyStepSimulation extends Component {
	state = {
		isSurveyGenerated: false,
		survey: {},
	};

	generateSurvey = survey => {
		console.log("submit", survey);

		this.setState({ survey, isSurveyGenerated: true });
	};

	render() {
		const {
			current,
			steps,
			prev,
			next,
			sucess,
			disabled,
			currentSurvey,
		} = this.props;
		console.log(this.state.survey);
		console.log(this.state.isSurveyGenerated);
		return (
			<Fragment>
				<StepCard margin="40px auto 62px">
					<PhoneContainer outline={this.state.isSurveyGenerated}>
						{this.state.isSurveyGenerated ? (
							<SimulatedSurvey json={currentSurvey} />
						) : (
							<StepContainer>
								<EmplacementQuestion
									json={currentSurvey}
									handleSubmit={this.generateSurvey}
								/>
							</StepContainer>
						)}
					</PhoneContainer>
				</StepCard>
				<ButtonStep
					current={current}
					steps={steps}
					prev={() => prev(current)}
					next={() => next(current)}
					sucess={() => sucess(current)}
					disabled={disabled}
				/>
			</Fragment>
		);
	}
}

export default (SurveyStepSimulation);
