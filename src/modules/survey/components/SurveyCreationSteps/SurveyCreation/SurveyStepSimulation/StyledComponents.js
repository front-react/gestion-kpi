import styled from "styled-components";
import { Button, Card, InputNumber } from "antd";

export const StepCard = styled(Card)`
	&& {
		margin: ${props => (props.margin ? props.margin : "40px auto 0 auto")};
		border-top: 4px solid ${props => props.theme.colors.primary} !important;
		text-align: ${props => (props.align ? props.align : "left")};
		box-shadow: 3px 3px 5px 0 rgba(170, 170, 170, 0.5);
		height: 75vh;
		width: 42vh;
		min-width: 430px;
		min-height: 620px;
		display: flex;
		& > .ant-card-body {
			flex: 1;
			display: flex;
			width: 100%;
		}
	}
`;
export const StepContainer = styled.div`
	overflow: hidden;
	padding: 10px;
	flex: 1;
	display: flex;
	flex-direction: column;
`;

export const RowContainer = styled.div`
	margin-bottom: 5px;
	display: flex;
	align-items: center;
	justify-content: space-between;
`;

export const SpanIndent = styled.span`
	padding-left: ${props => (props.paddingLeft ? props.paddingLeft : "0px")};
	text-decoration: ${props => (props.decoration ? props.decoration : "none")};
`;

/** Simulated Survey **/
export const PhoneContainer = styled.div`
	border: ${props => (props.outline ? "solid 1px black" : "none")};
	display: flex;
	flex-direction: column;
	overflow: hidden;
	flex: 1;
`;
export const ContentContainer = styled.div`
	padding: 15px 20px 0;
	height: 90%;
`;

export const QuestionsContainer = styled.div`
	overflow-y: auto;
	height: 80%;
	/* SCROLLBAR */
	::-webkit-scrollbar {
		width: 10px; /* for vertical scrollbars */
		height: 10px; /* for horizontal scrollbars */
	}

	::-webkit-scrollbar-track {
		background: #efefef;
	}

	::-webkit-scrollbar-thumb {
		background: #ed8b00;
	}
`;
export const ButtonContainer = styled.div`
	display: flex;
	margin-top: auto;
	box-shadow: 0 -1px 4px 0 rgba(168, 168, 168, 0.5);
`;
export const NavButton = styled(Button)`
	border: none;
	height: 45px;
	color: ${props => props.theme.colors.primary};
	flex: 1;
	text-transform: uppercase;
	font-size: 12px;
	font-weight: 500;
	z-index: 1;
`;
export const LocationTypeTitle = styled.p`
	color: ${props => props.theme.colors.primary};
	text-transform: uppercase;
	margin: 10px 0 0 0;
`;
export const StatementTitle = styled.p`
	font-weight: 500;
	font-size: 12px;
`;
export const ProductContainer = styled.div`
	margin-top: 25px;
	&:first-child {
		border-top: 1px solid lightgray;
		padding-top: 25px;
	}
`;
export const ProductTitle = styled.span`
	font-weight: 500;
	font-size: 14px;
`;
export const QuestionProductLabel = styled.div`
	font-size: 14px;
	margin: 8px 0;
`;
/* matrix */
export const MatrixContainer = styled.div`
	display: grid;
	grid-template-columns: 1fr repeat(4, 45px);
	grid-gap: 10px;
	grid-auto-rows: minmax(10px, auto);
	align-items: center;
	margin-right: 5px;
`;
export const MatrixProduct = styled.div`
	grid-column: 1;
	grid-row: 1;
	padding: 10px 0;
`;

export const KpiMatrixLabel = styled.div`
	text-align: center;
`;
export const KpiInputNumber = styled(InputNumber)`
	&& {
		width: 35px;
		height: 25px;
		margin: auto;
		display: flex;
		.ant-input-number-handler-wrap {
			display: none;
		}
		.ant-input-number-input {
			height: 100%;
		}
	}
`;
