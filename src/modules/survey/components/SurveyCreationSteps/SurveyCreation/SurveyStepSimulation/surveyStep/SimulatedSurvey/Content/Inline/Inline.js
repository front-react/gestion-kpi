import React, { Component, Fragment } from "react";
import { InputNumber, Radio } from "antd";
import {
	ProductTitle,
	QuestionProductLabel,
	ProductContainer,
} from "./../../../../StyledComponents";

const Inline = props => {
	const { categories, kpis, ltN, lN, sN, setResponse, statement } = props;

	const handleChangeAnswer = (value, ltN, lN, sN, k, p) => {
		setResponse(value, ltN, lN, sN, k, p);
	};

	return (
		<Fragment>
			{categories.map((category, c) => {
				return (
					<Fragment key={c}>
						{/* <div className="cTitle">{category.label}</div> */}
						{category.products.map((product, p) => {
							return (
								<ProductContainer key={p}>
									<ProductTitle className="pLabel">
										{product.label}
									</ProductTitle>
									{kpis.map((kpi, k) => {
										console.log(new Boolean(kpi.surveyDisplay));
										return new Boolean(kpi.surveyDisplay) ? (
											<div className="kpi" key={k}>
												<QuestionProductLabel>
													{kpi.question}
												</QuestionProductLabel>
												{(function() {
													switch (kpi.answerFormat) {
														case "Num":
															return (
																<InputNumber
																	key={`${p}${k}`}
																	value={
																		statement.kpis[k].response
																			? statement.kpis[k].response[p]
																			: ""
																	}
																	onChange={value =>
																		handleChangeAnswer(value, ltN, lN, sN, k, p)
																	}
																	style={{ width: "90%" }}
																	min="0"
																/>
															);
														case "Booléen":
															return (
																<Radio.Group
																	value={
																		statement.kpis[k].response[p] ? true : false
																	}
																	buttonStyle="solid"
																	onChange={e =>
																		handleChangeAnswer(
																			e.target.value,
																			ltN,
																			lN,
																			sN,
																			k,
																			p
																		)
																	}
																>
																	<Radio.Button
																		id={`${p}${k}1`}
																		value={true}
																		name="booltrue"
																	>
																		Oui
																	</Radio.Button>
																	<Radio.Button
																		id={`${p}${k}0`}
																		value={false}
																		name="boolfalse"
																	>
																		Non
																	</Radio.Button>
																</Radio.Group>
															);
														default:
															return null;
													}
												})()}
											</div>
										) : null;
									})}
								</ProductContainer>
							);
						})}
					</Fragment>
				);
			})}
		</Fragment>
	);
};

export default Inline;
