import React, { Component, Fragment } from "react";
import Content from "./Content";
import { Icon } from "antd";
import { NavButton, ButtonContainer } from "./../../StyledComponents";
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! local
const sendResponse = (idSurvey, response) => {
	console.log(idSurvey);
	console.log(response);
};
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! local

// SimulatedSurvey Component
class SimulatedSurvey extends Component {
	constructor(props) {
		super(props);
		this.state = {
			ltN: 0,
			lN: 0,
			sN: 0,
			firstPage: true,
			surveyOn: true,
			visit: {
				survey: this.props.json.surveyAnswers,
				surveyId: 4,
				//surveyId: this.props.json.surveyId,
				label: this.props.json.label,
			},
			answerData: {
				surveyId: 4,
				//surveyId: this.props.json.surveyId,
				label: this.props.json.label,
				survey: {},
			},
		};
	}

	/*
	 ****************************
	 * FUNCTIONS *
	 ****************************
	 */

	setNewStates = (locationType, location, statement) => {
		let firstPage;
		// If first page
		if (locationType === 0 && location === 0 && statement === 0) {
			firstPage = true;
		}
		// Set new states
		this.setState({
			ltN: locationType,
			lN: location,
			sN: statement,
			firstPage: firstPage,
		});
		console.log(
			"______SET STATEMENT TO " +
				statement +
				" ( LOCATION " +
				this.state.lN +
				" LOCATIONTYPE " +
				this.state.ltN +
				" )"
		);
	};

	nextOrPrev = (nav, ltN, lN, sN, sendResponse) => {
		switch (nav) {
			case "next":
				this.getNextState(ltN, lN, sN, sendResponse);
				break;
			case "prev":
				this.getPrevState(ltN, lN, sN, sendResponse);
				break;
			default:
				console.log("do nothing");
				break;
		}
	};

	getPrevState = (ltN, lN, sN, sendResponse) => {
		const { firstPage, visit } = this.state;

		// Go to prev statement
		sN = sN - 1;
		// Get prev statement with correct locationType and location
		while (!visit.survey[ltN].locations[lN].statements[sN]) {
			if (!firstPage) {
				// IF NO STATEMENT, UPDATE TO PREVIOUS LOCATION
				if (!visit.survey[ltN].locations[lN].statements[sN]) {
					console.log(
						"NO STATEMENT " +
							sN +
							" FOR LOCATION " +
							lN +
							" LOCATIONTYPE " +
							ltN
					);
					lN = lN - 1;
					sN =
						lN >= 0
							? visit.survey[ltN].locations[lN].statements.length - 1
							: sN;
				}
				// IF NO LOCATION, UPDATE TO PREVIOUS LOCATIONTYPE
				if (!visit.survey[ltN].locations[lN]) {
					console.log("NO LOCATION " + lN + " FOR LOCATIONTYPE " + ltN);
					ltN = ltN - 1;
					lN = visit.survey[ltN].locations.length - 1;
					sN = visit.survey[ltN].locations[lN].statements.length - 1;
				}
			}
		}
		this.setNewStates(ltN, lN, sN);
	};

	getNextState = (ltN, lN, sN, sendResponse) => {
		const { visit } = this.state;
		console.log(visit.survey);
		console.log(this.props.json.surveyAnswers);

		console.log("getNextState");

		// Go to next statement
		sN = sN + 1;
		//this.setState({ sN });
		console.log(visit.survey[ltN].locations[lN].statements[sN]);
		// Get next statement with correct locationType and location
		while (!visit.survey[ltN].locations[lN].statements[sN]) {
			console.log("W H I L E");
			console.log(ltN + " " + lN + " " + sN);
			// IF NO STATEMENT, UPDATE TO NEXT LOCATION
			if (!visit.survey[ltN].locations[lN].statements[sN]) {
				console.log(
					"NO STATEMENT " + sN + " FOR LOCATION " + lN + " LOCATIONTYPE " + ltN
				);
				lN = lN + 1;
				sN = 0;
			}
			// IF NO LOCATION, UPDATE TO NEXT LOCATIONTYPE
			if (!visit.survey[ltN].locations[lN]) {
				console.log("NO LOCATION " + lN + " FOR LOCATIONTYPE " + ltN);
				ltN = ltN + 1;
				lN = 0;
				sN = 0;
			}
			// IF NO LOCATIONTYPE, END SURVEY
			if (!visit.survey[ltN]) {
				console.log("NO LOCATIONTYPE " + ltN);
				this.setState({ surveyOn: false });
				break; // to avoid this while when survey is over
			}
		}
		this.setNewStates(ltN, lN, sN);
	};

	setConditionStatus = (ltN, lN, sN, c, status) => {
		const { visit } = this.state;
		// Ser current condition status (for one condition)
		visit.survey[ltN].locations[lN].statements[sN].conditions[
			c
		].status = status;
		// Set global conditions status (for all conditions of the statement)
		visit.survey[ltN].locations[lN].statements[sN].conditions.length > 0
			? visit.survey[ltN].locations[lN].statements[sN].conditions.map(
					(condition, c) => {
						if (condition.status === false) {
							this.setResponse("dev-remove-responses", ltN, lN, sN);
							return (visit.survey[ltN].locations[lN].statements[
								sN
							].conditionStatus = false);
						} else {
							visit.survey[ltN].locations[lN].statements[
								sN
							].conditionStatus = true;
						}
					}
			  )
			: null;
		this.setState({ visit: visit });
	};

	setResponse = (response, ltN, lN, sN, k, p) => {
		const { visit } = this.state;
		// If conditionStatus is set to false, remove all the responses of this statement's kpis
		if (response === "dev-remove-responses") {
			visit.survey[ltN].locations[lN].statements[sN].kpis.map((kpi, k) => {
				kpi.response = [];
			});
		} else {
			visit.survey[ltN].locations[lN].statements[sN].conditionStatus
				? (visit.survey[ltN].locations[lN].statements[sN].kpis[k].response[
						p
				  ] = response)
				: (visit.survey[ltN].locations[lN].statements[sN].kpis[
						k
				  ].response = []);
		}
		console.log(visit.survey[ltN].locations[lN].statements[sN]);
		this.setState({ visit: visit });
	};

	/*
	 ****************************
	 * RENDER *
	 ****************************
	 */

	render() {
		const { ltN, lN, sN, firstPage, surveyOn, visit } = this.state;
		console.log(surveyOn);
		const json = this.props.json;

		// Set global conditions status for current item
		/*let conditionStatus = true;
    visit.survey[ltN].locations[lN].statements[sN].conditions.length > 0
      ? visit.survey[ltN].locations[lN].statements[sN].conditions.map(
          (condition, c) => {
            if (condition.status === false) {
              return (conditionStatus = false);
            }
          }
        )
			: null;*/

		return surveyOn === true ? (
			/* If survey is active */
			<Fragment>
				{/* <div className="title">{visit.label}</div> */}
				{visit.survey[ltN].locations[lN].statements[sN] ? (
					<Content
						key={visit.survey[ltN].locationTypeId}
						locationType={visit.survey[ltN]}
						location={visit.survey[ltN].locations[lN]}
						statement={visit.survey[ltN].locations[lN].statements[sN]}
						ltN={ltN}
						lN={lN}
						sN={sN}
						setResponse={this.setResponse}
						setConditionStatus={this.setConditionStatus}
						allProducts={json.metier.products}
						conditionStatus={
							visit.survey[ltN].locations[lN].statements[sN].conditionStatus
						}
					/>
				) : (
					<span>toto</span>
				)}
				<ButtonContainer>
					<NavButton
						disabled={firstPage ? true : false}
						onClick={() => this.getPrevState(ltN, lN, sN)}
					>
						Retour
					</NavButton>
					<NavButton
						onClick={() => this.getNextState(ltN, lN, sN, sendResponse)}
					>
						Suivant
					</NavButton>
				</ButtonContainer>
			</Fragment>
		) : (
			/* When survey is over */
			<div
				style={{
					display: "flex",
					alignItems: "center",
					justifyContent: "center",
					flex: 1,
					flexDirection: "column",
				}}
			>
				<span>Merci, le questionnaire est en cours d'envoi...</span>
				<Icon type="loading" theme="outlined" />
			</div>
		);
	}
}

export default SimulatedSurvey;
