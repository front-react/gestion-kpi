import React, { Component, Fragment } from "react";
import { InputNumber, Select, Col } from "antd";
import {
	TitleStep,
	SubTitleStep,
	InputSubmit,
} from "./../../../../../StyledComponents/StyledComponents";
import { RowContainer, SpanIndent } from "./../StyledComponents";

const Option = Select.Option;

class EmplacementQuestion extends Component {
	constructor(props) {
		super(props);
		this.state = {
			locationTypes: [],
			json: props.json,
			submitted: false,
			countEmptyRanges: 0,
			countFilledRanges: 0,
		};
	}

	handleChangeLocations(value, lt, locationTypeId, label) {
		let locationTypes = this.state.locationTypes;
		locationTypes[lt] = { locationTypeId, label, locations: [] };
		// for each location in locationType, populate array
		for (let a = 0; a < value; a++) {
			const locationNumber = a + 1;
			locationTypes[lt].locations[a] = {
				locationTypeId: `${lt}${a}`,
				label: label + " " + locationNumber,
				categories: [],
				statements: this.state.json.metier.locationTypes[lt].statements,
			};
		}

		// setstate
		this.setState({
			locationTypes: locationTypes,
			countEmptyRanges: value,
		});
	}

	handleChangeRanges(value, lt, l, c, categoryId, label) {
		let locationTypes = this.state.locationTypes;
		// get current location and add categories with their range
		locationTypes[lt].locations[l].categories[c] = {
			categoryId,
			label,
			rangeId: value,
		};
		// setstate
		this.setState({
			locationTypes: locationTypes,
			countFilledRanges: this.state.countFilledRanges + 1,
		});
	}

	handleSubmit() {
		// create json
		let json = this.state.json;
		json.surveyAnswers = this.state.locationTypes;
		this.props.handleSubmit(json);
	}

	render() {
		const {
			json,
			countEmptyRanges,
			countFilledRanges,
			locationTypes,
		} = this.state;

		return (
			<Fragment>
				<TitleStep
					marginBottom={"0px"}
					borderBottom={"none"}
					paddingBottom={"0px"}
				>
					SurveyStepSimulation
				</TitleStep>
				<SubTitleStep>
					Pour chaque type d'emplacement, indiquer le nombre présents dans le
					magasin :
				</SubTitleStep>
				<form>
					{// for each locationType, ask how many location
					json.metier.locationTypes.map((locationType, lt) => {
						return (
							<div className="inputItem" key={lt}>
								<RowContainer type="flex" justify="start" align="middle">
									<span className="inputLabel">{locationType.label} :</span>
									<InputNumber
										value={
											this.state.locationTypes[lt]
												? this.state.locationTypes[lt].locations.length
												: ""
										}
										min="0"
										onChange={event =>
											this.handleChangeLocations(
												event,
												lt,
												locationType.locationTypeId,
												locationType.label
											)
										}
										style={{ width: "120px" }}
									/>
								</RowContainer>
							</div>
						);
					})}
					{this.state.locationTypes.length > 0 ? (
						// for each locationType
						<div style={{ overflowY: "auto", height: "280px" }}>
							{this.state.locationTypes.map((locationType, lt) => {
								console.log(
									locationType.locationTypeId,
									json.metier.locationTypes[locationType.locationTypeId],
									json.metier.locationTypes
								);
								return (
									<div key={lt}>
										{// for each location in locationType
										this.state.locationTypes[lt].locations.map(
											(location, l) => {
												return (
													<Fragment key={l}>
														<SpanIndent
															decoration={"underline"}
															className="formDesc"
														>
															{location.label} :
														</SpanIndent>
														<div className="inputItem">
															{// for each category of current location
															json.metier.locationTypes[
																locationType.locationTypeId
															].categories.map((category, c) => {
																return (
																	<div className="inputItem" key={c}>
																		<RowContainer>
																			<SpanIndent
																				paddingLeft={"10px"}
																				className="inputLabel"
																			>
																				Famille {category.label} :
																			</SpanIndent>
																			<Select
																				id="interet"
																				onChange={event =>
																					this.handleChangeRanges(
																						event,
																						lt,
																						l,
																						c,
																						category.categoryId,
																						category.label
																					)
																				}
																				style={{ width: "120px" }}
																				placeholder="Choisir la strate"
																			>
																				{// for each range, select the one for current category
																				json.metier.ranges.map((range, r) => {
																					return (
																						<Option
																							value={range.rangeId}
																							key={r}
																						>
																							{range.label}
																						</Option>
																					);
																				})}
																			</Select>
																		</RowContainer>
																	</div>
																);
															})}
														</div>
													</Fragment>
												);
											}
										)}
									</div>
								);
							})}
						</div>
					) : null}
				</form>
				{// display validate button only if all family range are filled
				// controls should be better with form fields
				countEmptyRanges > 0 &&
					countFilledRanges >= countEmptyRanges && (
						<InputSubmit
							type="submit"
							value="Valider"
							onClick={() => this.handleSubmit(json, locationTypes)}
						/>
					)}
			</Fragment>
		);
	}
}

export default EmplacementQuestion;
