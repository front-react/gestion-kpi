import React, { Component, Fragment } from "react";
import { Progress, Radio } from "antd";

import {
	ContentContainer,
	QuestionsContainer,
	LocationTypeTitle,
	StatementTitle,
} from "./../../../StyledComponents";
import Matrix from "./Matrix";
import Inline from "./Inline";

class Content extends Component {
	constructor(props) {
		super(props);
		this.state = {
			conditionStatus: props.conditionStatus,
		};
	}

	updateConditionStatus(ltN, lN, sN, c, value) {
		this.setState({ conditionStatus: value });
		this.props.setConditionStatus(ltN, lN, sN, c, value);
	}

	componentWillReceiveProps(nextProps) {
		this.setState({ conditionStatus: nextProps.conditionStatus });
	}

	render() {
		const {
			locationType,
			location,
			statement,
			ltN,
			lN,
			sN,
			allProducts,
			setResponse,
		} = this.props;
		const { conditionStatus } = this.state;

		let categories = [];
		// Get products by categories & ranges
		location.categories.map((category, c) => {
			// get products
			let products = [];
			allProducts.map((product, p) => {
				if (
					product.categoryId === category.categoryId &&
					product.range.rangeId === category.rangeId
				) {
					products.push(product);
				}
			});
			// push array
			categories.push({
				...category,
				products,
			});
		});
		return (
			<ContentContainer>
				<Progress
					percent={((lN + 1) / locationType.locations.length) * 100}
					showInfo={false}
					strokeColor="#ED8B00"
				/>
				<LocationTypeTitle>{locationType.label}</LocationTypeTitle>
				<StatementTitle>{statement.label}</StatementTitle>
				<QuestionsContainer className="sContent">
					{statement.conditions.length > 0
						? /* Display question to test condition to display KPIs (if yes is answered) */
						  statement.conditions.map((condition, c) => {

								return (
									<div className="sContent" key={c}>
										<p>{condition.question}</p>
										<Radio.Group
											value={conditionStatus}
											buttonStyle="solid"
											onChange={e =>
												this.updateConditionStatus(
													ltN,
													lN,
													sN,
													c,
													e.target.value
												)
											}
										>
											<Radio.Button id="1" value={true} name="condition">
												Oui
											</Radio.Button>
											<Radio.Button id="0" value={false} name="condition">
												Non
											</Radio.Button>
										</Radio.Group>
									</div>
								);
						  })
						: /*update conditionStatus to true?*/ null}
					{conditionStatus === true ? (
						<div className="sContent">
							{(function() {
								switch (statement.displayType) {
									case "matrix":
										return (
											<Matrix
												categories={categories}
												kpis={statement.kpis}
												ltN={ltN}
												lN={lN}
												sN={sN}
												setResponse={setResponse}
												statement={statement}
											/>
										);
									case "inline":
										return (
											<Inline
												categories={categories}
												kpis={statement.kpis}
												ltN={ltN}
												lN={lN}
												sN={sN}
												setResponse={setResponse}
												statement={statement}
											/>
										);
									default:
										return null;
								}
							})()}
						</div>
					) : null}
				</QuestionsContainer>
			</ContentContainer>
		);
	}
}

export default Content;
