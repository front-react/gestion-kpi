import React, { Component, Fragment } from "react";
import { Checkbox } from "antd";
import {
	MatrixContainer,
	MatrixProduct,
	KpiMatrixLabel,
	KpiInputNumber,
} from "./../../../../StyledComponents";

const Matrix = props => {
	const { categories, kpis, ltN, lN, sN, setResponse, statement } = props;

	const handleChangeAnswer = (value, ltN, lN, sN, k, p) => {
		setResponse(value, ltN, lN, sN, k, p);
	};
	console.log("je suis la !!");
	return (
		<Fragment>
			{categories.map((category, c) => {
				return (
					<Fragment key={c}>
						{/* <div className="cTitle">{category.label}</div> */}
						<MatrixContainer>
							{kpis.map((kpi, k) => {
								return new Boolean(kpi.surveyDisplay) ? (
									<Fragment key={k}>
										<MatrixProduct className="matrixProduct" />
										<KpiMatrixLabel>{kpi.label}</KpiMatrixLabel>
									</Fragment>
								) : null;
							})}
						</MatrixContainer>
						{category.products.map((product, p) => {
							return (
								<MatrixContainer key={p} className="matrix">
									<MatrixProduct className="matrixProduct">
										<span className="pLabel">{product.label}</span>
									</MatrixProduct>
									{kpis.map((kpi, k) => {
										return new Boolean(kpi.surveyDisplay) ? (
											<Fragment key={k}>
												{(function() {
													switch (kpi.answerFormat) {
														case "Num":
															return (
																<KpiInputNumber
																	className="matrixInput"
																	key={`${p}${k}`}
																	value={
																		statement.kpis[k].response[p]
																			? statement.kpis[k].response[p]
																			: ""
																	}
																	onChange={value =>
																		handleChangeAnswer(value, ltN, lN, sN, k, p)
																	}
																	min="0"
																/>
															);
														case "Booléen":
															return (
																<div
																	key={k}
																	style={{ width: "100%", textAlign: "center" }}
																>
																	<Checkbox
																		onChange={event =>
																			handleChangeAnswer(
																				event.target.checked,
																				ltN,
																				lN,
																				sN,
																				k,
																				p
																			)
																		}
																		id={`${p}${k}1`}
																		checked={
																			statement.kpis[k].response[p]
																				? true
																				: false
																		}
																	/>
																</div>
															);
														default:
															return null;
													}
												})()}
											</Fragment>
										) : null;
									})}
								</MatrixContainer>
							);
						})}
					</Fragment>
				);
			})}
		</Fragment>
	);
};

export default Matrix;
