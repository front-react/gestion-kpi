import React, { Component, Fragment, PureComponent } from "react";
import { Card, Row, Col, Divider } from "antd";
import Sortable from "react-sortablejs";
import uniqueId from "lodash/uniqueId";
import {
	TitleStep,
	StepCard,
	SubTitleStep,
	DraggableItem,
} from "./../../../../StyledComponents/StyledComponents";
import ButtonStep from "./../../../ButtonStep";
import _ from "lodash";
import { assign, zipObject, forEach, pick, filter, xor } from "lodash/fp";

class SurveyStepEmplacement extends PureComponent {
	state = {
		listEmplacementsSource: [],
		listEmplacementsTarget: [],
		locations: [],
	};

	componentDidMount() {
		if (this.props.surveyProgress.locations.length !== 0) {
			let surveyLocations = this.props.surveyProgress.locations;

			let locationsCurrent = [];
			_.forEach(surveyLocations, (location, index) => {
				let locations = _.pick(location, ["label"]);
				locationsCurrent.push(locations.label);
			});
			let locatonList = this.props.locatonList;
			let sources = [];
			_.forEach(locatonList, (src, index) => {
				sources.push(src.label);
			});

			let setectSource = xor(sources, locationsCurrent);
			this.setState({
				listEmplacementsSource: setectSource,
				listEmplacementsTarget: locationsCurrent,
			});
			this.props.handleDisabled(false);
		}
		
		let surveyLocations = this.props.surveyProgress.locations;

		let locationsCurrent = [];
		_.forEach(surveyLocations, (location, index) => {
			let locations = _.pick(location, ["label"]);
			locationsCurrent.push(locations.label);
		});
		let locatonList = this.props.locatonList;
		let sources = [];
		_.forEach(locatonList, (src, index) => {
			sources.push(src.label);
		});

		this.setState({
			listEmplacementsSource: xor(sources, locationsCurrent),
			locations: locatonList,
		});

		this.props.getKpiList();
	}

	handleFilterSource = (items, sortable, evt) => {
		this.setState({
			listEmplacementsSource: items,
		});
	};
	handleFilterTarget = (items, sortable, evt) => {
		console.log(items);
		if (items.length > 0) {
			this.props.handleDisabled(false);
		}
		if (items.length === 0) {
			this.props.handleDisabled(true);
		}
		this.setState({ listEmplacementsTarget: items });
	};

	next = current => {
		let locations = this.state.locations;

		let surveyProgress = this.props.surveyProgress;

		let selectEmplacement = [];
		locations.filter(location =>
			this.state.listEmplacementsTarget.filter(target => {
				if (target === location.label) {
					let locationToStatement = _.assign(location, { statements: [] });
					selectEmplacement.push(locationToStatement);
				}
			})
		);
		let zipEmplacement = zipObject(["locations"], [selectEmplacement]);
		let surveyEmplacement = assign(surveyProgress, zipEmplacement);
		this.props.addStepSurvey(surveyEmplacement);
		this.props.handleDisabled(true);
		this.props.next(current);
	};

	render() {
		const {
			listEmplacementsSource,
			listEmplacementsTarget,
			locations,
		} = this.state;
		const { current, steps, prev, sucess, disabled } = this.props;
		return (
			<Fragment>
				<StepCard>
					<TitleStep
						marginBottom={"0px"}
						borderBottom={"none"}
						paddingBottom={"0px"}
					>
						Choix des types d'emplacements
					</TitleStep>
					<SubTitleStep>
						Sélectionner et glisser les emplacements dans la colonne de droite
						afin de les ordonner.
					</SubTitleStep>

					<Row
						gutter={16}
						style={{
							display: "flex",
							justifyContent: "center",
							height: " 200px",
						}}
					>
						<Col span={7}>
							<Sortable
								style={{ padding: "5px 0", height: "100%" }}
								options={{
									animation: 150,
									sort: false,
									group: {
										name: "shared",
										pull: true,
										put: true,
									},
								}}
								onChange={(items, sortable, evt) =>
									this.handleFilterSource(items, sortable, evt)
								}
								tag="div"
							>
								{listEmplacementsSource.map((source, key) => {
									return (
										<DraggableItem key={source} data-id={source}>
											{source}
										</DraggableItem>
									);
								})}
							</Sortable>
						</Col>
						<Col span={2}>
							<Divider type="vertical" style={{ height: "100%" }} />
						</Col>
						<Col span={7}>
							<Sortable
								style={{ padding: "5px 0", height: "100%" }}
								options={{
									animation: 150,
									group: {
										name: "shared",
										pull: true,
										put: true,
									},
								}}
								onChange={(items, sortable, evt) => {
									this.handleFilterTarget(items, sortable, evt);
								}}
								tag="div"
							>
								{listEmplacementsTarget.map((source, key) => {
									return (
										<DraggableItem key={key} data-id={source}>
											{source}
										</DraggableItem>
									);
								})}
							</Sortable>
						</Col>
					</Row>
				</StepCard>
				<ButtonStep
					current={current}
					steps={steps}
					prev={() => prev(current)}
					next={() => this.next(current)}
					sucess={() => sucess(current)}
					disabled={disabled}
				/>
			</Fragment>
		);
	}
}

export default SurveyStepEmplacement;
