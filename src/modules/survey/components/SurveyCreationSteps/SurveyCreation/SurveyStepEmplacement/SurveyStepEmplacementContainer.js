import { connect } from "react-redux";
import SurveyStepEmplacement from "./SurveyStepEmplacement";
//import { surveySelector } from "./../../redux/selectors";
import { addStepSurvey } from "./../../../../redux/actions/progress/post";
import { getKpiList } from "./../../../../redux/actions/dashboard/get";

const mapStateToProps = (state, ownProps) => {
	return {
		//currentSurvey: surveySelector(state, ownProps),
		surveyProgress: state.survey.surveyManager.surveyProgress,
		locatonList: state.survey.surveyManager.locatonList,
		model: state.survey.surveyManager.surveyProgress.model,
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		addStepSurvey: survey => {
			dispatch(addStepSurvey(survey));
		},
		getKpiList: () => {
			dispatch(getKpiList());
		},
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SurveyStepEmplacement);
