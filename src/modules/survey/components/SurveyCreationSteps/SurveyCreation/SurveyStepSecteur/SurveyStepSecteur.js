import React, { Component, Fragment } from "react";
import { Checkbox, Card } from "antd";
import {
	TitleStep,
	StepCard,
} from "./../../../../StyledComponents/StyledComponents";
import ButtonStep from "./../../../ButtonStep";
import _ from "lodash";
import { toString, parseInt, forEach, uniqueId } from "lodash/fp";

const CheckboxGroup = Checkbox.Group;

class SurveyStepSecteur extends Component {
	state = {
		surveySecteurs: [],
		secteurList: this.props.sectorList,
		// secteurList: [
		// 	{
		// 		value: 1,
		// 		label: "IDF",
		// 	},
		// 	{
		// 		value: 2,
		// 		label: "Lyon",
		// 	},
		// 	{
		// 		value: 3,
		// 		label: "Ouest",
		// 	},
		// ],
	};

	componentDidMount() {
		if (this.props.surveyProgress.secteurs.length !== 0) {
			let surevySecteurs = this.props.surveyProgress.secteurs;
			let labelSecteur = [];
			_.forEach(surevySecteurs, (secteur, index) => {
				labelSecteur.push(secteur.sectorName);
			});
			this.setState({ surveySecteurs: labelSecteur });
			this.props.handleDisabled(false);
		}
	}

	onChange = checkedValues => {
		this.setState({ surveySecteurs: checkedValues });
		if (checkedValues.length < 1) {
			this.props.handleDisabled(true);
		}
		if (checkedValues.length > 0) {
			this.props.handleDisabled(false);
		}
	};
	next = current => {
		let surveyProgress = this.props.surveyProgress;
		let secteurList = this.state.surveySecteurs;
		let selectSecteurs = [];

		_.forEach(secteurList, (secteur, index) => {
			selectSecteurs.push({ label: secteur });
		});

		let zipSecteurs = _.zipObject(["sectors"], [selectSecteurs]);
		_.assign(surveyProgress, zipSecteurs);
		this.props.addStepSurvey(surveyProgress);

		let assignId = _.assign(surveyProgress, { surveyId: _.uniqueId() });
		let transformSurvey = _.assign([], [assignId]);

		let data = {
			elements: JSON.stringify(transformSurvey),
			assortmentId: surveyProgress.assortments,
		};

		this.props.addElement(data);
		this.props.handleDisabled(true);
		this.props.next(current);
	};
	render() {
		const { current, steps, prev, sucess, disabled, sectorList } = this.props;

		const { surveySecteurs } = this.state;
		return (
			<Fragment>
				<StepCard maxHeight={"auto"}>
					<TitleStep>Secteurs</TitleStep>
					<CheckboxGroup
						style={{ width: "100%" }}
						onChange={this.onChange}
						value={surveySecteurs}
					>
						{sectorList.map((modele, index) => (
							<div key={modele.sectorName} style={{ margin: "6px 0" }}>
								<Checkbox value={modele.sectorName}>
									{modele.sectorName}
								</Checkbox>
							</div>
						))}
					</CheckboxGroup>
				</StepCard>
				<ButtonStep
					current={current}
					steps={steps}
					prev={() => prev(current)}
					next={() => this.next(current)}
					sucess={() => sucess(current)}
					disabled={disabled}
				/>
			</Fragment>
		);
	}
}

export default SurveyStepSecteur;
