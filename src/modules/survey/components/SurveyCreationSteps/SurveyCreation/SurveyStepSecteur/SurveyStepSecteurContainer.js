import { connect } from "react-redux";
import SurveyStepSecteur from "./SurveyStepSecteur";
//import { surveySelector } from "./../../redux/selectors";
import {
	addStepSurvey,
	addElement,
} from "./../../../../redux/actions/progress/post";
import { getSector } from "./../../../../redux/actions/progress/get";

const mapStateToProps = (state, ownProps) => {
	return {
		//currentSurvey: surveySelector(state, ownProps),
		surveyProgress: state.survey.surveyManager.surveyProgress,
		sectorList: state.survey.surveyManager.sectorList,
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		addStepSurvey: survey => {
			dispatch(addStepSurvey(survey));
		},
		addElement: data => {
			dispatch(addElement(data));
		},
		getSector: client => {
			dispatch(getSector(client));
		},
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SurveyStepSecteur);
