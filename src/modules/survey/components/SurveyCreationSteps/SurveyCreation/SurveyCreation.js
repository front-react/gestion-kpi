import React, { Component, Fragment } from "react";
import {
	PaperApp,
	Title,
	SubTitle,
} from "./../../../StyledComponents/StyledComponents";
import { Steps, Row, Col, Button, message, Divider, Icon } from "antd";
import SurveyStepPeriode from "./SurveyStepPeriode";
import SurveyStepModeles from "./SurveyStepModeles";
import SurveyStepEmplacement from "./SurveyStepEmplacement";
import SurveyStepActionsKpi from "./SurveyStepActionsKpi";
import SurveyStepSecteur from "./SurveyStepSecteur";
import SurveyStepSimulation from "./SurveyStepSimulation";
import Summary from "./../Summary";
import _ from "lodash";
import { withRouter } from "react-router-dom";
import { isEmpty, assign } from "lodash/fp";
//import SurveyStepTabAmbiant from "./SurveyStepActionsKpi/SurveyStepTabAmbiant";
const Step = Steps.Step;

class SurveyCreation extends Component {
	componentDidMount() {
		this.props.getSector("GSK");
	}
	state = {
		current: 0,
		status: 0,
		disabled: true,
	};

	next = () => {
		const current = this.state.current + 1;

		this.setState({ current });
	};

	nextStep = currentstep => {
		let current = currentstep + 1;
		this.setState({ current });
	};

	prev = () => {
		const current = this.state.current - 1;
		this.setState({ current });
	};

	prevStep = currentstep => {
		let current = currentstep - 1;
		this.setState({ current });
	};

	continue = index => {
		this.setState({ current: index });
	};

	sucess = ind => {
		message.success("Vous avez fini la creation ou modi");
		let transformSurvey = _.assign([], [this.props.surveyProgress]);
		let data = {
			elements: JSON.stringify(transformSurvey),
			assortmentId: this.props.surveyProgress.assortments,
		};

		this.props.createElement(data);
		this.pushHistory();
		//this.props.getSurveyList(this.props.currentSurvey);
	};
	//processus à améliorer
	pushHistory = () => {
		this.props.history.push("/survey");
	};

	handleDisabled = status => {
		this.setState({ disabled: status });
	};

	render() {
		const {
			steps,
			surveyProgress,
			locations,
			currentSurvey,
			modelClient,
			locatonList,
			sectorList,
		} = this.props;
		const { current, disabled } = this.state;

		return (
			<Fragment>
				<Row gutter={24}>
					<Col span={18}>
						<PaperApp>
							<SubTitle
								size={"16px"}
								marginLeft={"5px"}
								marginTop={"10px"}
								paddingLeft="0"
							>{`Création du questionnaire GSK 2018`}</SubTitle>
							<Fragment>
								{[1].indexOf(1) !== -1 ? (
									<Fragment>
										<Steps current={current}>
											{steps.map((item, index) => (
												<Step
													key={item.title}
													title={item.title}
													//onClick={() => this.continue(index)}
												/>
											))}
										</Steps>
									</Fragment>
								) : (
									<Fragment>
										<Steps current={current}>
											{steps.map((item, index) => (
												<Step key={item.title} title={item.title} />
											))}
										</Steps>
									</Fragment>
								)}
							</Fragment>
							{current === 0 &&
								modelClient.length !== 0 && (
									<SurveyStepModeles
										status={this.state.status}
										steps={steps}
										current={current}
										prev={this.prevStep}
										next={this.nextStep}
										sucess={this.sucess}
										surveyProgress={surveyProgress}
										disabled={disabled}
										handleDisabled={this.handleDisabled}
										current={current}
										modeleList={modelClient}
									/>
								)}
							{current === 1 && (
								<SurveyStepPeriode
									status={this.state.status}
									steps={steps}
									current={current}
									prev={this.prevStep}
									next={this.nextStep}
									sucess={this.sucess}
									surveyProgress={surveyProgress}
									disabled={disabled}
									handleDisabled={this.handleDisabled}
									current={current}
								/>
							)}

							{current === 2 &&
								locatonList.length !== 0 && (
									<SurveyStepEmplacement
										steps={steps}
										current={current}
										prev={this.prevStep}
										next={this.nextStep}
										sucess={this.sucess}
										surveyProgress={surveyProgress}
										locations={locations}
										disabled={disabled}
										handleDisabled={this.handleDisabled}
										current={current}
									/>
								)}
							{current === 3 && (
								<SurveyStepActionsKpi
									steps={steps}
									current={current}
									prev={this.prevStep}
									next={this.next}
									sucess={this.sucess}
									disabled={disabled}
									handleDisabled={this.handleDisabled}
									current={current}
									prevAcess={this.prev}
								/>
							)}
							{current === 4 &&
								sectorList.length !== 0 && (
									<SurveyStepSecteur
										steps={steps}
										current={current}
										prev={this.prevStep}
										next={this.nextStep}
										sucess={this.sucess}
										disabled={disabled}
										handleDisabled={this.handleDisabled}
										current={current}
									/>
								)}
							{current === 5 &&
								!_.isEmpty(currentSurvey) && (
									<SurveyStepSimulation
										steps={steps}
										current={current}
										prev={this.prevStep}
										next={this.nextStep}
										sucess={this.sucess}
										disabled={disabled}
										current={current}
										currentSurvey={currentSurvey}
									/>
								)}
						</PaperApp>
					</Col>
					<Col span={6}>
						{(current === 0 ||
							current === 1 ||
							current === 2 ||
							current === 3 ||
							current === 4 ||
							current === 5) && <Summary />}
					</Col>
				</Row>
			</Fragment>
		);
	}
}

SurveyCreation.defaultProps = {
	steps: [
		{
			title: "Modèle",
		},
		{
			title: "Période",
		},
		{
			title: "Emplacement",
		},
		{
			title: "Actions & Kpis",
		},
		{
			title: "Secteurs",
		},
		{
			title: "Simulation",
		},
	],
};
export default withRouter(SurveyCreation);
