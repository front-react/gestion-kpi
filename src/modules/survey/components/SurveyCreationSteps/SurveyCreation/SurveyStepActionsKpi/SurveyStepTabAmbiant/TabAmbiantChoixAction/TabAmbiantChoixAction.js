import React, { Component, Fragment } from "react";
import { Card, Row, Col, Divider } from "antd";
import Sortable from "react-sortablejs";
import {
	StepCard,
	TitleStep,
	SubTitleStep,
	DraggableItem,
} from "./../../../../../../StyledComponents/StyledComponents";
import ButtonStepAction from "./../../ButtonStepAction";
import _ from "lodash";
import {
	assign,
	zipObject,
	forEach,
	merge,
	pick,
	xor,
	isEqual,
	isEmpty,
	uniqueId,
} from "lodash/fp";

class TabAmbiantChoixAction extends Component {
	state = {
		listEmplacementsSource: ["plv", "releve", "implantation", "mea"],
		listEmplacementsTarget: [],
		statements: [
			{
				statementId: _.uniqueId(),
				conditions: [],
				conditionStatus: true,
				displayType: "inline",
				label: "releve",
				kpis: [],
				medias: [],
				mediaQuantity: [
					{
						type: "Photo",
						quantity: "0",
					},
					{
						type: "Video",
						quantity: "0",
					},
					{
						type: "Audio",
						quantity: "0",
					},
				],
			},
			{
				statementId: _.uniqueId(),
				conditions: [],
				conditionStatus: true,
				displayType: "inline",
				label: "implantation",
				kpis: [],
				medias: [],
				mediaQuantity: [
					{
						type: "Photo",
						quantity: "0",
					},
					{
						type: "Video",
						quantity: "0",
					},
					{
						type: "Audio",
						quantity: "0",
					},
				],
			},
			{
				statementId: _.uniqueId(),
				conditions: [],
				conditionStatus: true,
				displayType: "inline",
				label: "mea",
				kpis: [],
				medias: [],
				mediaQuantity: [
					{
						type: "Photo",
						quantity: "0",
					},
					{
						type: "Video",
						quantity: "0",
					},
					{
						type: "Audio",
						quantity: "0",
					},
				],
			},
			{
				statementId: _.uniqueId(),
				conditions: [],
				conditionStatus: true,
				displayType: "inline",
				label: "plv",
				kpis: [],
				medias: [],
				mediaQuantity: [
					{
						type: "Photo",
						quantity: "0",
					},
					{
						type: "Video",
						quantity: "0",
					},
					{
						type: "Audio",
						quantity: "0",
					},
				],
			},
		],
	};

	componentDidMount() {
		if (
			this.props.surveyProgress.locations.length !== 0 &&
			this.props.surveyProgress.locations[0].statements.length !== 0
		) {
			let locationSurvy = this.props.surveyProgress.locations;
			let labelSelector = this.props.labelSelector;

			let currentLocatiion = [];
			_.forEach(locationSurvy, (location, index) => {
				if (location.label === labelSelector) {
					currentLocatiion.push(location.statements);
				}
			});

			let targetLocation = [];
			_.forEach(currentLocatiion[0], (current, index) => {
				let pickCurrentLocation = _.pick(current, ["label"]);
				targetLocation.push(pickCurrentLocation.label);
			});

			let selectSource = _.xor(
				this.state.listEmplacementsSource,
				targetLocation
			);

			this.setState({
				listEmplacementsSource: selectSource,
				listEmplacementsTarget: targetLocation,
			});

			this.props.handleDisabled(false);
		}
	}

	handleFilterSource = (items, sortable, evt) => {
		//console.log(items);
		this.setState({
			listEmplacementsSource: items,
		});
	};

	handleFilterFilter = (items, sortable, evt) => {
		//console.log(items);
		if (items.length > 0) {
			this.props.handleDisabled(false);
		}
		if (items.length === 0) {
			this.props.handleDisabled(true);
		}
		this.setState({
			listEmplacementsTarget: items,
		});
	};

	next = current => {
		let statements = this.state.statements;
		let surveyProgress = this.props.surveyProgress;
		let labelSelector = this.props.labelSelector;
		let statementsSelectct = [];
		let filterLocationSelect = [];

		_.forEach(statements, (statement, index) =>
			_.forEach(this.state.listEmplacementsTarget, (target, index) => {
				if (target === statement.label) {
					//console.log(statement);
					statementsSelectct.push(statement);
				}
			})
		);
		let zipStatement = _.zipObject(["statements"], [statementsSelectct]);

		_.forEach(surveyProgress.locations, (location, index) => {
			if (location.label === labelSelector) {

				//if (_.isEmpty(surveyProgress.locations[index].statements)) {
					_.assign(surveyProgress.locations[index], zipStatement);
				//}
			}
		});

		this.props.addStepSurvey(surveyProgress);
		this.props.next(current);
	};

	render() {
		const { listEmplacementsSource, listEmplacementsTarget } = this.state;
		const {
			currentAmbiant,
			prev,
			sucess,
			current,
			disabled,
			labelSelector,
			prevAcess,
		} = this.props;
		return (
			<Fragment>
				<StepCard margin="0 auto !important" width="100%" maxWidth="none">
					<TitleStep
						marginBottom={"0px"}
						borderBottom={"none"}
						paddingBottom={"0px"}
					>
						Choix des actions
					</TitleStep>
					<SubTitleStep>
						Sélectionner et glisser les actions dans la colonne de droite afin
						de les ordonner.
					</SubTitleStep>

					<Row
						gutter={16}
						style={{
							display: "flex",
							justifyContent: "center",
							height: "250px",
						}}
					>
						<Col span={7}>
							<Sortable
								style={{
									padding: "5px 0",
									height: "100%",
								}}
								options={{
									animation: 150,
									sort: false,
									group: {
										name: "shared",
										pull: true,
										put: true,
									},
								}}
								onChange={(items, sortable, evt) =>
									this.handleFilterSource(items, sortable, evt)
								}
								tag="div"
							>
								{listEmplacementsSource.map((source, key) => {
									return (
										<DraggableItem key={source} data-id={source}>
											{source}
										</DraggableItem>
									);
								})}
							</Sortable>
						</Col>
						<Col span={2}>
							<Divider type="vertical" style={{ height: "100%" }} />
						</Col>
						<Col span={7}>
							<Sortable
								style={{ padding: "5px 0", height: "100%" }}
								options={{
									animation: 150,
									group: {
										name: "shared",
										pull: true,
										put: true,
									},
								}}
								onChange={(items, sortable, evt) => {
									this.handleFilterFilter(items, sortable, evt);
								}}
								tag="div"
							>
								{listEmplacementsTarget.map((source, key) => {
									return (
										<DraggableItem key={key} data-id={source}>
											{source}
										</DraggableItem>
									);
								})}
							</Sortable>
						</Col>
					</Row>
				</StepCard>
				<ButtonStepAction
					currentAmbiant={currentAmbiant}
					prev={() => prev(currentAmbiant)}
					next={() => this.next(currentAmbiant)}
					sucess={() => sucess(currentAmbiant)}
					current={current}
					disabled={disabled}
					selectEmplacement={labelSelector}
					prevAcess={prevAcess}
				/>
			</Fragment>
		);
	}
}

export default TabAmbiantChoixAction;
