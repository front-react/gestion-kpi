import React, { Component, Fragment } from "react";
import {
	StepCard,
	TitleStep,
	TableKpi,
} from "./../../../../../../StyledComponents/StyledComponents";
import { Checkbox, Tooltip } from "antd";

class SurveyStepTableActionsKpi extends Component {
	render() {
		const { labelSelector, kpisList, listTabs } = this.props;
		console.log(kpisList);
		return (
			<Fragment>
				<StepCard
					margin="0 auto !important"
					width="100%"
					maxWidth="none"
					maxHeight="500px"
					style={{
						overflowY: "auto",
					}}
				>
					<TitleStep
						marginBottom={"0px"}
						borderBottom={"none"}
						paddingBottom={"0px"}
					>
						{`${labelSelector}: Choix des actions et KPIS`}
					</TitleStep>
					<TableKpi>
						<thead>
							<tr>
								<th />
								{listTabs.map((actions, index) => {
									return (
										<th
											class="col"
											style={{ width: "150px", textAlign: "center" }}
										>
											{actions.label}
										</th>
									);
								})}
							</tr>
						</thead>
						<tbody>
							{kpisList.map((kpi, index) => {
								return (
									<tr style={{ height: "50px" }}>
										<th class="row">{kpi.label}</th>
										{listTabs.map((actions, index) => {
											return (
												<td
													style={{ height: 16, width: 16, textAlign: "center" }}
												>
													<Tooltip placement="left" title={actions.label}>
														<Checkbox />
													</Tooltip>
												</td>
											);
										})}
									</tr>
								);
							})}
							
						</tbody>
					</TableKpi>
				</StepCard>
			</Fragment>
		);
	}
}

export default SurveyStepTableActionsKpi;
