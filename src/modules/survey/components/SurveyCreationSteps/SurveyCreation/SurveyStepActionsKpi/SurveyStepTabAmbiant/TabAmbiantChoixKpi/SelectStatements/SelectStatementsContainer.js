import { connect } from "react-redux";
import SelectStatements from "./SelectStatements";
import { addStepSurvey } from "./../../../../../../../redux/actions/progress/post";

const mapStateToProps = (state, ownProps) => {

	return {
		medias: state.survey.surveyManager.medias,
		surveyProgress: state.survey.surveyManager.surveyProgress,
		kpisList: state.survey.surveyManager.kpisList,
		statements: state.survey.surveyManager.statements,
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		addStepSurvey: survey => {
			dispatch(addStepSurvey(survey));
		},
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SelectStatements);
