import { connect } from "react-redux";
import TabAmbiantGestionKpi from "./TabAmbiantGestionKpi";
//import { surveySelector } from "./../../redux/selectors";

const mapStateToProps = (state, ownProps) => {
	return {
		//currentSurvey: surveySelector(state, ownProps),
		surveyProgress: state.survey.surveyManager.surveyProgress,
		statements: state.survey.surveyManager.statements,
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(TabAmbiantGestionKpi);
