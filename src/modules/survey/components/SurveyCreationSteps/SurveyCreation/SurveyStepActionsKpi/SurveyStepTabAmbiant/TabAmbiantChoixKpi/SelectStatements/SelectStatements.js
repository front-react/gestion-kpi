import React, { Component, Fragment } from "react";
import { Card, Select, Radio, Input, Divider } from "antd";
import {
	TitleStep,
	SubTitle,
	ContentCard,
} from "./../../../../../../../StyledComponents/StyledComponents";
import _ from "lodash";
import {
	forEach,
	update,
	assign,
	filter,
	map,
	pick,
	partialRight,
	zipObject,
	uniqueId,
} from "lodash/fp";
const Option = Select.Option;
const InputGroup = Input.Group;

class SelectStatements extends Component {
	state = {
		statementSelect: [],
		typeRelve: 1,
		displayType: "inline",
		question: "",
	};
	componentDidMount() {
		let currentKpi = [];

		_.forEach(
			_.map(this.props.statement.kpis, _.partialRight(_.pick, ["label"])),
			(kpi, index) => {
				currentKpi.push(kpi.label);
			}
		);
		this.setState({ statementSelect: currentKpi });

		if (currentKpi.length !== 0) {
			this.props.handleDisabled(false);
		}
	}

	handleChange = evt => {
		if (evt.length > 0) {
			this.props.handleDisabled(false);
		}
		if (evt.length === 0) {
			this.props.handleDisabled(true);
		}
		this.setState({ statementSelect: evt });
		let labelStatement = this.props.labelStatement;
		let labelSelector = this.props.labelSelector;
		let surveyProgress = this.props.surveyProgress;
		let choixManager = evt;
		let listKpi = this.props.kpisList;
		let kpiDeleteLabel = [];
		let indexLocation = null;
		let indexStatement = null;

		let selectKpiManager = [];
		_.forEach(listKpi, (labelKpi, index) => {
			_.forEach(choixManager, (choixKpi, index) => {
				if (labelKpi.label === choixKpi) {
					selectKpiManager.push(_.assign(labelKpi, { response: [] }));
				}
			});
		});
		_.forEach(surveyProgress.locations, (location, indexLocation) => {
			if (location.label === labelSelector) {
				indexLocation = indexLocation;
				_.forEach(location.statements, (statement, indexStatement) => {
					if (statement.label === labelStatement) {
						indexStatement = indexStatement;
						let zipChoixManager = _.zipObject(["kpis"], [selectKpiManager]);
						_.assign(
							surveyProgress.locations[indexLocation].statements[
								indexStatement
							],
							zipChoixManager
						);
					}
				});
			}
		});
		this.props.addStepSurvey(surveyProgress);
	};

	handleChangeType = e => {
		this.setState({ typeRelve: e.target.value });
	};

	handleKeyup = (evt, name) => {
		let labelStatement = this.props.labelStatement;
		let labelSelector = this.props.labelSelector;
		let surveyProgress = this.props.surveyProgress;

		let indexLocation = null;
		let indexStatement = null;
		let indexMedia = null;

		let generateTypeMedia = (name, quantity, indexLocation, indexStatement) => {
			for (let i = 0; i < parseInt(quantity); i++) {
				surveyProgress.locations[indexLocation].statements[
					indexStatement
				].medias.push({ type: name });
			}
		};

		_.forEach(surveyProgress.locations, (location, index) => {
			if (location.label === labelSelector) {
				indexLocation = index;
				_.forEach(location.statements, (statement, index) => {
					if (statement.label === labelStatement) {
						indexStatement = index;
						switch (name) {
							case "Photo":
								indexMedia = 0;
								generateTypeMedia(
									name,
									evt.target.value,
									indexLocation,
									indexStatement
								);
								_.assign(
									surveyProgress.locations[indexLocation].statements[
										indexStatement
									].mediaQuantity[indexMedia],
									{
										type: name,
										quantity: evt.target.value,
									}
								);
								break;
							case "Video":
								indexMedia = 1;
								generateTypeMedia(
									name,
									evt.target.value,
									indexLocation,
									indexStatement
								);
								_.assign(
									surveyProgress.locations[indexLocation].statements[
										indexStatement
									].mediaQuantity[indexMedia],
									{
										type: name,
										quantity: evt.target.value,
									}
								);
								break;
							case "Audio":
								indexMedia = 2;
								generateTypeMedia(
									name,
									evt.target.value,
									indexLocation,
									indexStatement
								);
								_.assign(
									surveyProgress.locations[indexLocation].statements[
										indexStatement
									].mediaQuantity[indexMedia],
									{
										type: name,
										quantity: evt.target.value,
									}
								);
								break;
							default:
						}
					}
				});
			}
		});

		this.props.addStepSurvey(surveyProgress);
	};

	handleAffichageChange = event => {
		let labelStatement = this.props.labelStatement;
		let labelSelector = this.props.labelSelector;
		let surveyProgress = this.props.surveyProgress;
		let indexLocation = null;
		let indexStatement = null;

		_.forEach(surveyProgress.locations, (location, indexLocation) => {
			if (location.label === labelSelector) {
				indexLocation = indexLocation;
				_.forEach(location.statements, (statement, indexStatement) => {
					if (statement.label === labelStatement) {
						indexStatement = indexStatement;
						//let zipChoixManager = _.zipObject(["kpis"], [selectKpiManager]);
						_.assign(
							surveyProgress.locations[indexLocation].statements[
								indexStatement
							],
							{ displayType: event.target.value }
						);
					}
				});
			}
		});
		this.setState({ displayType: event.target.value });
		this.props.addStepSurvey(surveyProgress);
	};

	handleCondition = id => {
		//console.log(id.target.value);
		this.setState({ question: id.target.value });
		let labelStatement = this.props.labelStatement;
		let labelSelector = this.props.labelSelector;
		let surveyProgress = this.props.surveyProgress;
		let indexLocation = null;
		let indexStatement = null;

		_.forEach(surveyProgress.locations, (location, indexLocation) => {
			if (location.label === labelSelector) {
				indexLocation = indexLocation;
				_.forEach(location.statements, (statement, indexStatement) => {
					if (statement.label === labelStatement) {
						indexStatement = indexStatement;
						let zipChoixCondition = _.zipObject(
							["conditions"],
							[
								{
									conditionId: _.uniqueId(),
									question: this.state.question,
									status: "false",
								},
							]
						);
						_.assign(
							surveyProgress.locations[indexLocation].statements[
								indexStatement
							],
							zipChoixCondition
						);
						_.assign(
							surveyProgress.locations[indexLocation].statements[
								indexStatement
							],
							{ conditionStatus: false }
						);
					}
				});
			}
		});
	};

	render() {
		const { statement, labelStatement, kpisList, listReleve } = this.props;
		const { statementSelect, typeRelve } = this.state;
		let currentKpi = [];

		_.forEach(
			_.map(statement.kpis, _.partialRight(_.pick, ["label"])),
			(kpi, index) => {
				currentKpi.push(kpi.label);
			}
		);
		return (
			<Fragment>
				<SubTitle marginTop={"20px"} marginLeft={"0px"} marginBottom={"0px"}>
					{`${statement.label}`}:
				</SubTitle>
				<ContentCard>
					<Input
						placeholder="Question condition"
						onChange={this.handleCondition}
					/>
				</ContentCard>
				<ContentCard marginTop={"10px"}>
					<Select
						mode="tags"
						//size={statementSelect}
						defaultValue={currentKpi}
						placeholder="Please select"
						onChange={this.handleChange}
						style={{ width: "100%" }}
					>
						{kpisList.map((element, index) => {
							return <Option key={element.label}>{element.label}</Option>;
						})}
					</Select>
				</ContentCard>

				<ContentCard marginTop={"10px"}>
					<Radio.Group
						//value={typeRelve}
						onChange={this.handleChangeType}
						style={{ display: "flex" }}
					>
						{statement.mediaQuantity.map((releve, index) => {
							return (
								<div key={index}>
									<InputGroup compact>
										<Radio.Button value={releve.type}>
											{releve.type}
										</Radio.Button>
										<Input
											placeholder={releve.quantity}
											style={{ width: "30%" }}
											onKeyUp={evt => this.handleKeyup(evt, releve.type)}
										/>
									</InputGroup>
								</div>
							);
						})}
					</Radio.Group>
				</ContentCard>
				<ContentCard>
					<span>Affichage :</span>
					<Radio.Group
						value={this.state.displayType}
						onChange={this.handleAffichageChange}
					>
						<Radio.Button value="matrix">Matrice</Radio.Button>
						<Radio.Button value="inline">En ligne</Radio.Button>
					</Radio.Group>
				</ContentCard>
			</Fragment>
		);
	}
}
SelectStatements.defaultProps = {
	listReleve: [
		{
			id: 1,
			name: "Photo",
		},
		{
			id: 2,
			name: "Video",
		},
		{
			id: 3,
			name: "Audio",
		},
	],
};
export default SelectStatements;
