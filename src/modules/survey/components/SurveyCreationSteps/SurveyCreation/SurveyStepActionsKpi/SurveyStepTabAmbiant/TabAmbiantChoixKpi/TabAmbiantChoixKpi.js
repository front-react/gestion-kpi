import React, { Component, Fragment } from "react";
import { Card, Select, Radio, Input, Divider } from "antd";
import {
	TitleStep,
	StepCard,
	SubTitle,
	ContentCard,
} from "./../../../../../../StyledComponents/StyledComponents";
import ButtonStepAction from "./../../ButtonStepAction";
import SelectStatements from "./SelectStatements";
const Option = Select.Option;
const InputGroup = Input.Group;

class TabAmbiantChoixKpi extends Component {
	state = {
		mea: {},
		pdv: {},
		implantation: {},
		releve: {},
		typeRelve: 1,
	};

	next = current => {
		this.props.next(current);
	};
	render() {
		const {
			kpisList,
			listReleve,
			currentAmbiant,
			prev,
			sucess,
			labelSelector,
			selectLocation,
			current,
			disabled,
			handleDisabled,
			prevAcess,
		} = this.props;
		return (
			<Fragment>
				<StepCard
					margin="0 auto !important"
					width="100%"
					maxWidth="none"
					maxHeight="500px"
					style={{
						overflowY: "auto",
					}}
				>
					<Fragment>
						<TitleStep color={`#8A8A8A`}>choix des Kpis</TitleStep>
						{Object.keys(selectLocation.statements).map((statement, index) => (
							<div key={index}>
								<SelectStatements
									labelStatement={selectLocation.statements[statement].label}
									labelSelector={labelSelector}
									statement={selectLocation.statements[statement]}
									kpisList={kpisList}
									handleDisabled={handleDisabled}
								/>
							</div>
						))}
					</Fragment>
				</StepCard>
				<ButtonStepAction
					currentAmbiant={currentAmbiant}
					prev={() => prev(currentAmbiant)}
					next={() => this.next(currentAmbiant)}
					sucess={() => sucess(currentAmbiant)}
					current={current}
					disabled={disabled}
					selectEmplacement={labelSelector}
					prevAcess={prevAcess}
				/>
			</Fragment>
		);
	}
}
TabAmbiantChoixKpi.defaultProps = {
	listReleve: [
		{
			id: 1,
			name: "Photo",
		},
		{
			id: 2,
			name: "Vidéo",
		},
		{
			id: 3,
			name: "Audio",
		},
	],
};
export default TabAmbiantChoixKpi;
