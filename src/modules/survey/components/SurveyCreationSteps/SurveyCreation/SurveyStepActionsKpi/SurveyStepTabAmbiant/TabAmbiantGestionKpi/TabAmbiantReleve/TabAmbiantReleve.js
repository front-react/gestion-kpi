import React, { Component, Fragment } from "react";
import { SubTitle } from "./../../../../../../../StyledComponents/StyledComponents";
import { Input, Button } from "antd";

import {
	InputField,
	InputSubmitOutline,
} from "./../../../../../../../StyledComponents/StyledComponents";
import _ from "lodash";
import { forEach } from "lodash/fp";
class TabAmbiantReleve extends Component {
	state = {};
	handleSubmit = event => {
		event.preventDefault();
		let labelKpi = this.props.labelKpi;
		let labelSelector = this.props.labelSelector;
		let listQuestion = this.state;
		let keyOfSelectKpi = Object.keys(listQuestion);
		let surveyManager = this.props.surveyProgress;
		let indexLocation = null;
		let indexStatement = null;
		let indexKpi = null;

		_.forEach(surveyManager.locations, (location, index) => {
			if (location.label === labelSelector) {
				indexLocation = index;
				_.forEach(
					surveyManager.locations[indexLocation].statements,
					(statement, index) => {
						if (statement.label === labelKpi) {
							indexStatement = index;
						}
					}
				);
			}
		});

		_.forEach(
			surveyManager.locations[indexLocation].statements[indexStatement].kpis,
			(kpi, index) => {
				indexKpi = index;
				_.filter(keyOfSelectKpi, (selectKpi, index) => {
					if (kpi.label === selectKpi) {
						_.assign(
							surveyManager.locations[indexLocation].statements[indexStatement]
								.kpis[indexKpi],
							{ question: listQuestion[selectKpi] }
						);
					}
				});
			}
		);
		this.setState({});
		this.props.addStepSurvey(surveyManager);
	};
	handleChangeKpi = event => {
		const { name, value, id } = event.target;
		this.setState({ [name]: value });
	};
	render() {
		const { KpiListManager } = this.props;

		return (
			<div>
				<Fragment>
					<form onSubmit={this.handleSubmit}>
						{KpiListManager[0].map((kpi, index) => {

							return (
								<Fragment key={kpi.label}>
									<SubTitle marginTop="0" marginBottom="5px" paddingLeft="0">
										{kpi.label}
									</SubTitle>
									<InputField
										id={`${kpi._id}`}
										name={`${kpi.label}`}
										type="text"
										placeholder={kpi.question}
										onChange={this.handleChangeKpi}
									/>
								</Fragment>
							);
						})}
						<InputSubmitOutline type="submit" value="Enregistrer" />
					</form>
				</Fragment>
			</div>
		);
	}
}

export default TabAmbiantReleve;
