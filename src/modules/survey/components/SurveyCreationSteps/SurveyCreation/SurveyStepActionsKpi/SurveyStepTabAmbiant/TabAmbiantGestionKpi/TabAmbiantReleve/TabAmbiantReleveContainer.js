import { connect } from "react-redux";
import TabAmbiantReleve from "./TabAmbiantReleve";
import { kpiLocationStatementSelector } from "./../../../../../../../redux/selectors";
import { addStepSurvey } from "./../../../../../../../redux/actions/progress/post";

const mapStateToProps = (state, ownProps) => {
	return {
		surveyProgress: state.survey.surveyManager.surveyProgress,
		KpiListManager: kpiLocationStatementSelector(state, ownProps),
		labelKpi: ownProps.labelKpi,
		labelSelector: ownProps.labelSelector,
		statements: state.survey.surveyManager.statements,
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		addStepSurvey: survey => {
			dispatch(addStepSurvey(survey));
		},
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(TabAmbiantReleve);
