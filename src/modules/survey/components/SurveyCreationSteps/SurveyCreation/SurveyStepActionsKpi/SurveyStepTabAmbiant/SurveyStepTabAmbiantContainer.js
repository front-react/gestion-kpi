import { connect } from "react-redux";
import SurveyStepTabAmbiant from "./SurveyStepTabAmbiant";
//import { surveySelector } from "./../../redux/selectors";
import {getKpiList} from "./../../../../../redux/actions/dashboard/get"


const mapStateToProps = (state, ownProps) => {
	return {
		//currentSurvey: surveySelector(state, ownProps),
		kpisList: state.survey.surveyManager.kpisList,
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		getKpiList: () => {
			dispatch(getKpiList());
		},
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SurveyStepTabAmbiant);
