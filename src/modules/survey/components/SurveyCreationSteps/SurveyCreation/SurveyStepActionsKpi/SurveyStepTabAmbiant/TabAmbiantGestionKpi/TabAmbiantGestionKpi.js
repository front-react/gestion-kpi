import React, { Component, Fragment } from "react";
import { TabListFree } from "./../../../../../TabsList";
import { Card } from "antd";
import { StepCard } from "./../../../../../../StyledComponents/StyledComponents";
import ButtonStepAction from "./../../ButtonStepAction";
import _ from "lodash";
import { assign, zipObject, forEach, merge } from "lodash/fp";

const ContentActions = ({ listTabs }) => {
	return (
		<div>
			<TabListFree
				tabsList={listTabs}
				position={"top"}
				tabBarStyle={{ margin: 0 }}
				render={({ ContentTab }) => {
					if (ContentTab) return ContentTab;
				}}
			/>
		</div>
	);
};

class TabAmbiantGestionKpi extends Component {
	state = {
		TabListSelect: [],
	};
	componentDidMount() {
		let listTabs = this.props.listTabs;
		let surveyProgress = this.props.surveyProgress;
		let labelSelector = this.props.labelSelector;
		let TabListSelect = [];
		_.forEach(surveyProgress.locations, (location, index) => {
			if (location.label === labelSelector) {
				_.forEach(location.statements, (statement, index) => {
					listTabs.filter(list => {
						if (list.label === statement.label) {
							TabListSelect.push(list);
						}
					});
				});
			}
		});

		this.setState({ TabListSelect: TabListSelect });
	}
	next = current => {
		this.props.next(current);
	};
	render() {
		const {
			currentAmbiant,
			sucess,
			prev,
			current,
			disabled,
			labelSelector,
			prevAcess,
		} = this.props;
		const { TabListSelect } = this.state;

		return (
			<Fragment>
				<ContentActions listTabs={TabListSelect} />
				<ButtonStepAction
					currentAmbiant={currentAmbiant}
					prev={() => prev(currentAmbiant)}
					next={() => this.next(currentAmbiant)}
					sucess={() => sucess(currentAmbiant)}
					current={current}
					disabled={disabled}
					selectEmplacement={labelSelector}
					prevAcess={prevAcess}
				/>
			</Fragment>
		);
	}
}

export default TabAmbiantGestionKpi;
