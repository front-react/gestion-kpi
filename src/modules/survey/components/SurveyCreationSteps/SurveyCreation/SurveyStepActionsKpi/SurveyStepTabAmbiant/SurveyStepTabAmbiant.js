import React, { Component, Fragment } from "react";
import {
	Card,
	Select,
	Radio,
	Input,
	Divider,
	Button,
	Icon,
	message,
} from "antd";
import {
	TitleStep,
	SubTitle,
	ContentCard,
} from "./../../../../../StyledComponents/StyledComponents";
import TabAmbiantGestionKpi from "./TabAmbiantGestionKpi";
import TabAmbiantChoixKpi from "./TabAmbiantChoixKpi";
import TabAmbiantChoixAction from "./TabAmbiantChoixAction";

import SurveyStepTableActionsKpi from "./SurveyStepTableActionsKpi";

import TabAmbiantReleve from "./TabAmbiantGestionKpi/TabAmbiantReleve";
import _ from "lodash";
import { isEmpty } from "lodash/fp";

class SurveyStepTabAmbiant extends Component {
	// componentDidMount() {
	// 	if (_.isEmpty(this.props.kpisList)) {
	// 		this.props.getKpiList();
	// 	}
	// }
	state = {
		currentAmbiant: 0,
		disabled: true,
	};
	next = () => {
		const currentAmbiant = this.state.currentAmbiant + 1;
		this.setState({ currentAmbiant, disabled: true });
	};

	prev = () => {
		const currentAmbiant = this.state.currentAmbiant - 1;
		this.setState({ currentAmbiant });
	};
	sucess = current => {
		this.props.sucess();
	};
	handleDisabled = status => {
		this.setState({ disabled: status });
	};
	render() {
		const { currentAmbiant, disabled } = this.state;
		const {
			kpisList,
			labelSelector,
			surveyProgress,
			current,
			prevAcess,
		} = this.props;
		const listTabs = [
			{
				label: "releve",
				component: (
					<TabAmbiantReleve labelKpi={"releve"} labelSelector={labelSelector} />
				),
			},
			{
				label: "implantation",
				component: (
					<TabAmbiantReleve
						labelKpi={"implantation"}
						labelSelector={labelSelector}
					/>
				),
			},
			{
				label: "mea",
				component: (
					<TabAmbiantReleve labelKpi={"mea"} labelSelector={labelSelector} />
				),
			},
			{
				label: "plv",
				component: (
					<TabAmbiantReleve labelKpi={"plv"} labelSelector={labelSelector} />
				),
			},
		];
		return (
			<Fragment>
				<SurveyStepTableActionsKpi
					labelSelector={labelSelector}
					kpisList={kpisList}
					listTabs={listTabs}
				/>
				{/* {currentAmbiant === 0 && (
					<TabAmbiantChoixAction
						prev={this.prev}
						next={this.next}
						sucess={this.sucess}
						currentAmbiant={currentAmbiant}
						labelSelector={labelSelector}
						handleDisabled={this.handleDisabled}
						current={current}
						disabled={disabled}
						prevAcess={prevAcess}
					/>
				)}
				{currentAmbiant === 1 && (
					<TabAmbiantChoixKpi
						prev={this.prev}
						next={this.next}
						sucess={this.sucess}
						currentAmbiant={currentAmbiant}
						labelSelector={labelSelector}
						handleDisabled={this.handleDisabled}
						current={current}
						disabled={disabled}
						prevAcess={prevAcess}
					/>
				)}
				{currentAmbiant === 2 && (
					<TabAmbiantGestionKpi
						prev={this.prev}
						next={this.next}
						sucess={this.sucess}
						currentAmbiant={currentAmbiant}
						labelSelector={labelSelector}
						listTabs={listTabs}
						handleDisabled={this.handleDisabled}
						current={current}
						disabled={disabled}
						prevAcess={prevAcess}
					/>
				)} */}
			</Fragment>
		);
	}
}

export default SurveyStepTabAmbiant;
