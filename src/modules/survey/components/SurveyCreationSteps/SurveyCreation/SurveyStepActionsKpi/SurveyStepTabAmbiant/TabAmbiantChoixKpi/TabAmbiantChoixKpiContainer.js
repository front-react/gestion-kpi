import { connect } from "react-redux";
import TabAmbiantChoixKpi from "./TabAmbiantChoixKpi";
import { locationSelector } from "./../../../../../../redux/selectors";

const mapStateToProps = (state, ownProps) => {
	return {
		//currentSurvey: surveySelector(state, ownProps),
		selectLocation: locationSelector(state, ownProps),
		statements: state.survey.surveyManager.statements,
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(TabAmbiantChoixKpi);
