import { connect } from "react-redux";
import SurveyStepActionsKpi from "./SurveyStepActionsKpi";
//import { surveySelector } from "./../../redux/selectors";
import {addStepSurvey} from "./../../../../redux/actions/progress/post"

const mapStateToProps = (state, ownProps) => {
	return {
		//currentSurvey: surveySelector(state, ownProps),
		surveyProgress: state.survey.surveyManager.surveyProgress,
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		addStepSurvey: survey => {
			dispatch(addStepSurvey(survey));
		},
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SurveyStepActionsKpi);
