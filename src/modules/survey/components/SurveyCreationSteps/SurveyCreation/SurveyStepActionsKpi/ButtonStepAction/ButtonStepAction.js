import React, { Component, Fragment } from "react";
import { Button, Icon, Divider } from "antd";

class ButtonStepAction extends Component {
	render() {
		const {
			currentAmbiant,
			prev,
			next,
			sucess,
			selectEmplacement,
			current,
			disabled,
			prevAcess,
		} = this.props;
		return (
			<Fragment>
				{current === 3 && (
					<div
						style={{
							position: "fixed",
							bottom: "60px",
							width: "65%",
							textAlign: "center",
							left: "160px",
						}}
					>
						<Button.Group size={"large"} style={{ display: "flex" }}>
							{currentAmbiant > 0 && (
								<Button
									type="ghost"
									shape="circle"
									onClick={prev}
									style={{
										marginLeft: 8,
										width: "130px",
									}}
								>
									<Icon type="left" />
									Précédent
								</Button>
							)}
							{currentAmbiant === 0 && (
								<Button
									type="ghost"
									shape="circle"
									onClick={prevAcess}
									style={{
										marginLeft: 8,
										width: "130px",
									}}
								>
									<Icon type="left" />
									Précédent
								</Button>
							)}
							{currentAmbiant < 2 && (
								<Button
									type="primary"
									shape="circle"
									onClick={next}
									style={{
										width: "130px",
										position: "absolute",
										right: 0,
									}}
									disabled={disabled}
								>
									Suivant <Icon type="right" theme="outlined" />
								</Button>
							)}
							{currentAmbiant === 2 && (
								<Button
									type="primary"
									onClick={sucess}
									style={{
										width: "130px",
										position: "absolute",
										right: 0,
									}}
								>
									<Icon type="check" theme="outlined" />
									{`${selectEmplacement}`}
								</Button>
							)}
						</Button.Group>
					</div>
				)}
			</Fragment>
		);
	}
}

ButtonStepAction.defaultProps = {
	selectEmplacement: {
		label: "Ambiant",
	},
};

export default ButtonStepAction;
