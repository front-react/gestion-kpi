import React, { Component, Fragment } from "react";
import { TabsList } from "./../../../TabsList";
import { StepDiv } from "./../../../../StyledComponents/StyledComponents";
import ButtonStep from "./../../../ButtonStep";
import SurveyStepTabAmbiant from "./SurveyStepTabAmbiant";
import _ from "lodash";
import { toString, parseInt, forEach } from "lodash/fp";

const ContentActions = ({ listTabs, ActiveKey, ...res }) => {
	return (
		<div>
			<TabsList
				tabsList={listTabs}
				position={"top"}
				ActiveKey={ActiveKey}
				type="line"
				render={({ ContentTab }) => {
					if (ContentTab) return ContentTab;
				}}
			/>
		</div>
	);
};

class SurveyStepActionsKpi extends Component {
	componentDidMount() {
		this.props.addStepSurvey(this.props.surveyProgress);
	}
	sucess = () => {
		if (this.state.ActiveKey <= this.state.lenghtTabAction) {
			this.setState({ ActiveKey: this.state.ActiveKey + 1 });
		}
		if (this.state.ActiveKey === this.state.lenghtTabAction) {
			this.props.next();
		}
		if (this.state.ActiveKey === -1) {
			this.props.prev();
		}
	};
	state = {
		listTabsSelect: [],
		ActiveKey: 0,
		// listTabs: [
		// 	{
		// 		id: 1,
		// 		label: "Ambiant",
		// 		component: (
		// 			<SurveyStepTabAmbiant
		// 				sucess={this.sucess}
		// 				labelSelector={"Ambiant"}
		// 				handleDisabled={this.props.handleDisabled}
		// 				current={this.props.current}
		// 				prevAcess={this.props.prevAcess}
		// 			/>
		// 		),
		// 	},
		// 	{
		// 		id: 2,
		// 		label: "Lineaire",
		// 		component: (
		// 			<SurveyStepTabAmbiant
		// 				sucess={this.sucess}
		// 				labelSelector={"Lineaire"}
		// 				current={this.props.current}
		// 				prevAcess={this.props.prevAcess}
		// 			/>
		// 		),
		// 	},
		// 	{
		// 		id: 3,
		// 		label: "Frigo",
		// 		component: (
		// 			<SurveyStepTabAmbiant
		// 				sucess={this.sucess}
		// 				labelSelector={"Frigo"}
		// 				current={this.props.current}
		// 				prevAcess={this.props.prevAcess}
		// 			/>
		// 		),
		// 	},
		// ],
		listTabs: (index, label) => {
			return {
				id: index,
				label: label,
				component: (
					<SurveyStepTabAmbiant
						sucess={this.sucess}
						labelSelector={label}
						current={this.props.current}
						prevAcess={this.props.prevAcess}
					/>
				),
			};
		},
		lenghtTabAction: null,
	};
	componentDidMount() {
		this.handleFilterActionJson();
	}
	handleFilterActionJson = () => {
		let surveyProgress = this.props.surveyProgress;
		//let listTabs = this.props.listTabs;
		let listSelect = [];

		_.forEach(surveyProgress.locations, (location, index) => {
			let locationOrigin = this.state.listTabs(index, location.label);
			listSelect.push(locationOrigin);
		});
		// this.state.listTabs.filter(tab =>
		// 	surveyProgress.locations.filter(action => {
		// 		if (action.label === tab.label) {
		// 			listSelect.push(tab);
		// 		}
		// 	})
		// );
		this.setState({
			listTabsSelect: listSelect,
			lenghtTabAction: listSelect.length - 1,
		});
	};

	render() {
		const { listTabsSelect, ActiveKey } = this.state;
		const {
			current,
			steps,
			prev,
			next,
			sucess,
			disabled,
			handleDisabled,
		} = this.props;
		return (
			<Fragment>
				<StepDiv maxWidth={"1000px"}>
					<ContentActions
						listTabs={listTabsSelect}
						ActiveKey={`${ActiveKey}`}
						//handleDisabled={handleDisabled}
					/>
				</StepDiv>
				<ButtonStep
					current={current}
					steps={steps}
					prev={() => prev(current)}
					next={() => next(current)}
					sucess={() => sucess(current)}
					disabled={disabled}
				/>
			</Fragment>
		);
	}
}
SurveyStepActionsKpi.defaultProps = {
	surveyProgress: [
		{
			id: 1,
			actionName: "Ambiant",
		},
		{
			id: 2,
			actionName: "Frigo",
		},
	],
};

export default SurveyStepActionsKpi;
