import React, { Component, Fragment } from "react";
import { DatePicker, LocaleProvider, Card } from "antd";
import frFR from "antd/lib/locale-provider/fr_FR";
import moment from "moment";
import {
	TitleStep,
	StepCard,
} from "./../../../../StyledComponents/StyledComponents";
import "moment/locale/fr";
import ButtonStep from "./../../../ButtonStep";
import _ from "lodash";
import { assign, zipObject, isEmpty } from "lodash/fp";
const { RangePicker } = DatePicker;

class SurveyStepPeriode extends Component {
	state = {
		label: "label GSK",
		status: 1,
		startDate: "",
		endDate: "",
	};
	componentDidMount() {
		if (_.isEmpty(this.props.surveyProgress.periodicity)) {
			this.props.handleDisabled(true);
		} else {
			this.setState({
				startDate: this.props.surveyProgress.periodicity.startDate,
				endDate: this.props.surveyProgress.periodicity.endDate,
			});
			this.props.handleDisabled(false);
		}
	}
	handleRangeDate = (dates, dateStrings) => {
		if (dateStrings[0].length === 0 && dateStrings[1].length === 0) {
			this.setState({ startDate: "", endDate: "" });
			this.props.handleDisabled(true);
			let surveyProgress = this.props.surveyProgress;
			let zipPeriodicity = zipObject(["periodicity"], [{}]);
			let surveyPeriod = assign(surveyProgress, zipPeriodicity);
			this.props.addStepSurvey(surveyPeriod);
		}

		if (dateStrings[0].length !== 0 && dateStrings[1].length !== 0) {
			let dateBegin = moment(
				`${dateStrings[0]} 12:00:00`,
				"DD-MM-YYYY HH:mm:ss"
			).format("YYYY-MM-DDTHH:mm:ss.SSSSZ");
			let dateEnd = moment(
				`${dateStrings[1]} 12:00:00`,
				"DD-MM-YYYY HH:mm:ss"
			).format("YYYY-MM-DDTHH:mm:ss.SSSSZ");
			this.setState({ startDate: dateBegin, endDate: dateEnd });
			this.props.handleDisabled(false);
			let surveyProgress = this.props.surveyProgress;
			let period = {
				label: "label GSK",
				startDate: dateBegin,
				endDate: dateEnd,
				status: this.state.status,
			};
			let zipPeriodicity = zipObject(["periodicity"], [period]);
			let surveyPeriod = assign(surveyProgress, zipPeriodicity);

			this.props.addStepSurvey(surveyPeriod);
		}
	};
	next = current => {
		//let surveyProgress = this.props.surveyProgress;
		//let period = this.state;
		//let zipPeriodicity = _.zipObject(["periodicity"], [period]);
		//let surveyPeriod = _.assign(surveyProgress, zipPeriodicity);
		let surveyPeriod = this.props.surveyProgress;
		this.props.addStepSurvey(surveyPeriod);
		this.props.next(current);
		this.props.handleDisabled(true);
	};
	render() {
		const {
			current,
			steps,
			prev,
			disabled,
			surveyProgress,
		} = this.props;
		
		return (
			<Fragment>
				<StepCard align="center">
					<TitleStep marginBottom="40px">Période</TitleStep>
					<LocaleProvider locale={frFR}>
						<RangePicker
							onChange={(dates, dateStrings) =>
								this.handleRangeDate(dates, dateStrings)
							}
							size={"large"}
							format={"DD-MM-YYYY"}
							disabledDate={current => {
								if (!_.isEmpty(surveyProgress.model[0].periodicity)) {
									return (
										current <
											moment(
												surveyProgress.model[0].periodicity.startDate,
												"YYYY-MM-DDTHH:mm:ss.SSSSZ"
											) ||
										current >
											moment(
												surveyProgress.model[0].periodicity.endDate,
												"YYYY-MM-DDTHH:mm:ss.SSSSZ"
											)
									);
								}
							}}
							value={
								!_.isEmpty(surveyProgress.periodicity) && [
									moment(
										surveyProgress.periodicity.startDate,
										"YYYY-MM-DDTHH:mm:ss.SSSSZ"
									),
									moment(
										surveyProgress.periodicity.endDate,
										"YYYY-MM-DDTHH:mm:ss.SSSSZ"
									),
								]
							}
							defaultValue={
								!_.isEmpty(surveyProgress.model[0].periodicity) && [
									moment(
										surveyProgress.model[0].periodicity.startDate,
										"YYYY-MM-DDTHH:mm:ss.SSSSZ"
									),
									moment(
										surveyProgress.model[0].periodicity.endDate,
										"YYYY-MM-DDTHH:mm:ss.SSSSZ"
									),
								]
							}
						/>
					</LocaleProvider>
				</StepCard>
				<ButtonStep
					current={current}
					steps={steps}
					prev={() => prev(current)}
					next={() => this.next(current)}
					disabled={disabled}
					current={current}
				/>
			</Fragment>
		);
	}
}
export default SurveyStepPeriode;
