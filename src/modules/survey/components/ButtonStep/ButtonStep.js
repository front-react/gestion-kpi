import React, { Component, Fragment } from "react";
import { Divider, Icon, Button } from "antd";
import { Link } from "react-router-dom";

class ButtonStep extends Component {
	render() {
		const { current, steps, prev, next, sucess, disabled } = this.props;
		return (
			<Fragment>
				{current !== 3 && (
					<div
						style={{
							position: "fixed",
							bottom: "60px",
							width: "65%",
							textAlign: "center",
						}}
					>
						<Button.Group size={"large"} style={{ display: "flex" }}>
							{current > 0 && (
								<Fragment>
									<Button
										//disabled={true}
										type="ghost"
										style={{
											marginLeft: 8,
											width: "130px",
										}}
										onClick={current => prev(current)}
									>
										<Icon type="left" />
										Précédent
									</Button>
									<Divider type="vertical" />
								</Fragment>
							)}
							{current < steps.length - 1 && (
								<Fragment>
									<Button
										style={{
											width: "130px",
											position: "absolute",
											right: 0,
										}}
										type="primary"
										onClick={current => next(current)}
										disabled={disabled}
									>
										Suivant <Icon type="right" />
									</Button>
								</Fragment>
							)}
							{current === steps.length - 1 && (
								<Fragment>
									<Button
										style={{
											width: "130px",
											position: "absolute",
											right: 0,
										}}
										type="primary"
										onClick={current => sucess(current)}
									>
										Fin <Icon type="crown" theme="outlined" />
									</Button>
								</Fragment>
							)}
						</Button.Group>
					</div>
				)}
			</Fragment>
		);
	}
}

export default ButtonStep;
