import { connect } from "react-redux";
import QuestionByKpi from "./QuestionByKpi";
//import { getAllKpiQuestions } from "./../../../../redux/actions/dashboard/get";
import { questionSelector } from "./../../../../redux/selectors";

const mapStateToProps = (state, ownProps) => {
  return {
    allKpiQuestions: questionSelector(state, ownProps),
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    // getAllKpiQuestions: () => {
    //   dispatch(getAllKpiQuestions());
    // },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(QuestionByKpi);
