import { connect } from "react-redux";
import KpiListSelect from "./KpiListSelect";

const mapStateToProps = (state, ownProps) => {
  return {
    kpisList: state.survey.surveyManager.kpisList,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(KpiListSelect);
