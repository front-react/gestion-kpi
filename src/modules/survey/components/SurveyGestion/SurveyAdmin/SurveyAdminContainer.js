import { connect } from "react-redux";
import SurveyAdmin from "./SurveyAdmin";

const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(SurveyAdmin);
