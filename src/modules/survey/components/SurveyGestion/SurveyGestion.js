import React, { Component, Fragment } from "react";
import { PaperApp, Title } from "./../../StyledComponents/StyledComponents";
import SurveyList from "./SurveyList";
import { withRouter } from "react-router-dom";
import store from "store";

class SurveyGestion extends Component {
	state = {
		status: 0,
	};

	handleStatusSelect = e => {
		this.setState({ status: e });
	};
	render() {
		return (
			<PaperApp>
				<Title>{`Gestion du questionnaire GSK 2018`}</Title>

				<SurveyList
					handleStatusSelect={this.handleStatusSelect}
					status={this.state.status}
				/>
			</PaperApp>
		);
	}
}

export default withRouter(SurveyGestion);
