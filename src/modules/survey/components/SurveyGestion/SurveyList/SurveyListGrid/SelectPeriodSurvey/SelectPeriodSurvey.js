import React, { Component, Fragment } from "react";
import { DatePicker, LocaleProvider } from "antd";
import frFR from "antd/lib/locale-provider/fr_FR";
import moment from "moment";
import "moment/locale/fr";
moment.locale("fr");
const { RangePicker } = DatePicker;

class SelectPeriodSurvey extends Component {
	handleRangeDate = (dates, dateStrings) => {
		this.props.RangeDateAction(dates, dateStrings);
	};

	render() {
		return (
			<Fragment>
				<LocaleProvider locale={frFR}>
					<RangePicker
						onChange={(dates, dateStrings) =>
							this.handleRangeDate(dates, dateStrings)
						}
						size={"large"}
						format={"DD-MM-YYYY"}
						style={{
							//backgroundColor: "#ED8B00",
							//borderColor: "#ED8B00",
							//color: "#ED8B00",
						}}
					/>
				</LocaleProvider>
			</Fragment>
		);
	}
}
export default SelectPeriodSurvey;
