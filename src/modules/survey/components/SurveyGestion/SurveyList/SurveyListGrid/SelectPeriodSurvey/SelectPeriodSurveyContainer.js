import { connect } from "react-redux";
import SelectPeriodSurvey from "./SelectPeriodSurvey";

const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(SelectPeriodSurvey);
