import React, { Component, Fragment } from "react";
import LocalGrid from "./LocalGrid";
import { TableFilterRow, Table } from "@devexpress/dx-react-grid-material-ui";
import { Link } from "react-router-dom";
import {
	Divider,
	Icon,
	Tooltip,
	notification,
	Button,
	Badge,
	Menu,
	Dropdown,
	message,
} from "antd";
//import { PaperApp, Title } from "./../../../StyledComponents/StyledComponents";
import DeleteSurvey from "./DeleteSurvey";
import SelectStatus from "./SelectStatus";
import AddSurvey from "./AddSurvey";
import SelectCriteria from "./SelectCriteria";
import SelectPeriodSurvey from "./SelectPeriodSurvey";
import ArchiveSurvey from "./ArchiveSurvey";
import moment from "moment";
import _ from "lodash";
import {
	filter,
	assign,
	zipObject,
	xor,
	forEach,
	partition,
	find,
} from "lodash/fp";
import {
	secteurList,
	modeleList,
	enseigneList,
} from "./../../../../redux/reducers/fakeDate";
import "moment/locale/fr";
import { withRouter } from "react-router-dom";
moment.locale("fr");

const EmptyFormatter = ({ value }) =>
	value ? <span>{value}</span> : <span>-</span>;

const ActionFormatter = ({ row }) => {
	const status = parseInt(row["periodicity"].status);
	return (
		<Fragment>
			{[status].indexOf(2) !== -1 && (
				<Fragment>
					<Link to={`/survey/${row.id}/surveyGestion/preview`}>
						<Tooltip placement="top" title={"Previsualiser"}>
							<Icon type="shake" style={{ fontSize: 14, color: "#000000" }} />
						</Tooltip>
					</Link>
					<Divider type="vertical" />
					<Link to={`/survey/${row.id}/surveyGestion/preview`}>
						<Tooltip placement="top" title={"Dupliquer"}>
							<Icon type="copy" style={{ fontSize: 14, color: "#000000" }} />
						</Tooltip>
					</Link>
					<Divider type="vertical" />
					<Link to={`/survey/${row.id}/surveyGestion/preview`}>
						<Tooltip placement="top" title={"Archiver"}>
							<Icon
								type="database"
								style={{ fontSize: 14, color: "#000000" }}
							/>
						</Tooltip>
					</Link>
				</Fragment>
			)}
			{[status].indexOf(1) !== -1 && (
				<Fragment>
					<Link to={`/survey/${row.id}/surveyGestion`}>
						<Tooltip placement="top" title={"Edit"}>
							<Icon type="edit" style={{ fontSize: 14, color: "#000000" }} />
						</Tooltip>
					</Link>
					<Divider type="vertical" />
					<DeleteSurvey idSurvey={row.id} />
					<Divider type="vertical" />
					<Link to={`/survey/${row.id}/surveyGestion/preview`}>
						<Tooltip placement="top" title={"Previsualiser"}>
							<Icon type="shake" style={{ fontSize: 14, color: "#000000" }} />
						</Tooltip>
					</Link>
					<Divider type="vertical" />
					<Link to={`/survey/${row.id}/surveyGestion/preview`}>
						<Tooltip placement="top" title={"Dupliquer"}>
							<Icon type="copy" style={{ fontSize: 14, color: "#000000" }} />
						</Tooltip>
					</Link>
					<Divider type="vertical" />
					<ArchiveSurvey surveyId={row._id} />
				</Fragment>
			)}
			{[status].indexOf(3) !== -1 && (
				<Fragment>
					<Link to={`/survey/${row.id}/surveyGestion`}>
						<Tooltip placement="top" title={"Edit"}>
							<Icon type="edit" style={{ fontSize: 14, color: "#000000" }} />
						</Tooltip>
					</Link>
					<Divider type="vertical" />
					<DeleteSurvey idSurvey={row.id} />
					<Divider type="vertical" />
					<Link to={`/survey/${row.id}/surveyGestion/preview`}>
						<Tooltip placement="top" title={"Previsualiser"}>
							<Icon type="shake" style={{ fontSize: 14, color: "#000000" }} />
						</Tooltip>
					</Link>
				</Fragment>
			)}
			{[status].indexOf(5) !== -1 && (
				<Fragment>
					<Link to={`/survey/${row.id}/surveyGestion/preview`}>
						<Tooltip placement="top" title={"Previsualiser"}>
							<Icon type="shake" style={{ fontSize: 14, color: "#000000" }} />
						</Tooltip>
					</Link>
				</Fragment>
			)}
			{[status].indexOf(4) !== -1 && (
				<Fragment>
					<Link to={`/survey/${row.id}/surveyGestion/preview`}>
						<Tooltip placement="top" title={"Previsualiser"}>
							<Icon type="shake" style={{ fontSize: 14, color: "#000000" }} />
						</Tooltip>
					</Link>
					<Divider type="vertical" />
					<Link to={`/survey/${row.id}/surveyGestion/preview`}>
						<Tooltip placement="top" title={"Dupliquer"}>
							<Icon type="copy" style={{ fontSize: 14, color: "#000000" }} />
						</Tooltip>
					</Link>
				</Fragment>
			)}
		</Fragment>
	);
};

const StatusFormatter = ({ row }) => {
	const status = parseInt(row["periodicity"].status);
	return (
		<Fragment>
			{[status].indexOf(1) !== -1 && (
				<Fragment>
					<Badge status="success" />
					<span>Actif</span>
				</Fragment>
			)}
			{[status].indexOf(2) !== -1 && (
				<Fragment>
					<Badge status="warning" />
					<span>Inactif</span>
				</Fragment>
			)}
			{[status].indexOf(3) !== -1 && (
				<Fragment>
					<Badge status="processing" />
					<span>Brouillon</span>
				</Fragment>
			)}
			{[status].indexOf(4) !== -1 && (
				<Fragment>
					<Badge status="default" />
					<span>Archivé</span>
				</Fragment>
			)}
		</Fragment>
	);
};

const filterCellTemplate = handleStatusSelect => props => {
	switch (props.column.name) {
		case "actions":
			return <Table.Cell />;
			break;
		case "retailer":
			return <Table.Cell />;
			break;
		case "enseigne":
			return <Table.Cell />;
			break;
		case "sectors":
			return <Table.Cell />;
			break;
		case "secteur":
			return <Table.Cell />;
			break;
		case "survey":
			return <Table.Cell />;
			break;
		case "kpi":
			return <Table.Cell />;
			break;
		case "modele":
			return <Table.Cell />;
			break;
		case "status":
			return <SelectStatus handleStatusSelect={handleStatusSelect} />;
			break;
		default:
			return <TableFilterRow.Cell {...props} />;
	}
};

class SurveyListGrid extends Component {
	state = {
		surveyList: this.props.surveyList,
		statusList: this.props.statusList,
	};

	componentWillReceiveProps(nextProps) {
		this.props.getSurveyEnseignesList(enseigneList);
		//this.props.getSurveySectorsList(nextProps.sectorList);
		this.props.getSurveyModelesList(modeleList);
		this.setState({
			surveyList: nextProps.surveyList,
			statusList: nextProps.statusList,
		});
		//this.props.getSurveyList();
	}

	// handleStatus = param => {
	// };
	RangeDateAction = (dates, dateStrings) => {
		let dateBegin = dateStrings[0];
		let dateEnd = dateStrings[1];
		const surveyList = this.state.surveyList;

		const filteredDates = _.filter(surveyList, list => {
			if (
				new Date(
					moment(
						list.periodicity.startDate,
						"YYYY-MM-DDTHH:mm:ss.SSSSZ"
					).format("YYYY-MM-DD")
				).getTime() <
					new Date(
						moment(dateBegin, "DD-MM-YYYY").format("YYYY-MM-DD")
					).getTime() &&
				new Date(
					moment(list.periodicity.endDate, "YYYY-MM-DDTHH:mm:ss.SSSSZ").format(
						"YYYY-MM-DD"
					)
				).getTime() >
					new Date(moment(dateEnd, "DD-MM-YYYY").format("YYYY-MM-DD")).getTime()
			)
				return list;
		});

		if (dateBegin !== "" && dateEnd !== "") {
			this.setState({ surveyList: filteredDates });
		} else {
			this.setState({ surveyList: this.props.surveyList });
		}
	};

	render() {
		const {
			pageBy,
			sorting,
			hiddenColumns,
			match,
			handleFilterCriteria,
			columns,
			filterBy,
			handleStatusSelect,
		} = this.props;
		const { surveyList, statusList } = this.state;
		const formatters = [
			{
				for: [
					"id",
					"surveyName",
					"enseigne",
					"begin",
					"end",
					"beginCreation",
					"endCreation",
					"kpi",
					"retailer",
					"secteur",
					"modele",
					"enseigne",
					"survey",
				],
				formatterComponent: EmptyFormatter,
			},
			{
				for: ["status"],
				formatterComponent: StatusFormatter,
			},
			{
				for: ["actions"],
				formatterComponent: ActionFormatter,
			},
		];
		return (
			<div>
				<AddSurvey />
				<Divider type="vertical" />
				<SelectCriteria handleFilterCriteria={handleFilterCriteria} />
				<Divider type="vertical" />
				{statusList === 0 && (
					<SelectPeriodSurvey RangeDateAction={this.RangeDateAction} />
				)}
				<LocalGrid
					columns={columns}
					rows={surveyList}
					pageBy={pageBy}
					sorting={sorting}
					// tableCellTemplate={renderTableCellTemplate(
					//   onConfirmDelete(deleteManaged),
					// )}
					allowPaging={true}
					formatters={formatters}
					filterCellTemplate={filterCellTemplate(handleStatusSelect)}
					hiddenColumns={hiddenColumns}
					filterBy={filterBy}
					allowSortingHeader
				/>
			</div>
		);
	}
}

SurveyListGrid.defaultProps = {
	//sorting: [{ columName: "surveyName", direction: "desc" }],
	sorting: [],
	pageBy: { defaultCurrentPage: 0, pageSize: 8 },
	hiddenColumns: [],
	centralesRegionalList: [],
};
export default withRouter(SurveyListGrid);
