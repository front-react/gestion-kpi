import { connect } from "react-redux";
import AddSurvey from "./AddSurvey";

const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(AddSurvey);
