import React, { Component, Fragment } from "react";
import { Button } from "antd";
import { Link } from "react-router-dom";
import styled from "styled-components";

// const ButtonSurvey = styled(Button)`
// 	/* pseudo selectors work as well */
// 	&&:hover {
// 		background: #000000;
// 	}
// `;

class AddSurvey extends Component {
	render() {
		return (
			<Fragment>
				<Link to={`/survey/creation`}>
					<Button
						type="primary"
						icon="plus-square-o"
						size={"large"}
						style={{
							marginTop: 25,
							marginBottom: 10,
							//backgroundColor: "#ED8B00",
							//borderColor: "#ED8B00",
						}}
					>
						Add survey
					</Button>
				</Link>
			</Fragment>
		);
	}
}
export default AddSurvey;
