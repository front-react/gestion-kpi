import React, { Component, Fragment } from "react";
import { Select } from "antd";
const Option = Select.Option;

class SelectCriteria extends Component {
	state = {
		value: "",
	};

	handleChange = e => {
		this.props.handleFilterCriteria(e);
		if (e === 0) {
			this.setState({ value: "" });
		} else {
			this.setState({ value: e });
		}
	};

	render() {
		const { criteriaList } = this.props;

		return (
			<Fragment>
				<Select
					size={"large"}
					style={{ width: "10%" }}
					value={this.state.value}
					onChange={e => (e ? this.handleChange(e) : this.handleChange(0))}
				>
					<Option key={"-1"} value={""}>
						{"Tout..."}
					</Option>
					{criteriaList.map(criteria => (
						<Option key={criteria.criteriaId} value={criteria.criteriaId}>
							{criteria.criteriaName}
						</Option>
					))}
				</Select>
			</Fragment>
		);
	}
}

SelectCriteria.defaultProps = {
	criteriaList: [
		{
			criteriaId: 1,
			criteriaName: "Secteurs",
		},
		{
			criteriaId: 2,
			criteriaName: "Modèles",
		},
		{
			criteriaId: 3,
			criteriaName: "Enseinges",
		},
	],
};
export default SelectCriteria;
