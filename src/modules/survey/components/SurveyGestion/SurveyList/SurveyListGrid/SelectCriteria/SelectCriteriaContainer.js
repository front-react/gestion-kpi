import { connect } from "react-redux";
import SelectCriteria from "./SelectCriteria";

const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(SelectCriteria);
