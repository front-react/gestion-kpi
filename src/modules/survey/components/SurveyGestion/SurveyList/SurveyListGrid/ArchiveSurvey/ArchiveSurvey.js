import React, { Component, Fragment } from "react";
import { Icon, notification, Tooltip, Modal } from "antd";
import _ from "lodash";
import { assign, filter, toString } from "lodash/fp";

class ArchiveSurvey extends Component {
	state = {
		visible: false,
	};
	handleArchivageSurvey = () => {
		this.setState({ visible: !this.state.visible });
	};

	handleConfirmArchivage = statusId => {
		let currentSurvey = this.props.currentSurvey;
		//let status = this.props.status;
		// _.assign(currentSurvey[0].periodicity, {
		// 	status: statusId,
		// });

		//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/Cyclic_object_value
		// const getCircularReplacer = () => {
		// 	const seen = new WeakSet();
		// 	return (key, value) => {
		// 		if (typeof value === "object" && value !== null) {
		// 			if (seen.has(value)) {
		// 				return;
		// 			}
		// 			seen.add(value);
		// 		}
		// 		return value;
		// 	};
		// };

		// let dataSurvey = {
		// 	survey: JSON.stringify(currentSurvey, getCircularReplacer()),
		// };
		// console.log(JSON.stringify(currentSurvey, getCircularReplacer()));
		let idSurvey = currentSurvey[0]._id;
		let idStatusSurvey = statusId;
		let data = {
			idSurvey: currentSurvey[0]._id,
			idStatus: statusId,
		};

		this.props.updateSurvey(data);
		this.handleArchivageSurvey();
	};

	render() {
		const { currentSurvey, status } = this.props;
		return (
			<Fragment>
				<Tooltip placement="top" title={"Archiver"}>
					<Icon
						type="database"
						style={{ fontSize: 14, color: "#000000" }}
						onClick={this.handleArchivageSurvey}
					/>
				</Tooltip>
				<Modal
					title={`Confirmation d'archivage du questionnaire`}
					visible={this.state.visible}
					onOk={() => this.handleConfirmArchivage(4)}
					onCancel={this.handleArchivageSurvey}
				>
					<p>{`Vous êtes sur le point d'archivé ce questionnaire !`}</p>
				</Modal>
			</Fragment>
		);
	}
}

export default ArchiveSurvey;
