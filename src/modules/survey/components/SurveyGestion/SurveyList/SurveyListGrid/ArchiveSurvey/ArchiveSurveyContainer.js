import { connect } from "react-redux";
import ArchiveSurvey from "./ArchiveSurvey";
import { surveyIdSelector } from "../../../../../redux/selectors";
import { updateSurvey } from "./../../../../../redux/actions/dashboard/update";

const mapStateToProps = (state, ownProps) => {
	return {
		currentSurvey: surveyIdSelector(state, ownProps),
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		updateSurvey: (data) => {
			dispatch(updateSurvey(data));
		},
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ArchiveSurvey);
