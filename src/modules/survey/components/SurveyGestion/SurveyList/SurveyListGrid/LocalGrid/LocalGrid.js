import React, { PureComponent } from "react";
import styled from "styled-components";
import {
	DataTypeProvider,
	GroupingState,
	PagingState,
	SortingState,
	EditingState,
	FilteringState,
	RowDetailState,
	IntegratedFiltering,
	IntegratedSorting,
	IntegratedPaging,
	IntegratedGrouping,
	SelectionState,
} from "@devexpress/dx-react-grid";
import {
	Grid,
	Table,
	VirtualTable,
	TableHeaderRow,
	TableEditRow,
	TableGroupRow,
	TableFilterRow,
	TableRowDetail,
	TableEditColumn,
	TableColumnReordering,
	TableColumnVisibility,
	TableColumnResizing,
	ColumnChooser,
	PagingPanel,
	GroupingPanel,
	DragDropProvider,
	Toolbar,
	TableSelection,
	TableBandHeader,
} from "@devexpress/dx-react-grid-material-ui";

//some defaut style
const TableRow = ({ className, ...rest }) => (
	<Table.Row className={className} {...rest} />
);
const TableRowStyled = styled(TableRow)`
	&& {
		height: 50px;
		background-color:white;
	}
	tr {
		background-color: none !important;
	}
	td {
		padding: 0;
	}
	th {
		padding: 0;
		text-align: center;
	}
	button {
		height: 37px;
		width: 37px;
	}
`;

const TableFilterRowBase = ({ className, ...rest }) => (
	<Table.Row className={className} {...rest} />
);
const TableFilterRowStyled = styled(TableFilterRowBase)`
	&& {
		height: 40px;
		input {
			padding: 0;
		}
	}
`;

class LocalGrid extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			rows: props.rows,
			columns: props.columns,
			sorting: props.sortBy,
			filters: props.filterBy,
			defaultOrder: props.columns.map(column => column.name),
		};
	}

	changeSorting = sorting => {
		this.setState({ sorting });
	};

	onSelectionChange = selection => {
		this.props.onSelectionChange(selection);
	};

	render() {
		const {
			columns,
			rows,
			formatters,
			allowDragDrop,
			isVirtualTable,
			virtualTableHeight,
			tableCellTemplate,
			tableColumnExtensions,
			filterCellTemplate,
			allowSortingHeader,
			hiddenColumns,
			allowChangeHiddenColumns,
			headerCellTemplate,
			integratedGroupingColumnExtensions,
			groupBy,
			expandedGroups,
			showGroupPanel,
			groupCellTemplate,
			showGroupingControls,
			tableGroupColumnExtension,
			integratedSortingColumnExtensions,
			allowSortingGroup,
			integratedFilteringColumnExtension,
			allowFiltering,
			allowReordering,
			pageBy,
			defaultColumnWidths,
			allowPaging,
			rowDetail,
			emptyMessageComponentGroupPanel,
			allowSelection,
			selection,
			onSelectionChange,
			tableSelectionProps,
			tableRow,
			tableFilterRow,
			columnBands,
		} = this.props;

		const { sorting, filters } = this.state;

		return (
			<Grid
				rows={rows}
				columns={columns}
				style={{ border: "1px solid rgb(224, 224, 224)", borderRadius: "5px" }}
			>
				{allowDragDrop && <DragDropProvider />}
				{formatters &&
					formatters.map((formatter, index) => {
						return (
							<DataTypeProvider
								key={index}
								for={formatter.for}
								formatterComponent={formatter.formatterComponent}
								//key={formatter.formatterComponent}
							/>
						);
					})}
				<SortingState sorting={sorting} onSortingChange={this.changeSorting} />
				{allowFiltering && <FilteringState defaultFilters={filters} />}
				{allowPaging && (
					<PagingState
						defaultCurrentPage={pageBy.defaultCurrentPage}
						pageSize={pageBy.pageSize}
					/>
				)}
				{rowDetail && (
					<RowDetailState
						defaultExpandedRows={
							rowDetail.defaultExpandedRows ? rowDetail.defaultExpandedRows : []
						}
					/>
				)}
				{groupBy.length > 0 && (
					<GroupingState
						defaultGrouping={groupBy}
						defaultExpandedGroups={expandedGroups}
					/>
				)}
				{allowFiltering && (
					<IntegratedFiltering
						columnExtensions={integratedFilteringColumnExtension}
					/>
				)}
				{allowPaging && <IntegratedPaging />}
				{groupBy.length > 0 && (
					<IntegratedGrouping
						columnExtensions={integratedGroupingColumnExtensions}
					/>
				)}
				<IntegratedSorting
					columnExtensions={integratedSortingColumnExtensions}
				/>

				{allowSelection && (
					<SelectionState
						selection={selection}
						onSelectionChange={this.onSelectionChange}
					/>
				)}

				{isVirtualTable ? (
					<VirtualTable
						columnExtensions={tableColumnExtensions}
						height={virtualTableHeight}
						rowComponent={TableRowStyled}
					/>
				) : (
					<Table
						columnExtensions={tableColumnExtensions}
						rowComponent={tableRow}
					/>
				)}
				{(groupBy.length > 0 || allowChangeHiddenColumns) && <Toolbar />}
				{defaultColumnWidths && (
					<TableColumnResizing defaultColumnWidths={defaultColumnWidths} />
				)}
				{allowReordering && (
					<TableColumnReordering defaultOrder={this.state.defaultOrder} />
				)}
				{headerCellTemplate ? (
					<TableHeaderRow
						showSortingControls={allowSortingHeader}
						cellComponent={headerCellTemplate}
					/>
				) : (
					<TableHeaderRow showSortingControls={allowSortingHeader} />
				)}
				<TableColumnVisibility defaultHiddenColumnNames={hiddenColumns} />
				{allowChangeHiddenColumns && <ColumnChooser />}
				{allowPaging && <PagingPanel />}
				{allowFiltering && filterCellTemplate ? (
					<TableFilterRow
						cellComponent={filterCellTemplate}
						rowComponent={tableFilterRow}
					/>
				) : (
					allowFiltering && <TableFilterRow />
				)}
				{rowDetail && (
					<TableRowDetail contentComponent={rowDetail.contentComponent} />
				)}
				{groupBy.length > 0 &&
					(groupCellTemplate ? (
						<TableGroupRow
							cellComponent={groupCellTemplate}
							columnExtensions={tableGroupColumnExtension}
						/>
					) : (
						<TableGroupRow columnExtensions={tableGroupColumnExtension} />
					))}

				{groupBy.length > 0 &&
					showGroupPanel && (
						<GroupingPanel
							showSortingControls={allowSortingGroup}
							showGroupingControls={showGroupingControls}
							emptyMessageComponent={emptyMessageComponentGroupPanel}
						/>
					)}

				{allowSelection && <TableSelection {...tableSelectionProps} />}
				<TableBandHeader
					columnBands={columnBands}
					rowComponent={TableRowStyled}
				/>
			</Grid>
		);
	}
}

LocalGrid.defaultProps = {
	groupBy: [],
	sortBy: [],
	allowSortingHeader: false,
	allowSortingGroup: false,
	allowFiltering: true,
	filterBy: [],
	allowPaging: true,
	pageBy: { defaultCurrentPage: 0, pageSize: 10 },
	allowedPageSizes: false,
	allowDragDrop: false,
	expandedGroups: [],
	showGroupPanel: true,
	allowReordering: false,
	showGroupingControls: false,
	allowChangeHiddenColumns: false,
	isVirtualTable: true,
	emptyMessageComponentGroupPanel: () => (
		<span>Drag le nom d'une colonne pour grouper les données</span>
	),
	virtualTableHeight: 530,
	allowSelection: false,
	selection: [],
	tableSelectionProps: {},
	tableRow: TableRowStyled,
	tableFilterRow: TableFilterRowStyled,
	columnBands: [],
	columnBands: [
		{
			title: "Periode questionnaire",
			children: [{ columnName: "startDate" }, { columnName: "endDate" }],
		},
		{
			title: "Periode modèle",
			children: [
				{ columnName: "periodicityAssortment.startDate" },
				{ columnName: "periodicityAssortment.endDate" },
			],
		},
	],
};

export default LocalGrid;
