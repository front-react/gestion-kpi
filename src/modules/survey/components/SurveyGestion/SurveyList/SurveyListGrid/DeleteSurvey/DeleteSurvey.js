import React, { Component, Fragment } from "react";
import { Icon, notification, Tooltip, Modal } from "antd";

class DeleteSurvey extends Component {
	state = {
		visible: false,
	};
	handleDeleteSurvey = () => {
		this.setState({ visible: !this.state.visible });
	};

	handleConfirmDelete = idSurvey => {
		alert(idSurvey);
	};

	render() {
		const { idSurvey, currentSurvey } = this.props;
		return (
			<Fragment>
				<Tooltip placement="top" title={"Delete"}>
					<Icon
						type="delete"
						style={{ fontSize: 14, color: "#000000" }}
						onClick={this.handleDeleteSurvey}
					/>
				</Tooltip>
				<Modal
					title={`Confirmation de suppression du questionnaire`}
					visible={this.state.visible}
					onOk={() => this.handleConfirmDelete(idSurvey)}
					onCancel={this.handleDeleteSurvey}
				>
					<p>Button de suppression.</p>
				</Modal>
			</Fragment>
		);
	}
}

export default DeleteSurvey;
