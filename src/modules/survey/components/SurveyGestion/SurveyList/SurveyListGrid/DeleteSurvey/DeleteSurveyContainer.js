import { connect } from "react-redux";
import DeleteSurvey from "./DeleteSurvey";
import { surveySelector } from "./../../../../../redux/selectors";
const mapStateToProps = (state, ownProps) => {
  return {
    currentSurvey: surveySelector(state, ownProps),
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(DeleteSurvey);
