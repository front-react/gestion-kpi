import React, { Component } from "react";
import { Select } from "antd";

import { Table } from "@devexpress/dx-react-grid-material-ui";
//import { Table } from "antd";

const Option = Select.Option;

class SelectStatus extends Component {
	// componentWillReceiveProps(nextProps, stateProps) {
	//   if (nextProps.filter !== null) {
	//     let filterAttr = `statutId`;
	//     let filterLabel = nextProps.filter.statusId;
	//     this.props.getPdvList(filterAttr, filterLabel);
	//   }
	// }

	// componentDidMount() {
	//   this.props.handleStatus("Hello !");
	// }

	render() {
		//const { filter, onFilter, statusList } = this.props;
		const { filter, statusList, handleStatusSelect } = this.props;
		return (
			<Table.Cell>
				<Select
					style={{ width: "100%" }}
					value={filter ? filter.value : ""}
					onChange={e => (e ? handleStatusSelect(e) : handleStatusSelect(0))}
					placeholder="Filter..."
				>
					<Option key={"-1"} value={""}>
						{"Select filter..."}
					</Option>
					{statusList.map(status => (
						<Option key={status.statusId} value={status.statusId}>
							{status.statusName}
						</Option>
					))}
				</Select>
			</Table.Cell>
		);
	}
}

SelectStatus.defaultProps = {
	statusList: [
		{
			statusId: 1,
			statusName: "Actif",
		},
		{
			statusId: 2,
			statusName: "Inactif",
		},
		{
			statusId: 3,
			statusName: "Brouillon",
		},
		{
			statusId: 4,
			statusName: "Archivé",
		},
	],
};
export default SelectStatus;
