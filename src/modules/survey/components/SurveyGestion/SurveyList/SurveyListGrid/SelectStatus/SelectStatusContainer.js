import { connect } from "react-redux";
import SelectStatus from "./SelectStatus";

const mapStateToProps = (state, ownProps) => {
	return {};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SelectStatus);
