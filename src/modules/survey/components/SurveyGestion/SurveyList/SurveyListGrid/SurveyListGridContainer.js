import { connect } from "react-redux";
import SurveyListGrid from "./SurveyListGrid";
import {
	getSurveyEnseignesList,
	getSurveySectorsList,
	getSurveyModelesList,
	getSurveyList
} from "./../../../../redux/actions/dashboard/get";
import { constructSector } from "./../../../../redux/selectors";
const mapStateToProps = (state, ownProps) => {
	return {
		//secteurListTransaction: constructSector(state, ownProps),
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		getSurveyEnseignesList: enseignesList => {
			dispatch(getSurveyEnseignesList(enseignesList));
		},
		getSurveySectorsList: sectorsList => {
			dispatch(getSurveySectorsList(sectorsList));
		},
		getSurveyModelesList: modelesList => {
			dispatch(getSurveyModelesList(modelesList));
		},
		getSurveyList: () => {
			dispatch(getSurveyList());
		}
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SurveyListGrid);
