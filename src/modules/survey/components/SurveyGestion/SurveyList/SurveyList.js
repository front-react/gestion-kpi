import React, { Component, Fragment } from "react";
import SurveyListGrid from "./SurveyListGrid";
import {
	Divider,
	Icon,
	Tooltip,
	notification,
	Button,
	Badge,
	Menu,
	Dropdown,
	message,
} from "antd";
import moment from "moment";
import {
	SURVEYS_LIST,
	SECTEURS_LIST,
	MODELES_LIST,
	ENSEIGNES_LIST,
} from "./../../../../../components/Constants";

import _ from "lodash";
import { forEach } from "lodash/fp";

const DropdownSector = ({ row, columnName }) => {
	let rowList = row;
	if (rowList[columnName].length === undefined) {
		return (
			<Dropdown
				overlay={
					<Menu>
						<Menu.Item>{rowList[columnName].retailerName}</Menu.Item>
					</Menu>
				}
			>
				<a className="ant-dropdown-link" href="#">
					{`1 ${columnName}(s)`} <Icon type="eye" />
				</a>
			</Dropdown>
		);
	}
	if (rowList[columnName].length > 0) {
		return (
			<Dropdown
				overlay={
					<Menu>
						{rowList[columnName].map((element, index) => {
							return <Menu.Item key={index}>{element.label}</Menu.Item>;
						})}
					</Menu>
				}
			>
				<a className="ant-dropdown-link" href="#">
					{`${rowList[columnName].length} ${columnName}(s)`} <Icon type="eye" />
				</a>
			</Dropdown>
		);
	}
	if (rowList[columnName].length === 0) {
		return (
			<Dropdown
				overlay={
					<Menu>
						{rowList[columnName].map((element, index) => {
							return <Menu.Item key={index}>{element.label}</Menu.Item>;
						})}
					</Menu>
				}
			>
				<a className="ant-dropdown-link" href="#">
					{`${rowList[columnName].length} ${columnName}(s)`} <Icon type="eye" />
				</a>
			</Dropdown>
		);
	}
};
const DropdownMenu = ({ row, columnName }) => {
	let rowList = row;
	let menu = [{ name: "menu 1" }, { name: "menu 2" }, { name: "menu 3" }];
	return (
		<Dropdown
			overlay={
				<Menu>
					{menu.map((element, index) => {
						return <Menu.Item key={index}>{element.name}</Menu.Item>;
					})}
				</Menu>
			}
		>
			<a>test</a>
		</Dropdown>
	);
};

const DropdownKpi = ({ row, columnName }) => {
	let rowList = row;
	let menu = [{ name: "menu 1" }, { name: "menu 2" }, { name: "menu 3" }];
	let locationType = rowList["metier"].locationTypes;
	let statementList = [];
	let kpiList = [];
	_.forEach(locationType, (location, index) => {
		_.forEach(location.statements, (statement, index) => {
			statementList.push(statement);
		});
	});
	_.forEach(statementList, (statement, index) => {
		_.forEach(statement.kpis, (kpi, index) => {
			kpiList.push(kpi);
		});
	});
	return (
		<Dropdown
			overlay={
				<Menu>
					{kpiList.map((element, index) => {
						return <Menu.Item key={index}>{element.label}</Menu.Item>;
					})}
				</Menu>
			}
		>
			<a className="ant-dropdown-link" href="#">
				{`${kpiList.length} kpi(s)`} <Icon type="eye" />
			</a>
		</Dropdown>
	);
};

const DropdownElementMetier = ({ row, columnName }) => {
	let rowList = row;
	return (
		<Dropdown
			overlay={
				<Menu>
					{rowList[columnName].map((element, index) => {
						return <Menu.Item key={index}>{element.label}</Menu.Item>;
					})}
				</Menu>
			}
		>
			<a className="ant-dropdown-link" href="#">
				{`${rowList[columnName].length} ${columnName}(s)`} <Icon type="eye" />
			</a>
		</Dropdown>
	);
};

const SpanGrid = ({ row, columnName }) => {
	let spanRow = row;

	return <div> {`${moment(spanRow[columnName]).format("DD-MM-YYYY")}`}</div>;
};
const NameGrid = ({ row, columnName }) => {
	let nameRow = row;
	return `${nameRow["periodicity"].label}`;
};

const StatusGrid = ({ row, columnName }) => {
	let statusRow = row;
	return `${statusRow.status}`;
};

const ModelGrid = ({ row, columnName }) => {
	let modelRow = row;
	return `${modelRow[columnName].retailerName}`;
};

const columnsSurvey = [
	{
		name: "surveyName",
		title: "Nom Questionnaire",
		getCellValue: (row, columnName) => (
			<NameGrid row={row} columnName={columnName} />
		),
	},
	{
		name: "status",
		title: "Status questionnaire",
		getCellValue: (row, columnName) => (
			<StatusGrid row={row} columnName={columnName} />
		),
	},
	{
		name: "startDate",
		title: "Début",
		getCellValue: (row, columnName) => (
			<span>{`${moment(
				row.periodicity[columnName],
				"YYYY-MM-DDTHH:mm:ss.SSSSZ"
			).format("DD-MM-YYYY")}`}</span>
		),
	},
	{
		name: "endDate",
		title: "Fin",
		getCellValue: (row, columnName) => (
			<span>{`${moment(
				row.periodicity[columnName],
				"YYYY-MM-DDTHH:mm:ss.SSSSZ"
			).format("DD-MM-YYYY")}`}</span>
			// <SpanGrid row={row} columnName={columnName} />
		),
	},
	{
		name: "periodicityAssortment.startDate",
		title: "Début",
		getCellValue: (row, columnName) => (
			<span>{`${moment(
				row.periodicityAssortment["startDate"],
				"YYYY-MM-DDTHH:mm:ss.SSSSZ"
			).format("DD-MM-YYYY")}`}</span>
		),
	},
	{
		name: "periodicityAssortment.endDate",
		title: "Fin",
		getCellValue: (row, columnName) => (
			<span>{`${moment(
				row.periodicityAssortment["endDate"],
				"YYYY-MM-DDTHH:mm:ss.SSSSZ"
			).format("DD-MM-YYYY")}`}</span>
		),
	},
	{
		name: "retailer",
		title: "Modèle(s)",
		getCellValue: (row, columnName) => (
			<ModelGrid row={row} columnName={columnName} />
		),
	},
	{
		name: "kpi",
		title: "Kpi(s)",
		getCellValue: (row, columnName) => (
			<DropdownKpi row={row} columnName={columnName} />
		),
	},

	{
		name: "sectors",
		title: "Secteur(s)",
		getCellValue: (row, columnName) => (
			<DropdownSector row={row} columnName={columnName} />
		),
	},
	{ name: "actions", title: "Actions", width: 350 },
];

const columnsModel = [
	{ name: "modeleName", title: "Nom du modele" },
	{ name: "beginCreation", title: "Début" },
	{ name: "endCreation", title: "Fin" },
	{
		name: "enseigne",
		title: "Nb(s) enseignes",
		getCellValue: (row, columnName) => (
			<DropdownElementMetier row={row} columnName={columnName} />
		),
	},
	{
		name: "secteur",
		title: "Nb(s) sectueurs",
		getCellValue: (row, columnName) => (
			<DropdownElementMetier row={row} columnName={columnName} />
		),
	},
	{
		name: "survey",
		title: "Nb(s) questionnaire",
		getCellValue: (row, columnName) => (
			<DropdownElementMetier row={row} columnName={columnName} />
		),
	},
	{
		name: "kpi",
		title: "Nb(s) kpi",
		getCellValue: (row, columnName) => (
			<DropdownElementMetier row={row} columnName={columnName} />
		),
	},
];

const columnsEnseigne = [
	{ name: "enseigneName", title: "Nom de l'enseigne" },
	{
		name: "secteur",
		title: "Nb(s) secteur",
		getCellValue: (row, columnName) => (
			<DropdownElementMetier row={row} columnName={columnName} />
		),
	},
	{
		name: "survey",
		title: "Nb(s) questionnaire",
		getCellValue: (row, columnName) => (
			<DropdownElementMetier row={row} columnName={columnName} />
		),
	},
	{
		name: "modele",
		title: "Nb(s) Modele",
		getCellValue: (row, columnName) => (
			<DropdownElementMetier row={row} columnName={columnName} />
		),
	},
	{
		name: "kpi",
		title: "Nb(s) Kpi",
		getCellValue: (row, columnName) => (
			<DropdownElementMetier row={row} columnName={columnName} />
		),
	},
];

const columnsSecteur = [
	{ name: "sectorName", title: "Nom du secteur" },
	{
		name: "label",
		title: "Nom du questionnaire",
		getCellValue: (row, columnName) => (
			<span>{`${row["periodicity"].label}`}</span>
		),
	},
	{
		name: "status",
		title: "Status",
		getCellValue: (row, columnName) => (
			<span>{`${row["periodicity"].satus}`}</span>
		),
	},
	{
		name: "startDate",
		title: "Début",
		getCellValue: (row, columnName) => (
			<span>{`${moment(
				row.periodicity[columnName],
				"YYYY-MM-DDTHH:mm:ss.SSSSZ"
			).format("DD-MM-YYYY")}`}</span>
		),
	},
	{
		name: "endDate",
		title: "Fin",
		getCellValue: (row, columnName) => (
			<span>{`${moment(
				row.periodicity[columnName],
				"YYYY-MM-DDTHH:mm:ss.SSSSZ"
			).format("DD-MM-YYYY")}`}</span>
		),
	},
	{
		name: "periodicityAssortment.startDate",
		title: "Début",
		getCellValue: (row, columnName) => (
			<span>{`${moment(
				row.periodicityAssortment["startDate"],
				"YYYY-MM-DDTHH:mm:ss.SSSSZ"
			).format("DD-MM-YYYY")}`}</span>
		),
	},
	{
		name: "periodicityAssortment.endDate",
		title: "Fin",
		getCellValue: (row, columnName) => (
			<span>{`${moment(
				row.periodicityAssortment["endDate"],
				"YYYY-MM-DDTHH:mm:ss.SSSSZ"
			).format("DD-MM-YYYY")}`}</span>
		),
	},
	{
		name: "modele",
		title: "Modèle(s)",
		getCellValue: (row, columnName) => (
			<DropdownSector row={row} columnName={columnName} />
		),
	},
	{
		name: "kpi",
		title: "Kpi(s)",
		getCellValue: (row, columnName) => (
			<DropdownSector row={row} columnName={columnName} />
		),
	},
	{
		name: "enseigne",
		title: "Enseigne(s)",
		getCellValue: (row, columnName) => (
			<DropdownSector row={row} columnName={columnName} />
		),
	},
];

class SurveyList extends Component {
	state = {
		statusList: 0,
		filterBy: [{ columnName: "status", value: "" }],
	};
	handleFilterCriteria = e => {
		let statusList = parseInt(e);
		this.setState({ statusList });
	};

	componentDidMount() {
		this.props.getSurveyList();
		this.props.getmodelId(321);
	}

	render() {
		const {
			surveyList,
			enseigneList,
			modeleList,
			sectorList,
			match,
			handleStatusSelect,
			surveyTransactionList,
		} = this.props;
		return (
			<div>
				<SurveyListGrid
					surveyList={
						this.state.statusList === SURVEYS_LIST
							? surveyList
							: this.state.statusList === SECTEURS_LIST
								? sectorList
								: this.state.statusList === MODELES_LIST
									? modeleList
									: this.state.statusList === ENSEIGNES_LIST
										? enseigneList
										: surveyList
					}
					match={match}
					handleStatusSelect={handleStatusSelect}
					handleFilterCriteria={this.handleFilterCriteria}
					filterBy={this.state.filterBy}
					statusList={this.state.statusList}
					columns={
						this.state.statusList === SURVEYS_LIST
							? columnsSurvey
							: this.state.statusList === SECTEURS_LIST
								? columnsSecteur
								: this.state.statusList === MODELES_LIST
									? columnsModel
									: this.state.statusList === ENSEIGNES_LIST
										? columnsEnseigne
										: columnsSurvey
					}
				/>
			</div>
		);
	}
}

export default SurveyList;
