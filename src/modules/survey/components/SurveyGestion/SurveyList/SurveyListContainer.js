import { connect } from "react-redux";
import SurveyList from "./SurveyList";
import { getSurveyList } from "./../../../redux/actions/dashboard/get";
import { getmodelId } from "./../../../redux/actions/progress/get";
import { surveySelector } from "./../../../redux/selectors";

const mapStateToProps = (state, ownProps) => {
	return {
		surveyList: surveySelector(state, ownProps),
		sectorList: state.survey.surveyManager.secteurList,
		enseigneList: state.survey.surveyManager.surveyEnseigneList,
		modeleList: state.survey.surveyManager.surveyModelesList,
		secteurList: state.survey.surveyManager.surveySectorsList,
		match: ownProps.match,
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		getSurveyList: () => {
			dispatch(getSurveyList());
		},
		getmodelId: idClient => {
			dispatch(getmodelId(idClient));
		},
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SurveyList);
