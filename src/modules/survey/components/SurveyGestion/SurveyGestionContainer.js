import { connect } from "react-redux";
import SurveyGestion from "./SurveyGestion";
import { surveySelector } from "./../../redux/selectors";

const mapStateToProps = (state, ownProps) => {
	return {
		//currentSurvey: surveySelector(state, ownProps),
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SurveyGestion);
