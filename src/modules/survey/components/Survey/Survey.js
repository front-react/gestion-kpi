import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
//import { IndexRedirect, IndexRoute } from "react-router";
import { Layout, Menu, Breadcrumb, Icon } from "antd";
//import SurveyList from "./../SurveyList";
import SurveyGestion from "./../SurveyGestion";
import SurveyCreationSteps from "./../SurveyCreationSteps";

const { Header, Content, Footer, Sider } = Layout;
const SubMenu = Menu.SubMenu;

class Survey extends Component {
	state = {
		collapsed: true,
	};

	toggle = () => {
		this.setState({
			collapsed: !this.state.collapsed,
		});
	};

	render() {
		const { match } = this.props;
		const { collapsed } = this.state;
		const { url } = match;

		return (
			<div>
				<Layout style={{ minHeight: "100vh" }}>
					{/* <Sider
						trigger={
							<Icon
								className="trigger"
								type={collapsed ? "double-right" : "double-left"}
								onClick={this.toggle}
								style={{ height: 36 }}
							/>
						}
						collapsible
						collapsed={collapsed}
					>
						<Menu theme="dark" defaultSelectedKeys={["1"]} mode="vertical">
							<Menu.Item key="1" style={{ margin: 0 }}>
								<Icon type="pie-chart" />
								<span>Option 1</span>
							</Menu.Item>
							<Menu.Item key="2" style={{ margin: 0 }}>
								<Icon type="desktop" />
								<span>Option 2</span>
							</Menu.Item>
							<Menu.Item key="9" style={{ margin: 0 }}>
								<Icon type="file" />
								<span>File</span>
							</Menu.Item>
							<SubMenu
								key="sub1"
								title={
									<span>
										<Icon type="mail" />
										<span>Navigation One</span>
									</span>
								}
								style={{ margin: 0 }}
							>
								<Menu.Item key="1">Option 1</Menu.Item>
								<Menu.Item key="2">Option 2</Menu.Item>
								<Menu.Item key="3">Option 3</Menu.Item>
								<Menu.Item key="4">Option 4</Menu.Item>
							</SubMenu>
						</Menu>
					</Sider> */}

					<Layout>
						<Content style={{ margin: "0 16px" }}>
							<Breadcrumb style={{ margin: "16px 0" }}>
								<Breadcrumb.Item>GSK</Breadcrumb.Item>
								<Breadcrumb.Item>Gestion des questionnaires</Breadcrumb.Item>
							</Breadcrumb>
							<Switch>
								<Route
									name="SurveyGestion"
									exact
									path={`${url}/`}
									component={props => <SurveyGestion {...props} />}
								/>
								<Route
									name="SurveyCreationSteps"
									exact
									path={`${url}/creation`}
									component={props => <SurveyCreationSteps {...props} />}
								/>
							</Switch>
						</Content>
					</Layout>
				</Layout>
			</div>
		);
	}
}

export default Survey;
