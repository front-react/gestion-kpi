import React, { Component, Fragment } from "react";
import { Breadcrumb, LocaleProvider, DatePicker } from "antd";
import frFR from "antd/lib/locale-provider/fr_FR";
import moment from "moment";
import "moment/locale/fr";
import { Select } from "antd";
import {
	SubTitle,
	ContentCard,
	StepCard,
	InputSubmit,
	InputFile,
	InputFileLabel,
} from "../../../survey/StyledComponents/StyledComponents";
const Option = Select.Option;
const { RangePicker } = DatePicker;

class ImportClientAssortiment extends Component {
	state = {
		file_xlsx: "",
		dateStart: "",
		dateEnd: "",
		client: "",
		retailer: "",
	};
	componentDidMount() {
		this.props.getAllClients();
		this.props.getAllRetailers();
	}

	onFormSubmit = e => {
		e.preventDefault();
		let data = new FormData();

		let file_xlsx = this.state.file_xlsx;
		let client = this.state.client;
		let retailer = this.state.retailer;
		let startDate = this.state.dateStart;
		let endDate = this.state.dateEnd;

		data.append("file_xlsx", file_xlsx);
		data.append("client", client);
		data.append("retailer", retailer);
		data.append("startDate", startDate);
		data.append("endDate", endDate);
		console.log(data);

		this.props.addmportAssortiment(data);
	};
	onChange = e => {
		let files = e.target.files || e.dataTransfer.files;
		if (!files.length) return;
		this.setState({ file_xlsx: files[0] });
	};

	handleRangeDate = (dates, dateStrings) => {
		let dateBegin = moment(
			`${dateStrings[0]} 12:00:00`,
			"DD-MM-YYYY HH:mm:ss"
		).format("YYYY-MM-DDTHH:mm:ss.SSSSZ");
		let dateEnd = moment(
			`${dateStrings[1]} 12:00:00`,
			"DD-MM-YYYY HH:mm:ss"
		).format("YYYY-MM-DDTHH:mm:ss.SSSSZ");
		this.setState({ dateStart: dateBegin, dateEnd: dateEnd });
	};

	handleChangeClient = e => {
		if (e === 0) {
			this.setState({ client: "" });
		} else {
			this.setState({ client: e });
		}
	};
	handleChangeRetailer = e => {
		if (e === 0) {
			this.setState({ retailer: "" });
		} else {
			this.setState({ retailer: e });
		}
	};
	render() {
		const { clientsList, retailersList } = this.props;
		return (
			<Fragment>
				<Breadcrumb style={{ margin: "16px 0" }}>
					<Breadcrumb.Item>GSK</Breadcrumb.Item>
					<Breadcrumb.Item>Import fichier assortiment</Breadcrumb.Item>
				</Breadcrumb>
				<StepCard maxWidth="900px">
					<form
						onSubmit={this.onFormSubmit}
						style={{ display: "flex", flexDirection: "column" }}
					>
						<SubTitle
							marginTop={"20px"}
							marginLeft={"0px"}
							marginBottom={"0px"}
						>
							Choix du client
						</SubTitle>
						<ContentCard>
							<Select
								size={"large"}
								value={this.state.client}
								onChange={e =>
									e ? this.handleChangeClient(e) : this.handleChangeClient(0)
								}
							>
								<Option key={"-1"} value={""}>
									{"Choix du client"}
								</Option>
								{clientsList.map(client => (
									<Option key={client.companyName} value={client.companyName}>
										{client.companyName}
									</Option>
								))}
							</Select>
						</ContentCard>

						<SubTitle
							marginTop={"20px"}
							marginLeft={"0px"}
							marginBottom={"0px"}
						>
							Choix de l'enseigne
						</SubTitle>
						<ContentCard>
							<Select
								size={"large"}
								value={this.state.retailer}
								showSearch
								onChange={e =>
									e
										? this.handleChangeRetailer(e)
										: this.handleChangeRetailer(0)
								}
								filterOption={(input, option) =>
									option.props.children
										.toLowerCase()
										.indexOf(input.toLowerCase()) >= 0
								}
								style={{ minWidth: "150px" }}
							>
								<Option key={"-1"} value={""}>
									{"Choix de l'enseigne"}
								</Option>
								{retailersList.map(retailer => (
									<Option
										key={retailer.retailerName}
										value={retailer.retailerName}
									>
										{retailer.retailerName}
									</Option>
								))}
							</Select>
						</ContentCard>

						<SubTitle
							marginTop={"20px"}
							marginLeft={"0px"}
							marginBottom={"0px"}
						>
							Période de l'assortiment
						</SubTitle>
						<ContentCard>
							<LocaleProvider locale={frFR}>
								<RangePicker
									onChange={(dates, dateStrings) =>
										this.handleRangeDate(dates, dateStrings)
									}
									size={"large"}
									format={"DD-MM-YYYY"}
								/>
							</LocaleProvider>
						</ContentCard>
						<SubTitle
							marginTop={"20px"}
							marginLeft={"0px"}
							marginBottom={"5px"}
						>
							Importation de l'assortiment
						</SubTitle>
						<ContentCard>
							<InputFileLabel for="file_xlsx">
								{this.state.file_xlsx
									? this.state.file_xlsx.name
									: "Choisir un fichier"}
								<InputFile
									type="file"
									name="file_xlsx"
									//accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
									onChange={this.onChange}
								/>
							</InputFileLabel>
						</ContentCard>
						<InputSubmit type="submit" value="Importer" />
					</form>
				</StepCard>
			</Fragment>
		);
	}
}

export default ImportClientAssortiment;
