import { connect } from "react-redux";
import ImportClientAssortiment from "./ImportClientAssortiment";
import { addmportAssortiment } from "./../../redux/actions/assortiment/post";
import { getAllClients } from "./../../redux/actions/client/get";
import { getAllRetailers } from "./../../redux/actions/retailers/get";
//import { surveySelector } from "./../../redux/selectors";

const mapStateToProps = (state, ownProps) => {
	return {
		//currentSurvey: surveySelector(state, ownProps),
		clientsList: state.client.surveyManager.clientsList,
		retailersList: state.client.surveyManager.retailersList,
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		addmportAssortiment: body => {
			dispatch(addmportAssortiment(body));
		},
		getAllClients: () => {
			dispatch(getAllClients());
		},
		getAllRetailers: () => {
			dispatch(getAllRetailers());
		},
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ImportClientAssortiment);
