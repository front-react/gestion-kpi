import React, { Component, Fragment } from "react";
import { Breadcrumb } from "antd";
import "moment/locale/fr";
import {
	SubTitle,
	ContentCard,
	StepCard,
	InputSubmit,
	InputFile,
	InputFileLabel,
} from "../../../survey/StyledComponents/StyledComponents";

class ImportManagerKpi extends Component {
	state = {
		file_kpis: "",
	};

	onFormSubmit = e => {
		e.preventDefault();
		let data = new FormData();

		let file_kpis = this.state.file_kpis;
		data.append("file_kpis", file_kpis);
		this.props.addmportKpi(data);
	};
	onChange = e => {
		let files = e.target.files || e.dataTransfer.files;
		if (!files.length) return;
		this.setState({ file_kpis: files[0] });
	};

	render() {
		return (
			<Fragment>
				<Breadcrumb style={{ margin: "16px 0" }}>
					<Breadcrumb.Item>GSK</Breadcrumb.Item>
					<Breadcrumb.Item>Import fichier KPI</Breadcrumb.Item>
				</Breadcrumb>
				<StepCard width="350px">
					<form
						onSubmit={this.onFormSubmit}
						style={{
							display: "flex",
							flexDirection: "column",
							height: "250px",
						}}
					>
						<SubTitle
							marginTop={"20px"}
							marginLeft={"0px"}
							marginBottom={"10px"}
						>
							Importation des kpi
						</SubTitle>
						<ContentCard>
							<InputFileLabel for="file_kpis">
								{this.state.file_kpis
									? this.state.file_kpis.name
									: "Choisir un fichier"}
								<InputFile
									type="file"
									name="file_kpis"
									//accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
									onChange={this.onChange}
								/>
							</InputFileLabel>
						</ContentCard>
						<InputSubmit type="submit" value="Importer" />
					</form>
				</StepCard>
			</Fragment>
		);
	}
}

export default ImportManagerKpi;
