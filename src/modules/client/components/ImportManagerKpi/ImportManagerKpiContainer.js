import { connect } from "react-redux";
import ImportManagerKpi from "./ImportManagerKpi";
import {addmportKpi}from "./../../redux/actions/kpi/post"
//import { surveySelector } from "./../../redux/selectors";

const mapStateToProps = (state, ownProps) => {
	return {
		//currentSurvey: surveySelector(state, ownProps),
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		addmportKpi: body => {
			dispatch(addmportKpi(body));
		}
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ImportManagerKpi);
