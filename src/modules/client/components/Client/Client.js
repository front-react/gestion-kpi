import React, { Component } from "react";
import { Route, Switch, Link } from "react-router-dom";
//import { IndexRedirect, IndexRoute } from "react-router";
import { Layout, Menu, Breadcrumb, Icon } from "antd";
//import SurveyList from "./../SurveyList";
import ImportClientAssortiment from "./../ImportClientAssortiment";
import ImportManagerKpi from "./../ImportManagerKpi";

const { Header, Content, Footer, Sider } = Layout;
const SubMenu = Menu.SubMenu;

class Client extends Component {
	state = {
		collapsed: true,
	};

	toggle = () => {
		this.setState({
			collapsed: !this.state.collapsed,
		});
	};

	render() {
		const { match } = this.props;
		const { collapsed } = this.state;
		const { url } = match;

		return (
			<div>
				<Layout style={{ minHeight: "100vh" }}>
					<Sider
						trigger={
							<Icon
								className="trigger"
								type={collapsed ? "double-right" : "double-left"}
								onClick={this.toggle}
								style={{ height: 36 }}
							/>
						}
						collapsible
						collapsed={collapsed}
					>
            <Menu 
            theme="dark" 
            //defaultSelectedKeys={["1"]} 
            mode="vertical">
							<Menu.Item key="1" style={{ margin: 0 }}>
								<Link to={`${url}/import-client-assortiment`}>
									<Icon type="usergroup-add" />
									<span>Client assortiment</span>
								</Link>
							</Menu.Item>
							<Menu.Item key="2" style={{ margin: 0 }}>
								<Link to={`${url}/import-manager-kpi`}>
									<Icon type="setting" />
									<span>Manager Kpi</span>
								</Link>
							</Menu.Item>
						</Menu>
					</Sider>

					<Layout>
						<Content style={{ margin: "0 16px" }}>
							<Switch>
								<Route
									name="ImportClientAssortiment"
									exact
									path={`${url}/import-client-assortiment`}
									component={props => <ImportClientAssortiment {...props} />}
								/>
								<Route
									name="ImportManagerKpi"
									exact
									path={`${url}/import-manager-kpi`}
									component={props => <ImportManagerKpi {...props} />}
								/>
							</Switch>
						</Content>
					</Layout>
				</Layout>
			</div>
		);
	}
}

export default Client;
