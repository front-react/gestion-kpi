import {
  ADD_IMPORT_CLIENT,
  GET_ALL_CLIENT,
  GET_ALL_RETAILER,
  ADD_IMPORT_KPI
} from "../types";

import {
  createReducer,
  createApiHandlers,
  //createAddItemsInArrayWithFilter,
  //createAddItemInArray,
  //createDeleteItemInArray,
} from "./../../../../redux/reducers/functions";

const initialState = {
  reponseImportClient: [],
  reponseImportKpi: [],
  clientsList: [],
  retailersList:[]
};

const addImportClientsHandlers = createApiHandlers(
  ADD_IMPORT_CLIENT,
  (state, action) => {
    return { ...state, reponseImportClient: action.payload.data };
  },
);

const addImportKpisHandlers = createApiHandlers(
  ADD_IMPORT_KPI,
  (state, action) => {
    return { ...state, reponseImportKpi: action.payload.data };
  },
);
const getAllClientsHandlers = createApiHandlers(
  GET_ALL_CLIENT,
  (state, action) => {
    return { ...state, clientsList: action.payload.data };
  },
);

const getAllRetailersHandlers = createApiHandlers(
  GET_ALL_RETAILER,
  (state, action) => {
    return { ...state, retailersList: action.payload.data };
  },
);

const clientReducers = createReducer(initialState, {
  ...addImportClientsHandlers,
  ...getAllClientsHandlers,
  ...getAllRetailersHandlers,
  ...addImportKpisHandlers
});
export default clientReducers;
