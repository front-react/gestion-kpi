import { combineReducers } from "redux";
import clientReducers from "./clientReducers";

const clientAssortimentReducers = combineReducers({
  surveyManager: clientReducers,
});

export default clientAssortimentReducers;
