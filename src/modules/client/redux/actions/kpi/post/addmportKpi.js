import { ADD_IMPORT_KPI } from "../../../types";

import { kpiApiUpsellProvider } from "../../../../../../lib/api/providers";

const addmportKpi = body => {
	return {
		prefixType: ADD_IMPORT_KPI,
		callAPI: () => kpiApiUpsellProvider.post(`importKpis`, body),
	};
};

export default addmportKpi;
