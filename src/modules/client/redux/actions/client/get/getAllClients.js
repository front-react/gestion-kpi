import { GET_ALL_CLIENT } from "../../../types";

import { clientsApiProvider } from "../../../../../../lib/api/providers";

const getAllClients = () => {
	return {
		prefixType: GET_ALL_CLIENT,
		callAPI: () => clientsApiProvider.get(`findAll`),
	};
};

export default getAllClients;
