import { ADD_IMPORT_CLIENT } from "../../../types";

import { assortmentApiUpsellProvider } from "./../../../../../../lib/api/providers";

const addmportAssortiment = body => {
	return {
		prefixType: ADD_IMPORT_CLIENT,
		callAPI: () => assortmentApiUpsellProvider.post(`importAssortment`, body),
	};
};

export default addmportAssortiment;
