import { GET_ALL_RETAILER } from "../../../types";

import { retailersApiUpsellProvider } from "../../../../../../lib/api/providers";

const getAllRetailers = () => {
	return {
		prefixType: GET_ALL_RETAILER,
		callAPI: () => retailersApiUpsellProvider.get(`findAll`),
	};
};

export default getAllRetailers;
