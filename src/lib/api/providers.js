import axios from "axios";

const clientsApiProvider = axios.create({
	baseURL: process.env.REACT_APP_API_CLIENT_ENDPOINT,
});

const kpiApiUpsellProvider = axios.create({
	baseURL: process.env.REACT_APP_API_KPI_ENDPOINT,
});

const retailersApiUpsellProvider = axios.create({
	baseURL: process.env.REACT_APP_API_RETAILER_ENDPOINT,
});

const surveyApiUpsellProvider = axios.create({
	baseURL: process.env.REACT_APP_API_SURVEY_ENDPOINT,
});

const assortmentApiUpsellProvider = axios.create({
	baseURL: process.env.REACT_APP_API_ASSORTMENT_ENDPOINT,
});
const locationApiUpsellProvider = axios.create({
	baseURL: process.env.REACT_APP_API_LOCATION_ENDPOINT,
});
const sectorApiUpsellProvider = axios.create({
	baseURL: process.env.REACT_APP_API_SECTOR_ENDPOINT,
});

// assortmentApiUpsellProvider.defaults.headers.post["Content-Type"] =
// 	"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
export {
	clientsApiProvider,
	kpiApiUpsellProvider,
	surveyApiUpsellProvider,
	assortmentApiUpsellProvider,
	retailersApiUpsellProvider,
	locationApiUpsellProvider,
	sectorApiUpsellProvider,
};
