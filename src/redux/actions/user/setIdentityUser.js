import { SET_IDENTITY_USER } from "./../../types";
import { ErrorStatusLogin } from "./../user";

const getUserRequest = () => ({ type: `${SET_IDENTITY_USER}_REQUEST` });
const getUserSuccess = user => ({
  type: `${SET_IDENTITY_USER}_SUCCESS`,
  payload: { data: user },
});
const getUserFailure = () => ({ type: `${SET_IDENTITY_USER}_FAILURE` });

const setIdentityUser = user => {
  return dispatch => {
    dispatch(getUserRequest());
    dispatch(getUserSuccess(user));
  };
};

export default setIdentityUser;
