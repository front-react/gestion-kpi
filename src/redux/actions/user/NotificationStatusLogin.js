import { NOTIFICATION_STATUS_LOGIN } from "./../../types";

const NotificationStatusLoginFailure = notif => ({
  type: NOTIFICATION_STATUS_LOGIN,
  payload: { data: notif },
});

const NotificationStatusLogin = notif => {
  return dispatch => {
    dispatch(NotificationStatusLoginFailure(notif));
  };
};

export default NotificationStatusLogin;
