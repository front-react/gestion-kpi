import { ERROR_STATUS_LOGIN } from "./../../types";

const ErrorStatusLoginFailure = err => ({
  type: ERROR_STATUS_LOGIN,
  payload: { data: err },
});

const ErrorStatusLogin = err => {
  return dispatch => {
    dispatch(ErrorStatusLoginFailure(err));
  };
};

export default ErrorStatusLogin;
