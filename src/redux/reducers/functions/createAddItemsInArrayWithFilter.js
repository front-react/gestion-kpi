const createAddItemsInArrayWithFilter = (stateValue, fieldToFilter) => {
  return (state, action) => {
    const arrayFiltered = state[stateValue].filter(
      item => item[fieldToFilter] !== action.payload[fieldToFilter],
    );
    return {
      ...state,
      [stateValue]: [...arrayFiltered, ...action.payload.data],
    };
  };
};

export default createAddItemsInArrayWithFilter;
