import {
  SET_IDENTITY_USER,
  ERROR_STATUS_LOGIN,
  NOTIFICATION_STATUS_LOGIN,
} from "../types";

import { createReducer, createApiHandlers } from "./functions";

const initialState = {
  personId: "",
  username: "",
  name: "",
  userCognito: {},
  telephone: "",
  email: "",
  errorUser: null,
  notifUser: null,
};

const setIdentityUserHandlers = createApiHandlers(
  SET_IDENTITY_USER,
  (state, action) => {
    return {
      ...state,
      personId:
        action.payload.data.signInUserSession.idToken.payload[
          "custom:personId"
        ],
      username:
        action.payload.data.signInUserSession.idToken.payload[
          "cognito:username"
        ],
      name: action.payload.data.signInUserSession.idToken.payload["name"],
      userCognito: action.payload.data,
      email: action.payload.data.signInUserSession.idToken.payload.email,
    };
  },
  (state, action) => {
    return { ...state, erreur: true };
  },
);

const satatusErrorForUserHandlers = {
  [ERROR_STATUS_LOGIN](state, action) {
    // on supprimer le storage store.remove('key')
    return { ...state, errorUser: action.payload.data };
  },
};

const satatusNotifForUserHandlers = {
  [NOTIFICATION_STATUS_LOGIN](state, action) {
    // on supprimer le storage store.remove('key')
    return { ...state, notifUser: action.payload.data };
  },
};

const userReducer = createReducer(initialState, {
  ...setIdentityUserHandlers,
  ...satatusErrorForUserHandlers,
  ...satatusNotifForUserHandlers,
});

export default userReducer;
