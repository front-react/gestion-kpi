import { combineReducers } from "redux";
import appReducer from "./appReducer";
import surveyManagerdReducers from "./../../modules/survey/redux/reducers";
import clientAssortimentReducers from "./../../modules/client/redux/reducers";
import userReducer from "./userReducer";

const rootReducer = combineReducers({
  app: appReducer,
  survey: surveyManagerdReducers,
  client: clientAssortimentReducers,
  user: userReducer,
});

export default rootReducer;
