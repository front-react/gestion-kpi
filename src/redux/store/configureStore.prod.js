import { createStore, applyMiddleware } from "redux";
import reducers from "../reducers";
import thunk from "redux-thunk";
import callAPIMiddleware from "./../middleware/callAPIMiddleware";
/**
 * Configure dev store
 */
export default function configureStore(initialState) {
  let middleware = [thunk, callAPIMiddleware];
  const createStoreWithMiddleware = applyMiddleware(...middleware)(createStore);

  const store = createStoreWithMiddleware(reducers, initialState);

  return store;
}
