import { createStore, applyMiddleware } from "redux";
import reducers from "../reducers";
import thunk from "redux-thunk";
import callAPIMiddleware from "./../middleware/callAPIMiddleware";
/**
 * Configure dev store
 */
export default function configureStore(initialState) {
	let middleware = [thunk, callAPIMiddleware];
	const createStoreWithMiddleware = applyMiddleware(...middleware)(createStore);

	const store = createStoreWithMiddleware(
		reducers,
		initialState,
		window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
	);

	return store;
}
