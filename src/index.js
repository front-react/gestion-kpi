import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import Root from "./components/Root";
import configureStore from "./redux/store";
//import 'antd/dist/antd.css';
//import registerServiceWorker from "./registerServiceWorker"; Progressive Web App

const store = configureStore();

ReactDOM.render(<Root store={store} />, document.getElementById("root"));
//registerServiceWorker();
